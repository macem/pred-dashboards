$(function() {

	/*
		Using FastClick library for eliminating the 300ms delay between a physical tap
		and the firing of a click event on mobile browsers:
		https://github.com/ftlabs/fastclick
	*/
	FastClick.attach(document.body);



	/*
		Using jQuery chosen plugin for selects
		https://harvesthq.github.io/chosen/
	*/
	$(".js-select, .js-select--tags").chosen({
		disable_search_threshold: -1,
		search_contains: true
	});

	$('.js-select--tags + .chosen-container input.default').on('keydown', function(e) {
		var keycode = (e.keyCode ? e.keyCode : e.which);
		var value = $(this).val();

		if(keycode == '13') {
			$(this).parents('.chosen-container').prev().
				append('<option selected>' + value + '</option>').
				trigger('chosen:updated');
		}
	});


	$(".box").on('click', function() {
		return false;
	});



	/*
		Using jQuery jBox plugin for Tooltip functionality
		https://github.com/StephanWagner/jBox/
	*/
	$('.js-dropdown').each(function(idx, el) {
		$(el).jBox("Tooltip", {
			trigger: 'click',
			fixed: true,
			closeOnClick: true,
			addClass: 'dropdown font-type-2 no-smooth',
			position: {
				x: 'center',
				y: 'bottom'
			},
			content: $(el).find('.dropdown__body').html()
		});
	});

	$('.js-preview').each(function(idx, el) {
		$(el).jBox("Tooltip", {
			trigger: 'mouseenter',
			closeOnClick: true,
			outside: 'x',
			pointTo: 'left',
			position: {
				x: 'right',
				y: 'center'
			},
			addClass: 'dropdown dropdown--preview no-smooth',
			content: $(el).find('.dropdown__body').html()
		});
	});



	/*
		Using Sortable plugin for sortable functionality
		https://github.com/RubaXa/Sortable
	*/
	$('.js-sortable').each(function(idx, el) {
		var sortable = new Sortable($(el)[0], {
			animation: 300,
			draggable: '.js-sortable__draggable',
			handle: '.js-sortable__handle',
			sort: !!$(el).data('groupSortable'),
			group: {
				name: $(el).data('groupName') || 'default',
				put: $(el).data('groupPut') ? ($(el).data('groupPut')).split(',') : ['default']
			}
		});
	});



	/*
		Using jBox plugin for tooltip functionality
		http://stephanwagner.me/jBox
	*/
	$('.js-tooltip').jBox('Tooltip');



	/*
		Using switchery plugin for switch functionality
		https://github.com/abpetkov/switchery
	*/
	$('.js-switch').each(function(idx, el) {
		var switchery = new Switchery(el, {
			color: '#019966',
			secondaryColor: '#CCC',
			size: 'small'
		});
	});

	/*
		Using custom JS code for clear input functionality
		Feel free to change / modify the following code
	*/
	$('.js-input-clear').on('keyup', function() {
		var target = $(this).next('.js-input-clear-trigger');
		(!!$(this).val().length) ? target.show() : target.hide();
	});

	$('.js-input-clear-trigger').on('click', function() {
		$(this).hide().prev('.js-input-clear').val("");
	});



	/*
		Using custom JS code for side nav functionality.
		Feel free to change / modify the following code
	*/
	$('.js-sideNavTrigger').on('click', function() {
		$('.sideNav').toggleClass('sideNav--pulled');
		$('.app').toggleClass('app--pushed');
	});



	/*
		Using custom JS code for collapsible functionality.
		Feel free to change / modify the following code
	*/
	$(".js-collapsible .collapsible__trigger").on('click', function(e) {
		if(!$(e.target).data('prevent')) {
			$(this).parents('.js-collapsible').toggleClass('is-collapsed');
		}
	});



	/*
		Using custom JS code for tabs functionality.
		Feel free to change / modify the following code
	*/
	$('.js-tabs .tabs__nav-item').on('click', function() {
		var tabs = $('.js-tabs');
		var tabsNavItems = $(tabs).find('.tabs__nav-item');
		var tabsContents = $(tabs).find('.tabs__body-item');
		var tabId = $(this).data('tabid');

		tabsNavItems.removeClass('is-active');
		tabsContents.removeClass('is-active');

		$(tabs).find('[data-tabid="' + tabId + '"]').addClass('is-active')
	});

});
