# Application Style Guides
Style guides are using styledown generator that helps to write maintainable CSS styleguides efficiently using a Markdown:
https://github.com/styledown/styledown

## Requirements
* NodeJS - 0.10.x
* Styledown (installed globally)

## Running style guide generator
* From console execute the command `styledown ../css/components/*/*.css config.jade > index.html` then open index.html to view generated style guides

## Contact
* Adam Chwilkowski adam.chwilkowski@roche.com