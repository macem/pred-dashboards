#!/bin/bash
cd ../resources
module load node-v0.12.6
echo "Starting pRED Portfolio Dashboard [PROD]..."
./node_modules/forever/bin/forever -o ../logs/pPD_out.log -e ../logs/pPD_err.log -a start app.js --prod