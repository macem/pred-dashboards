/**
 * Development environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {

	/***************************************************************************
	 * Set the default database connection for models in the development       *
	 * environment (see config/connections.js and config/models.js )           *
	 ***************************************************************************/
	connections: {
		mongoDB: {
			adapter: 'sails-mongo',
			host: 'rkalvmt1',
			port: 27017,
			user: 'pred_dashboard_tst_owner',
			password: 'pred_dashboard_tst_p4ss',
			database: 'pred_dashboard_tst',
			socketOptions: {
				noDelay: true,
				keepAlive: 1,
				connectTimeoutMS: 0,
				socketTimeoutMS: 0
			}
		}
	},
	models: {
		connection: 'mongoDB'
	},
	maxUploadedFileSize: 10000000,


    /***************************************************************************
     * Set the port in the development environment to 80                        *
     ***************************************************************************/
    port: 1337
};
