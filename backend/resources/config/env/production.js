/**
 * Development environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {

	/***************************************************************************
	 * Set the default database connection for models in the development       *
	 * environment (see config/connections.js and config/models.js )           *
	 ***************************************************************************/
	connections: {
		mongoDB: {
			adapter: 'sails-mongo',
			host: 'rkaubmose011p',
			port: 27017,
			user: 'pred_dashboard_prod_owner',
			password: 'dashboard_p4ss',
			database: 'pred_dashboard_prod-db@mgd-sers44p',
			socketOptions: {
				noDelay: true,
				keepAlive: 1,
				connectTimeoutMS: 0,
				socketTimeoutMS: 0
			}
		}
	},
	models: {
		connection: 'mongoDB'
	},
	maxUploadedFileSize: 10000000,

    /*session: {
        adapter: 'connect-mongo',
        url: 'mongodb://pred_dashboard_prod_owner:dashboard_p4ss@rkaubmose011p/pred_dashboard_prod-db@mgd-sers44p',
        collection: 'sessions',
        ssl: false,
        stringify: false
    },*/

    /***************************************************************************
     * Set the port in the development environment to 80                        *
     ***************************************************************************/
    port: 1337,

    /***************************************************************************
     * pingFed configuration                        *
     ***************************************************************************/
    env: {
        session: {
            host: 'wam.roche.com',
            secret: 'ghbySXNzOm2YQGbH0Ihn7aS6QDVjAM2823Tkx6x1dwGSzsEG0cutCSU6q2YlcsTS',
            redirect: 'http://preddashboard.roche.com',
            client: 'pRED-Portfolio-Dashboard'
        }
    }
};
