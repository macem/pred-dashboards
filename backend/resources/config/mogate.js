/**
 * Mogate Authorization Service (Roche)
 * Custom API KEY for this project is:
 * 37BF1B35-5F10-474F-8D56-CB1B79120EFE
 *
 * More information about Mogate
 * you can find here:
 * https://mogate.roche.com/
 *
 * Contact person:
 * Hanna Krueger (hanna.krueger@roche.com)
 *
  ____ ____ ____ ____ ____ ____ ____
 ||W |||A |||R |||N |||I |||N |||G ||
 ||__|||__|||__|||__|||__|||__|||__||
 |/__\|/__\|/__\|/__\|/__\|/__\|/__\|

 * !!! You shouldn't edit the following keys
 * without the really important reason.!!!

 * @type {{development: {apiKey: string}, production: {apiKey: string}}}
 */
module.exports.mogate = {
	apiKey: '37BF1B35-5F10-474F-8D56-CB1B79120EFE',
	host: 'mogate.roche.com',
	port: 443,
	connection: 'Close',
	operations: {
		authenticate: {
			path: function() {
				return '/user/authenticate';
			},
			method: 'GET'
		},
		photo: {
			path: function(username) {
				username = username || 'default';
				return '/photo/' + username + '/miniature';
			},
			method: 'GET'
		},
		group: {
			path: function(groupName) {
				groupName = groupName || '';
				return '/group/members/' + groupName;
			},
			method: 'GET'
		}
	}
};