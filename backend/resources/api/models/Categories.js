/**
 * Categories.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
	autoCreatedAt: false,
	autoUpdatedAt: false,
	attributes: {
		name: {
			type: 'string',
			required: true,
			minLength: 2
		},
	    isActive: {
	      	type: 'boolean',
	      	defaultsTo: true,
	      	required: true
	    },
		position: {
			type: 'integer',
			required: false
		},
		items: {
			collection: 'Items',
			via: 'category'
		},
		overrideWithDirective: {
			type: 'string',
			required: false,
			defaultsTo: ''
		},
		ordering: {
			type: 'array',
			required: false
		},
		displayHome: {
	      	type: 'boolean',
	      	defaultsTo: false,
	      	required: false
		},
		customAttribute: function (){
			return this.position + ' / ' + this.name;
		},
		countItems: function() {
			return this.items.length;
		}
	}
};

