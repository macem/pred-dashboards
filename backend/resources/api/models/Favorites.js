/**
 * Favorites.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
	attributes: {
		userId: {
			type: 'string',
			required: true,
			minLength: 2
		},
		contents: {
			collection: 'Items',
			dominant: true
		},
		ordering: {
			type: 'array',
			required: false
		},
		getContents: function() {
			return this.contents;
		}
	}
};

