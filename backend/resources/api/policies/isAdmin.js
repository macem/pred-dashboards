/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
module.exports = function(req, res, next) {

	/**
	 * Check if user has admin rights
	 */
	if (req.session.userData.isSuperUser) {
		return next();
	}

	return res.json(
			sails.services.rescodes.clientError.forbidden,
			sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.forbidden, req.__('noAccess'))
	);
};
