/**
 * SigninController
 *
 * @description :: Server-side logic for managing sign-in process
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
 var http = require('http');
 var https = require('https');
 var q = require('q');

module.exports = {

    index: function (req, res) {
        sails.log.warn('= signin: ', req.session.userData, req.body.key, new Date().toString());

        if (!req.session.hasOwnProperty('userData') || sails.services.utils.isSessionExpired(req.session)) {

            var options = {
                method: 'POST',
                host: sails.config.env.session.host,
                path: '/as/token.oauth2',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };

            var loginReq = https.request(options, function(response) {
                var tmpStr = '';
                response.on('data', function (chunk) {
                    tmpStr += chunk;
                });

                response.on('end', function() {
                    var tokenJSON = JSON.parse(tmpStr);
                    sails.log.warn('= get token success');

                    if (tokenJSON && tokenJSON.access_token) {
                        req.session.authenticated = true;
                        req.session.key = tokenJSON.access_token;
                        req.session.time = new Date().getTime();
                        req.session.expires = tokenJSON.expires_in * 1000;
                        req.session.refresh = tokenJSON.refresh_token;

                        getUserDetails(tokenJSON.access_token).then(function (userData) {
                            if (!userData || userData && !userData.sub) {
                                return res.json({ error: userData });
                            }
                            req.session.userData = {
                                username: userData.sub,
                                id: userData.sub,
                                name: userData.name,
                                roles: [],
                                isSuperUser: false,
                                email: userData.email,
                                token_expires: req.session.time + req.session.expires
                            };
                            sails.log.warn('= get user success: ', req.session.userData);

                            sails.services.utils.checkUserAccessGroups([sails.config.admin.allowGroup], userData.sub).then(function (isSuperUser) {
                                if (!isSuperUser) {
                                    req.session.userData.mappedRoles = [];
                                    req.session.userData.roles = [];

                                    sails.services.utils.groupsToIds(function (mappedRoles) {
                                        req.session.userData.mappedRoles = mappedRoles;
                                        sails.log.warn('= roles: ', mappedRoles);
                                        return res.json(req.session.userData);
                                    }, req);
                                } else {
                                    req.session.userData.isSuperUser = isSuperUser;
                                    sails.log.warn('= super user');
                                    return res.json(req.session.userData);
                                }
                            }).catch(function (err) {
                                sails.log.warn('= cannot checkUserAccessGroups(admin)');
                                return res.negotiate(err);
                            });

                        }).catch(function (err) {
                            sails.log.warn('= error getUserDetails: ', err.message);
                            return res.negotiate(err);
                        });
                    } else {
                        sails.log.warn('= get token error');
                        req.session.authenticated = false;
                        return res.status(401).json(sails.services.rescodes.clientError.unauthorized, tokenJSON);
                    }

                });

                response.on('error', function(e) {
                    sails.log.warn('= signin error:', e.message);
                });

                /*sails.log.warn('= login unknown error');

                response.on('error', function (error) {
                    return res.status(401).json(sails.services.rescodes.clientError.unauthorized, { error: error });
                });*/
            });

            loginReq.write('grant_type=authorization_code' +
                '&client_id=' + sails.config.env.session.client +
                '&code=' + req.body.key +
                '&client_secret=' + sails.config.env.session.secret +
                '&redirect_uri=' + sails.config.env.session.redirect);

            loginReq.end();

        } else {
            req.session.authenticated = true;
            sails.log.warn('= return userData', req.session.userData);

            return res.json(req.session.userData);
        }
    },

    /**
     * @route /currentUser
     * @name currentUser
     *
     * @method get
     * @description Returns the current config
     */
    config: function(req, res) {
        return res.status(200).json({
            host: sails.config.env.session.host,
            redirect: sails.config.env.session.redirect,
            client: sails.config.env.session.client
        });
    },

    /**
     * @route /currentUser
     * @name currentUser
     *
     * @method get
     * @description Returns the current user's data.
     */
    currentUser: function(req, res) {

        /**
         * Let's check if we have 'userData' object
         * in our session store...
         */

        if (sails.services.utils.isSessionExpired(req.session)) {
            req.session.authenticated = false;
            sails.log.warn('= currentUser session expired', new Date().toString(), req.session.userData);
            return res.status(401).json(
                sails.services.rescodes.clientError.unauthorized,
                sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.unauthorized, 'Active session not found!')
            );
        }

        if (req.session.hasOwnProperty('userData')) {
            if (!req.session.userData.isSuperUser) {
                req.session.userData.mappedRoles = [];

                sails.services.utils.groupsToIds(function (mappedRoles) {
                    req.session.userData.mappedRoles = mappedRoles;
                    sails.log.warn('= currentUser: ', new Date().toString(), req.session.userData);
                    return res.json(req.session.userData);
                }, req);
            } else {
                /*getUserDetails(req.session.key).then(function (userData) {
                    sails.log.warn('-> currentUser ', req.session.time + req.session.expires, new Date().getTime(), userData.active);
                });*/
                sails.log.warn('= currentUser: ', new Date().toString(), req.session.userData);
                return res.json(req.session.userData);
            }
            req.session.authenticated = true;

        } else {
            sails.log.warn('= currentUser error', new Date().toString(), req.session.userData);
            return res.status(401).json(
                sails.services.rescodes.clientError.unauthorized,
                sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.unauthorized, 'User not found!')
            );
        }
    },

    /**
     * @route /currentUser
     * @name currentUser
     *
     * @method get
     * @description Returns the current user's data.
     */
     refresh: function(req, res) {
        sails.log.warn('= token refresh:', req.session.hasOwnProperty('userData'), req.body.userName, req.body.expires, new Date().toString(), req.body.userName === req.session.userData.username, req.body.expires === req.session.userData.token_expires);

        if (req.session.hasOwnProperty('userData') && req.body.userName === req.session.userData.username && req.body.expires === req.session.userData.token_expires) {
            refreshToken(req.session.refresh).then(function (tokenJSON) {
                if (tokenJSON && tokenJSON.access_token) {
                    req.session.authenticated = true;
                    req.session.key = tokenJSON.access_token;
                    req.session.time = new Date().getTime();
                    req.session.expires = tokenJSON.expires_in * 1000;
                    req.session.refresh = tokenJSON.refresh_token;

                    req.session.userData.token_expires = req.session.time + req.session.expires;
                    sails.log.warn('= token refreshed', req.session.userData);
                    return res.json(req.session.userData);
                } else {
                    if (!req.session.hasOwnProperty('userData')) {
                        req.session.authenticated = false;
                        sails.log.warn('= token refresh error', req.session);
                        return res.json(
                            sails.services.rescodes.clientError.unauthorized,
                            sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.unauthorized, 'User not found!')
                        );
                    }
                    sails.log.warn('= token not refreshed: ', req.session.userData.token_expires);
                    req.session.authenticated = true;
                    return res.json(req.session.userData);
                }
            });
        } else {
            if (!req.session.hasOwnProperty('userData')) {
                req.session.authenticated = false;
                sails.log.warn('= token refresh error', req.session);
                return res.status(401).json(
                    sails.services.rescodes.clientError.unauthorized,
                    sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.unauthorized, 'User not found!')
                );
            }
            sails.log.warn('= token not refreshed: ', req.session.userData.token_expires);
            req.session.authenticated = true;
            return res.json(req.session.userData);
        }
     },
};

/*function getToken(code) {
    var deferred = q.defer();
    var options = {
        method: 'POST',
        host: sails.config.env.session.host,
        path: '/as/token.oauth2',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    };

    var requestCheck = https.request(options, function (response) {
        var data = '';

        response.on('data', function (chunk) {
            data += chunk;
        });

        response.on('end', function () {
            var tokenJSON = JSON.parse(data);
            sails.log.warn('--> token: ', tokenJSON);
            deferred.resolve(tokenJSON);
        });

        response.on('error', function (error) {
            deferred.resolve(error);
        });
    });

    loginReq.write('grant_type=authorization_code' +
        '&client_id=' + client +
        '&code=' + code +
        '&client_secret=' + secret +
        '&redirect_uri=' + redirect);
    requestCheck.end();

    return deferred.promise;
}*/

function refreshToken(refreshToken) {
    var deferred = q.defer();
    var options = {
        method: 'POST',
        host: sails.config.env.session.host,
        path: '/as/token.oauth2',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    };

    var requestCheck = https.request(options, function (response) {
        var data = '';

        response.on('data', function (chunk) {
            data += chunk;
        });

        response.on('end', function () {
            var tokenJSON = JSON.parse(data);
            deferred.resolve(tokenJSON);
        });

        response.on('error', function (error) {
            sails.log.warn('= refreshToken error:', error.message);
            deferred.resolve(error);
        });
    });

    requestCheck.write('grant_type=refresh_token' +
        '&refresh_token=' + refreshToken +
        '&client_secret=' + sails.config.env.session.secret +
        '&client_id=' + sails.config.env.session.client);
    requestCheck.end();

    return deferred.promise;
}

function getUserDetails(token) {
    var deferred = q.defer();
    var options = {
        method: 'POST',
        host: sails.config.env.session.host,
        path: '/as/introspect.oauth2',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    };

    var requestCheck = https.request(options, function (response) {
        var data = '';

        response.on('data', function (chunk) {
            data += chunk;
        });

        response.on('end', function () {
            var member = JSON.parse(data);
            // sails.log.warn('--> userData: ', data);
            deferred.resolve(member);
        });

        response.on('error', function (error) {
            deferred.resolve(error);
        });
    });

    requestCheck.write('grant_type=password' +
        '&client_id=' + sails.config.env.session.client +
        '&client_secret=' + sails.config.env.session.secret +
        '&token=' + token);
    requestCheck.end();

    return deferred.promise;
}

/*function checkIfSuperUser(adminGroup, userId) {
    var postData = JSON.stringify({
        userIds: [ userId ],
        userIdOnly: true,
        useCache: true,
        recLevel: 0,
        groups: [ adminGroup ]
    });

    var deferred = q.defer();
    var options = {
        method: 'POST',
        rejectUnauthorized: false,
        host: hostAPI,
        path: '/gateway/predilogin-prod/v2.0/groups/checkUserMembership',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': Buffer.byteLength(postData),
            'X-Gravitee-Api-Key': apiKey
        },
    };

    sails.log.warn('-> checkIfSuperUser', postData);

    var requestCheck = https.request(options, function (response) {
        var data = '';

        response.on('data', function (chunk) {
            data += chunk;
        });

        response.on('end', function () {
            var member = JSON.parse(data);
            sails.log.warn('--> checkIfSuperUser done: ', adminGroup, member);
            deferred.resolve(member.groups && member.groups[0].members.length ? true : false);
        });

        response.on('error', function (error) {
            sails.log.warn('--> checkIfSuperUser error:');
            deferred.resolve(error);
        });
    });

    requestCheck.write(postData);
    requestCheck.end();

    return deferred.promise;
}*/
