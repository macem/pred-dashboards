/**
 * SignoutController
 *
 * @description :: Server-side logic for managing Signouts
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var https = require('https');

module.exports = {
	index: function(req, res) {
		try {
			/**
			 * Destroy session cookie
			 * only if the user is still
			 * logged in.
			 */
			if (req.session.authenticated) {

                sails.log.warn('logout', req.session.hasOwnProperty('userData'));

                if (req.session.hasOwnProperty('userData')) {
                    var options = {
                        method: 'POST',
                        host: sails.config.env.session.host,
                        path: '/as/revoke_token.oauth2',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    };

                    var loginReq = https.request(options, function(response) {
                        var tmpStr = '';
                        response.on('data', function (chunk) {
                            tmpStr += chunk;
                        });

                        response.on('end', function() {
                            sails.log.warn('logout done');
                            req.session.destroy();
                            return res.json(
                                sails.services.rescodes.success.ok,
                                sails.services.utils.printMessage(sails.services.rescodes.success.ok, req.__('loggedOut'))
                            );
                        });

                        response.on('error', function (error) {
                            sails.log.warn('logout error', error);
                            return res.json(
                                sails.services.rescodes.serverError.internalError,
                                sails.services.utils.printMessage(sails.services.rescodes.serverError.internalError, req.__('couldntLogOut'))
                            );
                        });
                    });

                    loginReq.write('client_id=' + sails.config.env.session.client +
                        '&client_secret=' + sails.config.env.session.secret +
                        '&token=' + req.session.key);

                    loginReq.end();
                }
			}
			/*return res.json(
					sails.services.rescodes.success.ok,
					sails.services.utils.printMessage(sails.services.rescodes.success.ok, req.__('loggedOut'))
			);*/
		} catch(e) {
			return res.json(
					sails.services.rescodes.serverError.internalError,
					sails.services.utils.printMessage(sails.services.rescodes.serverError.internalError, req.__('couldntLogOut'))
			);
		}
	}
};

