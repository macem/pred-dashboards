/**
 * CategoriesController
 *
 * @description :: Server-side logic for managing categories
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

	/**
	 * @route /
	 * @name
	 *
	 * @method post
	 * @description Adds new record
	 * @parameter name:string:required:formData|Name
	 * @parameter position:integer:notrequired:formData|Position on list (only for the UI purpose).
	 * @parameter overrideWithDirective:string:notrequired:formData|Override this category with the available angular's directive.
	 * @parameter ordering:array:notrequired:formData|Reports ordering inside this category (as an array of IDs).
	 */

	/**
	 * @route /{id}
	 * @name
	 *
	 * @method delete
	 * @description Deletes the given record.
	 * @parameter id:string:required:path
	 */

	/**
	 * @route /sort
	 * @name sort
	 *
	 * @method put
	 * @description Sorts categories in given order.
	 * @parameter order:array:required:formData
	 */
	sort: function(req, res) {
        if (sails.services.utils.isSessionExpired(req.session)) {
            return res.status(401).json(
                sails.services.rescodes.clientError.unauthorized,
                sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.unauthorized, 'Active session not found!')
            );
        }

		/**
		 * Check if client's method is allowed in this
		 * method.
		 */
		if (!sails.services.utils.allowedMethod(req, res, ['PUT'])) {
			return res.json(
					sails.services.rescodes.clientError.methodNotAllowed,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.methodNotAllowed, req.__('methodNotAllowed'))
			);
		}

		var order = req.body.order || [];

		if (order.length === 0) {
			return res.json(
					sails.services.rescodes.clientError.notFound,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.notFound, req.__('notFound'))
			);
		}

		var pos = 0;

		order.forEach(function(cat, index) {
			Categories.update({
				id: cat
			}, {
				position: pos
			}).exec(function(err, users) {
				if(err) {
					return res.serverError(err);
				}

				if(++index === order.length) {
					return res.json({ok:true});
				}
			});
			pos++;
		});
	},

	/**
	 * @route /
	 * @name index
	 *
	 * @method get
	 * @description Return categories with their childs (reports)
	 */
	index: function(req, res) {

		var action = {
			get: function() {
				var async = require('async');
				var docTypes = {};

				/**
				 * Get all existing categories
				 * with included items.
				 * @param docTypes
				 */
				var findCategories = function(docTypes) {

					var popCriteria = {};

					if (!req.session.userData.isSuperUser) {
						popCriteria.group =  req.session.userData.mappedRoles.length > 0 ? req.session.userData.mappedRoles : ['xyzXYZJhndow'];
					}

					Categories.find()
                        .populate('items', popCriteria)
                        .then(function (categories) {
                            /**
                             * Map categories collection
                             * and overwrite items collection
                             * with proper 'docType' label.
                             */
                            async.map(categories,
                                function (cat, callback) {
                                    /**
                                     * Map items
                                     * and replace docType
                                     * with proper docType object.
                                     */
                                    async.map(cat.items, function (name, callb) {
                                        name.docType = docTypes[name.docType];
                                        if(cat.hasOwnProperty('ordering')) {
                                            name.position = cat.ordering.indexOf(name.id);
                                        }
                                        callb();
                                    }, function (err, result) {
                                        if(err) {
                                            return res.negotiate(err);
                                        }

                                        return callback(null, cat);
                                    });
                                },
                                function (err, result) {

                                    if(err) {
                                        return res.negotiate(err);
                                    }

                                    /*var mappedResults = sails.services.stats.map.categories(result);
                                    sails.services.stats.reports.increase(mappedResults, req);*/

                                    return res.json(result);
                                }
                            );
                        })
                        .catch(function (err) {
                            return res.negotiate(err);
                        });
				};

				DocTypes.find()
                    .then(function(docType) {
                        async.each(docType, function(docType, done) {
                            docTypes[docType.id] = docType;
                            done();
                        }, function(err) {

                            if(err) {
                                return res.negotiate(err);
                            }

                            findCategories(docTypes);

                        });
                    })
                    .catch(function(err) {
                        return res.negotiate(err);
                    });
			},
			post: function() {
				var record = {
					name: req.body.name,
					overrideWithDirective: req.body.overrideWithDirective,
                    isActive: req.body.isActive || true,
                    displayHome: req.body.displayHome
				};

				Categories.create(record)
					.then(function (category) {
						return res.json(category);
					})
					.catch(function (err) {
						return res.negotiate(err);
					});
			}
		};

        if (sails.services.utils.isSessionExpired(req.session)) {
            return res.status(401).json(
                sails.services.rescodes.clientError.unauthorized,
                sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.unauthorized, 'Active session not found!')
            );
        }

		switch(req.method) {
			case 'GET':
				action.get();
				break;
			case 'POST':
				action.post();
				break;
			default:
				return res.json(
						sails.services.rescodes.clientError.methodNotAllowed,
						sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.methodNotAllowed, req.__('methodNotAllowed'))
				);
		}

	},
	/**
	 * @route /list
	 * @name list
	 *
	 * @method get
	 * @description Return collection of categories.
	 */
	list: function(req, res) {

		var popCriteria = {};

        if (sails.services.utils.isSessionExpired(req.session)) {
            return res.status(401).json(
                sails.services.rescodes.clientError.unauthorized,
                sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.unauthorized, 'Active session not found!')
            );
        }

		if (req.session.userData && !req.session.userData.isSuperUser) {
			popCriteria.group =  req.session.userData.mappedRoles.length > 0 ? req.session.userData.mappedRoles : ['xyzXYZJhndow'];
		}

		Categories.find()
			.populate('items', popCriteria)
			.exec(function(err, categories) {
				if (err) {
					return res.negotiate(err);
				}

				if (categories.length > 0) {
					categories.map(function(cat) {

						cat.bzdura = true; // TODO check and refactor
						cat.child = cat.items.length || 0;
						cat.items = undefined;
						delete cat.items;

						return cat;
					});
					res.json(categories);
				} else {
					/**
					 * If not, send an error to the
					 * user browser.
					 */
					res.json(
							sails.services.rescodes.clientError.notFound,
							sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.notFound, req.__('notFound'))
					);
				}
			});

	},

  /**
   * @route /disable/{id}
   * @name disable
   *
   * @method get
   * @description Disable category's visibility to other users.
   * @parameter id:string:required:path
   */
  disable: function(req, res) {
        if (sails.services.utils.isSessionExpired(req.session)) {
            return res.status(401).json(
                sails.services.rescodes.clientError.unauthorized,
                sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.unauthorized, 'Active session not found!')
            );
        }
        /**
         * Check if client's method is allowed in this
         * method.
         */
        if(!sails.services.utils.allowedMethod(req, res, ['GET'])) {
            return res.json(
            sails.services.rescodes.clientError.methodNotAllowed,
            sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.methodNotAllowed, req.__('methodNotAllowed'))
            );
        }

    /**
     * Check if 'id' parameter
     * is in string format.
     */
    req.validate({
      id: 'string'
    });

    var criteria = {
      id: req.param('id')
    };


    var updateFields = {
      isActive: false
    };

    Categories.update(criteria, updateFields)
      .then(function(updated) {

        /**
         * If not found, resopnse with
         * a proper error message.
         */
        if(updated === undefined) {
          return res.json(
            404,
            sails.services.utils.printErrorMessage(-1, req.__('noRecord') + req.param('id'))
          );
        }

        return res.json(updated);

      })
      .catch(function (err) {
        return res.negotiate({aaa:err});
      });
  },
  /**
   * @route /enable/{id}
   * @name enable
   *
   * @method get
   * @description Enable categories's visibility to other users.
   * @parameter id:string:required:path
   */
  enable: function(req, res) {
    if (sails.services.utils.isSessionExpired(req.session)) {
        return res.status(401).json(
            sails.services.rescodes.clientError.unauthorized,
            sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.unauthorized, 'Active session not found!')
        );
    }
    /**
     * Check if client's method is allowed in this
     * method.
     */
    if(!sails.services.utils.allowedMethod(req, res, ['GET'])) {
      return res.json(
        sails.services.rescodes.clientError.methodNotAllowed,
        sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.methodNotAllowed, req.__('methodNotAllowed'))
      );
    }

    /**
     * Check if 'id' parameter
     * is in string format.
     */
    req.validate({
      id: 'string'
    });

    var criteria = {
      id: req.param('id')
    };

    var updateFields = {
      isActive: true
    };

    Categories.update(criteria, updateFields)
      .then(function(updated) {

        /**
         * If not found, resopnse with
         * a proper error message.
         */
        if(updated === undefined) {
          return res.json(
            404,
            sails.services.utils.printErrorMessage(-1, req.__('noRecord') + req.param('id'))
          );
        }

        return res.json(updated);

      })
      .catch(function (err) {
        return res.negotiate(err);
      });
  },
	/**
	 * @route /contentOrdering/{id}
	 * @name contentOrdering
	 *
	 * @method put
	 * @description Sorts childs inside the given category.
	 * @parameter contentOrdering:array:required
	 * @parameter id:string:required:path
	 */
	contentOrdering: function(req, res) {
		/**
		 * Check if client's method is allowed in this
		 * method.
		 */
		if(!sails.services.utils.allowedMethod(req, res, ['PUT'])) {
			return res.json(
					sails.services.rescodes.clientError.methodNotAllowed,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.methodNotAllowed, req.__('methodNotAllowed'))
			);
		}

		var criteria = {
			id: req.param('id')
		};

		var updateTo = {
			ordering: req.body.contentOrdering || []
		};

		Categories.update(criteria, updateTo)
				.then(function(updated) {
					return res.json(updated);
				})
				.catch(function(err) {
					return res.negotiate(err);
				});

	},

	/**
	 * @route /orderCollection/{id}
	 * @name orderCollection
	 *
	 * @method put
	 * @description Sort categories.
	 * @parameter order:array:required
	 * @parameter id:string:required:path
	 */
	orderCollection: function(req, res) {
		// TODO - this methos is (probably) not used anymore. Need to check.
		/**
		 * Check if client's method is allowed in this
		 * method.
		 */
		if(!sails.services.utils.allowedMethod(req, res, ['PUT'])) {
			return res.json(
					sails.services.rescodes.clientError.methodNotAllowed,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.methodNotAllowed, req.__('methodNotAllowed'))
			);
		}

		/**
		 * Check if 'id' parameter
		 * is in string format.
		 */
		req.validate({
			id: 'string'
		});

		var criteria = {
			id: req.param('id')
		};

		var order = req.body.order || [];

		if(order.length === 0) {
			return res.json(
					sails.services.rescodes.clientError.notFound,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.notFound, req.__('notFound'))
			);
		}

		var pos = 0;

		var updateTo = {
			items: []
		};

		Categories.update(criteria, updateTo).exec(function(err, users) {
			if(err) {
				return res.serverError(err);
			}

			if(++index === order.length) {
				return res.json({ok:true});
			}
		});
	}
};

