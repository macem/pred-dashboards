/**
 * DocTypesController
 *
 * @description :: Server-side logic for managing doctypes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	/**
	 * @route /
	 * @name index
	 *
	 * @method get
	 * @description Return available doctypes.
	 */

	/**
	 * @route /{id}
	 * @name
	 *
	 * @method delete
	 * @description Deletes the given record.
	 * @parameter id:string:required:path
	 */

	/**
	 * @route /
	 * @name
	 *
	 * @method post
	 * @description Adds new record
	 * @parameter title:string:required:formData|Title
	 * @parameter imgFlagId:integer:required:formData|ID to be added in the HTML's class name.
     * @parameter searchBy:boolean:formData|flag if we should search by this type.
	 */

};

