/**
 * AccessGroupsController
 *
 * @description :: Server-side logic for managing accessgroups
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var http = require('https');

module.exports = {

	/**
	 * @route /
	 * @name
	 *
	 * @method post
	 * @description Adds new record
	 * @parameter name:string:required:formData|Name
	 * @parameter aciveDirectoryId:string:required:formData|String ID of Active Directory group.
	 * @parameter position:integer:notrequired:formData|Position on list (only for the UI purpose).
	 */

	/**
	 * @route /{id}
	 * @name
	 *
	 * @method delete
	 * @description Deletes the given record.
	 * @parameter id:string:required:path
	 */

	/**
	 * @route /order
	 * @name order
	 *
	 * @method put
	 * @description Sets the order of Access Groups.
	 * @parameter order:array:required:formData|Group IDs as an array.
	 */
	order: function(req, res) {
		/**
		 * Check if client's method is allowed in this
		 * method.
		 */
		if(!sails.services.utils.allowedMethod(req, res, ['PUT'])) {
			return res.json(
					sails.services.rescodes.clientError.methodNotAllowed,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.methodNotAllowed, req.__('methodNotAllowed'))
			);
		}

		var order = req.body.order || [];

		var pos = 0;

		order.forEach(function(group, index) {

			var criteria = {
				id: group
			};

			var updateTo = {
				position: pos
			};

			AccessGroups.update(criteria, updateTo)
				.exec(function(err, updated) {
					if(err) {
						return res.negotiate(err);
					}

					if(++index === order.length) {
						return res.json({
							updated: true
						});
					}
				});

			pos++;
		});
	},
	validate: function(req, res) {

		/**
		 * Check if client's method is allowed in this
		 * method.
		 */
		if(!sails.services.utils.allowedMethod(req, res, ['POST'])) {
			return res.json(
					sails.services.rescodes.clientError.methodNotAllowed,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.methodNotAllowed, req.__('methodNotAllowed'))
			);
		}

		var options = {
			host: sails.config.mogate.host,
			connection: sails.config.mogate.close,
			port: sails.config.mogate.port,
			path: sails.config.mogate.operations.group.path(req.param('group')),
			method: sails.config.mogate.operations.group.method,
			rejectUnauthorized: false,
			headers: {
				'Api-Key': sails.config.mogate.apiKey
			}
		};

		var mogateReq = http.request(options, function(response) {

			/**
			 * Set needed encoding.
			 */
			response.setEncoding('utf8');

			/**
			 * Create an empty string
			 * where the incoming data
			 * will be stored.
			 * @type {string}
			 */
			var tmpData = '';

			/**
			 * Define 'onData' callback
			 * to catch and store incoming data.
			 */
			response.on('data', function (chunk) {
				tmpData += chunk;
			});

			/**
			 * After the incoming binary data
			 * transfer is completed,
			 * we can handle this data and display
			 * as an image in browser
			 */
			response.on('end', function() {

				var jsonResponse = {
					isValid: false
				};

				if(response.statusCode === 200 && req.param('group')) {
					jsonResponse.isValid = true;
				} else {
					jsonResponse.isValid = false;
				}
				return res.json(jsonResponse);

				tmpData = undefined;
			});
		});


		/**
		 * Define 'onTimeout' callback
		 * to catch the timeout event
		 * and send a proper message to client.
		 */
		mogateReq.on('timeout', function(err) {
			console.log('>>>>>>>>>>>>>>>>> TIMEOUT: ' +  + JSON.stringify(err));
			mogateReq.abort();
			return res.json(
					sails.services.rescodes.clientError.requestTimeout,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.requestTimeout, req.__('connectionTimeout'))
			);
		});

		/**
		 * Define 'onError' callback
		 * to catch possible connection
		 * errors, e.g. with roche's internal network.
		 */
		mogateReq.on('error', function(err) {
			console.log('>>>>>>>>>>>>>>>>> ERROR: ' + JSON.stringify(err));
			mogateReq.abort();
			return res.json(
					sails.services.rescodes.clientError.requestTimeout,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.requestTimeout, req.__('connectionTimeout'))
			);
		});

		mogateReq.end();
	}
};

