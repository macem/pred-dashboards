var http = require('http');
var https = require('https');
var q = require('q');

var hostAPI = 'api.core.minerva.roche.com';
var apiKey = '4668ff61-14e0-44cf-82f6-6b1390bd9a4a';

module.exports = {
	message: {
		error: {
			noRecord: 'Couldn\'t find the record with primary key = ',
			noAccess: 'You don\'t have permission to access this API method. Please contact with your pRed Portfolio Dashboard Administrator.',
			updateNotPossible: 'The update was not possible.',
			forbidden: 'Forbidden',
			timeout: 'Timeout'
		}
	},
	printMessage: function(code, message) {
		code = code || 200;
		message = message || null;

		return {
			code: code,
			message: message
		};
	},
	printErrorMessage: function(code, message) {
		return {
			code: code,
			message: message
		};
	},
	printExtendedErrorMessage: function(code, message, details) {
		return {
			code: code,
			message: message,
			details: details
		};
	},
	printUpdatedMessage: function(updatedObject) {
		return {
			code: 0,
			message: 'The record has been updated.',
			updated: updatedObject
		};
	},
	printUnauthorizedMessage: function() {
		return {
			code: sails.services.rescodes.clientError.forbidden,
			message: 'You don\'t have permission to access this API method. Please contact with your pRed Portfolio Dashboard Administrator.'
		};
	},
	allowedMethod: function(req, res, methods) {
		methods = methods || [];

		if(methods.indexOf(req.method) < 0) {
			return false;
		} else {
			return true;
		}
	},
	objectToJSON: function(object) {
		object = object || {};
		return JSON.parse(JSON.stringify(object))
	},
	groupsToIds: function(callback, req) {
		sails.log.warn('-> groupsToIds');

		AccessGroups.find().then(function(found) {
		    var validGroups = [];
            var groupsCounter = 0;

            found.forEach(function (group) { // for every access group
                var ADgroups = group.aciveDirectoryId.split(','); // get ad-groups
                sails.log.warn('-> GROUP:', group.id, group.name);

                sails.services.utils.checkUserAccessGroups(ADgroups, req.session.userData.username).then(function (isAccessGroupMember) {
                    if (isAccessGroupMember) {
                        validGroups.push(group.id);
                        sails.log.warn('-> groupsToIds access:', group.id, group.name);
                    } else {
                        sails.log.warn('-> groupsToIds no access:', group.id, group.name);
                    }
                    groupsCounter++;

                    if (groupsCounter == found.length) {
                        callback(validGroups);
                    }
                }).catch(function (err) {
                    sails.log.warn('= cannot checkUserAccessGroups(all)');
                    return res.negotiate(err);
                });
            });
		});
	},

    checkUserAccessGroups: function(groups, userId) {
        var postData = JSON.stringify({
            userIds: [ userId ],
            userIdOnly: true,
            useCache: true,
            recLevel: 0,
            groups: groups
        });

        var deferred = q.defer();
        var options = {
            method: 'POST',
            rejectUnauthorized: false,
            host: hostAPI,
            path: '/gateway/predilogin-prod/v2.0/groups/checkUserMembership',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(postData),
                'X-Gravitee-Api-Key': apiKey
            },
        };

        sails.log.warn('-> checkUserAccess', postData);

        var requestCheck = https.request(options, function (response) {
            var data = '';

            response.on('data', function (chunk) {
                data += chunk;
            });

            response.on('end', function () {
                var accessGroups = JSON.parse(data);
                var hasAccess = accessGroups && accessGroups.groups.filter(function(value) {
                    return value.members.length;
                }) || [];
                // sails.log.warn('= checkUserAccess done: ', hasAccess);
                deferred.resolve(hasAccess && hasAccess.length ? true : false);
            });

            response.on('error', function (error) {
                sails.log.warn('--> checkUserAccess error:');
                deferred.resolve(error);
            });
        });

        requestCheck.write(postData);
        requestCheck.end();

        return deferred.promise;
    },


  /**
   * @param user (string) - user login
   * @param ADgroups (string) - '|' separated AD groups
   * @returns {*|promise}
     */
  /*checkAccessGroupGRP: function(user, ADgroups) {
    var deferred = q.defer();

    ADgroups = 'AD:' + ADgroups.map(function (groupName) {
        return groupName.trim();
      }).join('|AD:');

    var options = {
      host: 'predilogin.roche.com',
      connection: 'Close',
      port: 80,
      rejectUnauthorized: false,
      headers: {
        'Content-Type': 'application/json'
      },
      path: encodeURI('/GrpmanServlet/isMember?group=' + ADgroups + '&userid=' + user),
      method: 'GET'
    };
    sails.log.warn('AD Groups query OBJ: ', options);

    // FOR TESTS ONLY
    // var content = 'http://' + options.host + options.path;
    // var fs = require('fs');
    // fs.appendFile('C:/projects/access_groups.txt', content + "\n\n", function (err) {
    //  console.log(err);
    // });

    var output = '';

    var req = http.request(options, function(res) {

      res.setEncoding('utf8');

      res.on('data', function (chunk) {
        output += chunk;
      });

      res.on('end', function() {
        deferred.resolve(output);
      });

      res.on('error', function (err) {
        sails.log.warn('Error while getting user groups assignment, err: ' + JSON.stringify(err));
      });
      res.on('timeout', function (err) {
        sails.log.warn('Timeout while getting user groups assignment, err: ' + JSON.stringify(err));
      });

    });

    req.setTimeout(10000);

    req.on('error', function(e) {
      deferred.reject(e.message);
    });

    req.on('timeout', function(err) {
      req.log('>>>>>>>>>>>>>>>>> TIMEOUT: ' +  + JSON.stringify(err));
      req.abort();
      deferred.reject('Timeout');
    });

    req.end();

    return deferred.promise;
  },*/

	isMobile: function(req) {
		if(!req) {
			return false;
		}

		var userAgent = req.header('user-agent');

		return /mobile/i.test(userAgent);
	},
	getBaseName: function(path) {
		var base = path.substring(path.lastIndexOf('/') + 1);
		if(base.lastIndexOf('.') !== -1) {
			base = base.substring(0, base.lastIndexOf('.'));
		}
		return base;
	},
    isSessionExpired: function(session) {
        sails.log.warn('= expiration: ', session.time + session.expires <= new Date().getTime());
        return session.time + session.expires <= new Date().getTime();
    }
};
