'use strict';

describe('Directive: modalError', function () {

  // load the directive's module
  beforeEach(module('mainApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<modal-error></modal-error>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the modalError directive');
  }));
});
