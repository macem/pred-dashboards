'use strict';

describe('Service: watchers', function () {

  // load the service's module
  beforeEach(module('mainApp'));

  // instantiate service
  var watchers;
  beforeEach(inject(function (_watchers_) {
    watchers = _watchers_;
  }));

  it('should do something', function () {
    expect(!!watchers).toBe(true);
  });

});
