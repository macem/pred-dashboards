'use strict';

/**
 * @ngdoc Main App Config using AngularJS
 * @name mainApp
 * @description
 * This application uses ui.router as a replacement for poor ngRoute.
 * More details about ui.router you can find here:
 * http://dominikmarczuk.pl/2014/12/jak-zaczac-uzywac-ui-router-przepisanie-aplikacji (polish)
 *
 * or here:
 * https://github.com/angular-ui/ui-router
 */
var MAIN_ROUTE = 'home';
var SIGIN_ROUTE = 'signin';

angular
		.module('mainApp', [
			'config',
			'ngAnimate',
			'ngCookies',
			'ngResource',
			'ngSanitize',
			'ngTouch',
			'ui.router',
			'angular-gestures',
			'ipCookie'
		])
		.run(function(notification, $http, api, $state, $location, $timeout, User, $rootScope, GlobalSelectors, preloader) {

			var TAG = 'app.js';

			$rootScope.$on('$viewContentLoading', function(){
				console.info('stateChangeStart');
				preloader.show();
			});

			$rootScope.$on('$stateChangeStart ', function(){
				console.info('stateChangeStart ');
				preloader.show();
			});

			$rootScope.$on('$stateChangeSuccess', function() {
				$timeout(function() {
					document.body.scrollTop = document.documentElement.scrollTop = 0;
                    preloader.hide();
				});
			});

			/**
			 * Check if user is already logged in.
			 */

            var params = new URLSearchParams(window.location.search);
            var code = params.get('code') || null;

			$http.post(api.currentUser, { key: code })
				.success(function(data) {
					/**
					 * Check if we have any data in our
					 * 'User.data' object and if not,
					 * we will apply the proper data
					 * to this object.
					 */
					if (Object.keys(User.data).length < 1) {
                        $timeout(function() {
                            User.setData(data);
                        });
					}
				})
				.error(function(error) {
					/**
					 * So, we have an error, which PROBABLY means
					 * that we have no permission to use the 'currentUser' method
					 * on the API.
					 *
					 * If we get code from pingFed then please signIn to api
					 */

                    if (code) {
                        $http.post(api.signIn, { key: code })
                            .success(function (data, status, headers, config) {
                                if (status !== 200) {
                                    notification.push('Error: ' + data, notification.types.error);
                                } else {
                                    User.setData(data);
                                    console.log('session expire: ', data.token_expires - new Date().getTime());
                                    // to remove ?code from url we need to reload page
                                    location.replace(location.origin + location.pathname +'#/' + MAIN_ROUTE);
                                }
                            });
                    } else {
                        if (error && error.code && error.code != 403 && error.code != 401 && $state.current.name !== SIGIN_ROUTE) {
                            console.log('Error: ');
                            notification.push('Error: ' + error.message, notification.types.error);
                        }
                    }
				});


			/**
			 * This is the moment,
			 * when we need to init our Notification Service.
			 * It will append a proper container
			 * to the document's body.
			 *
			 * That should happen only once per
			 * application's session (on app init).
			 */
			notification.init();
		})
		.config(function ($stateProvider, $urlRouterProvider, $httpProvider, $compileProvider, $cookiesProvider) {

			$httpProvider.defaults.withCredentials = true;

			/**
			 * What type of URL protocols should be allowed in ng-href attribute?
			 * The answer is: every! ;)
			 */
			$compileProvider.aHrefSanitizationWhitelist(/^\s*([A-Za-z]+)\:/i);

			var isIE = (navigator.userAgent.indexOf("MSIE") != -1)
        || (navigator.userAgent.indexOf("Trident/6.0") != -1)  //ie10
        || (navigator.userAgent.indexOf("Trident/7.0") != -1); //ie11

			if(isIE) {
				/**
				 * Initialize get if not there
				 */
				if (!$httpProvider.defaults.headers.get) {
					$httpProvider.defaults.headers.get = {};
				}

				/**
				 * Disable IE ajax request caching
				 * @type {string}
				 */
				$httpProvider.defaults.headers.get['If-Modified-Since'] = 'Fri, 05 Oct 1984 12:30:00 GMT';
				$httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
				$httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
			}

			/**
			 * Add custom http interceptor
			 * to manage UI loaders.
			 */
			$httpProvider.interceptors.push('httpInterceptor');

			/**
			 * Default state.
			 * This is the place where user will
			 * be redirected by default.
			 */
			$urlRouterProvider.otherwise('/' + MAIN_ROUTE + '/');

			/**
			 * Our states.
			 */
			$stateProvider
					.state('home', {
						url: '/home',
						templateUrl: 'templates/state-home.html',
						controller: 'HomeCtrl',
                        reloadOnSearch: false
					})
					.state('dashboard', {
						url: '/dashboard/:categoryId',
						templateUrl: 'templates/state-dashboard.html',
						defaultParams: {
							categoryId: null
						},
						controller: 'DashboardCtrl'
					})
					.state('dashboard.list', {
						url: '/list',
						templateUrl: 'templates/state-dashboard.html',
						controller: function ($scope) {
							$scope.items = ['A', 'List', 'Of', 'Items'];
						}
					})
					.state('projections', {
						url: '/projections-and-goals',
						templateUrl: 'templates/state-projections.html'
					})
					.state('report', {
						url: '/report/:id',
						templateUrl: 'templates/state-report.html',
						controller: 'ReportCtrl as main'
					})
					.state('category', {
						url: '/category/:selectedCategoryId',
						templateUrl: 'templates/state-category.html',
						controller: 'CategoryCtrl as main'
					})
					.state('mashup', {
						url: '/mashup',
						templateUrl: 'templates/state-mashup.html',
						controller: 'MashupCtrl'
					})
					.state('mashup.create', {
						url: '/create',
						views: {
							viewer: {
								templateUrl: 'templates/state-mashup-create.html',
								controller: 'MashupcreateCtrl'
							}
						}
					})
					.state('mashup.edit', {
						url: '/edit/:id',
						views: {
							viewer: {
								templateUrl: 'templates/state-mashup-edit.html',
								controller: 'MashupeditCtrl as main'
							}
						}
					})
					.state('mashup.view', {
						url: '/:id?created',
						views: {
							viewer: {
								templateUrl: 'templates/state-mashup-view.html',
								controller: 'MashupviewCtrl as main'
							}
						}
					})
					.state('help', {
						url: '/help',
						templateUrl: 'templates/state-help.html'
					})
					.state('help.about', {
						url: '/about/:id',
						defaultParams: {
							id: 0
						},
						views: {
							viewer: {
								templateUrl: 'templates/state-help-about.html',
								controller: function($scope, $stateParams, ENV) {
									$scope.x = $stateParams.id;
									$scope.appInfo = ENV.package;
								}
							}
						}
					})
					.state('help.plansource', {
						url: '/plansource',
						views: {
							viewer: {
								templateUrl: 'templates/state-help-plansource.html'
							}
						}
					})
					.state('help.portos', {
						url: '/portos',
						views: {
							viewer: {
								templateUrl: 'templates/state-help-portos.html'
							}
						}
					})
					.state('signin', {
						url: '/signin',
						templateUrl: 'templates/state-signin.html',
						controller: 'SigninCtrl'
					})
					.state('signout', {
						url: '/signout',
						templateUrl: 'templates/state-signout.html',
						controller: 'SignoutCtrl'
					})
					.state('administration', {
						url: '/administration',
						templateUrl: 'templates/state-administration.html',
						controller: 'AdministrationCtrl'
					})
					.state('administration.categories', {
						url: '/categories',
						views: {
							viewer: {
								templateUrl: 'templates/state-administration-categories.html',
								controller: 'AdministrationcategoriesCtrl'
							}
						}
					})
					.state('administration.types', {
						url: '/types',
						views: {
							viewer: {
								templateUrl: 'templates/state-administration-types.html',
								controller: 'AdministrationtypesCtrl'
							}
						}
					})
					.state('administration.reports', {
						url: '/reports',
						views: {
							viewer: {
								templateUrl: 'templates/state-administration-reports.html',
								controller: 'AdministrationreportsCtrl as main'
							}
						}
					})
					.state('administration.groups', {
						url: '/groups',
						views: {
							viewer: {
								templateUrl: 'templates/state-administration-groups.html',
								controller: 'AdministrationgroupsCtrl as main'
							}
						}
					})
					.state('administration.maintenance', {
						url: '/maintenance',
						views: {
							viewer: {
								templateUrl: 'templates/state-administration-maintenance.html',
								controller: 'AdministrationmaintenanceCtrl as main'
							}
						}
					})
					.state('notfound', {
						url: '/404',
						templateUrl: 'templates/state-404.html'
					});
		});
