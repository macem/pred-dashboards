'use strict';

/**
 * @ngdoc directive
 * @name mainApp.directive:report
 * @description
 * # report
 */
angular.module('mainApp')
		.directive('report', function (api, $compile, utils, $http, $state, $sce) {
			return {
				templateUrl: 'templates/directive-report.html',
				restrict: 'A',
				scope: {
					item: '=ngBody',
					category: '=ngCategory',
					favMethod: '=ngFavMethod',
					favData: '=ngFavData',
					openReportMethod: '=ngOpenReportMethod',
					isSortable: '@ngIsSortable',
					isBodySortable: '@ngIsBodySortable',
					query: '=ngQuery'
				},
				link: function postLink(scope, element, attrs) {
					var isSortable = scope.isSortable === 'true' ? true : false;
					var isBodySortable = scope.isBodySortable === 'true' ? true : false;
                    var docType = scope.item.docType.imgFlagId;

					scope.countAPI = api.itemsCount;

					scope.trustUrl = function(url) {
						return $sce.trustAsResourceUrl(url);
					};

					scope.css = {
						selectors: {
							preview: '.js-preview'
						}
					};

					/*scope.$watch('item', function(old, actial) {
						scope.coverImg = api.cover + '/' + scope.item.id + '?cache=' + Date.now();
					});*/
					scope.coverImg = api.cover + '/' + scope.item.id + '?cache=' + Date.now();

					scope.sortable = isSortable;
					scope.bodySortable = isBodySortable;
					scope.isExternalResource = utils.isExternalResource(scope.item.id, scope.item.embedURL, docType);

					scope.actions = {
						default: function() {
							return true;
						},
						increaseStats: function() {
							var openOutsideFrame = utils.openInNewWindow;
							if(openOutsideFrame.indexOf(parseInt(scope.item.docType.imgFlagId)) >= 0) {
								$http.get([api.items, '/', scope.item.id].join(''))
										.success(function() {
											return true;
										});
							}

							return true;
						},
						log: function(value) {
							console.warn('--------> click: %s', value);
						}
					};

					scope.getStateUrl = function(id) {
						return $state.href('report', {id: id});
					};

                    scope.getUrl = function(item) {
                        return item.embedURL.replace('action=embedview', 'action=view');
                    }

					if (scope.sortable && scope.bodySortable) {
						element.find(scope.css.selectors.preview).each(function(idx, el) {
							$(el).jBox('Tooltip', {
								trigger: 'mouseenter',
								closeOnClick: true,
								outside: 'x',
								pointTo: 'left',
								position: {
									x: 'right',
									y: 'center'
								},
								addClass: 'dropdown dropdown--preview no-smooth',
								content: $compile(angular.element($(el).find('.dropdown__body').html()))(scope)
							});
						});
					}
				}
			};
		});
