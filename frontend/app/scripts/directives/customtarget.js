'use strict';

/**
 * @ngdoc directive
 * @name mainApp.directive:customTarget
 * @description
 * # customTarget
 */
angular.module('mainApp')
		.directive('customTarget', function ($timeout) {
			return {
				restrict: 'A',
				link: function postLink(scope, element, attrs) {
					console.log(scope.customTarget);
					//element[0].setAttribute('target', attrs.customTarget);
				}
			};
		});
