'use strict';

/**
 * @ngdoc directive
 * @name mainApp.directive:alwaysVisibleMenu
 * @description
 * # navigation
 */
angular.module('mainApp')
		.directive('alwaysVisibleMenu', function () {

    function keepMenu(scope, announcement) {
        var $flex = $('.flex');

        if (parseInt($(window).width()) > 1024) {
            var scrollTop = parseInt($(window).scrollTop());

            if (scrollTop > 73) {
                $flex.css({ top: -73 });
                scope.$nav.css({ top: announcement ? 57 : 0, marginTop: 0 });
            } else {
                $flex.css({top: -1 * scrollTop});
                scope.$nav.css({ top: scope.navTop - scrollTop, marginTop: scope.navMarginTop });
            }
        } else {
            $flex.css({ top: 0 });
            scope.$nav.css({ top: scope.navTop, marginTop: scope.navMarginTop });
        }

        $('.app__footer').css({ top: 0 });
        $('.pane--filters').css({ top: 0 });
        $('#mashups .flex').css({ top: 0 });
    }

    return {
        restrict: 'A',
        scope: {},
        link: function postLink(scope) {
            scope.$nav = $('.app__header .nav');
            scope.navTop = 65;
            scope.navMarginTop = parseInt(scope.$nav.css('margin-top'));

            scope.$on('refreshPositionMenu', function(event, arg) {
                scope.navTop = arg;
                $(window).scroll(keepMenu.bind(this, scope, arg === 125 ? true : false));
                $(window).resize(keepMenu.bind(this, scope, arg === 125 ? true : false));
            });

            console.log('nav', scope.$nav);

            $(window).scroll(keepMenu.bind(this, scope, false));
            $(window).resize(keepMenu.bind(this, scope, false));
        }
    };
});
