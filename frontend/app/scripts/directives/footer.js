'use strict';

/**
 * @ngdoc directive
 * @name mainApp.directive:footer
 * @description
 * # footer
 */
angular.module('mainApp')
		.directive('appFooter', function ($timeout, $rootScope, ENV) {
			return {
				templateUrl: 'templates/directive-footer.html',
				restrict: 'E',
				link: function postLink(scope, navigationCtrl) {
					scope.ENV = ENV;
					scope.actions = {
						tellToNav: function(value) {
							$rootScope.$emit('footer:listener', value);
						}
					};
				}
			};
		});
