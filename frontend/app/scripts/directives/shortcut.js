'use strict';

/**
 * @ngdoc directive
 * @name mainApp.directive:shortcut
 * @description
 * # shortcut
 */
angular.module('mainApp')
		.directive('shortcut', function (api, $compile, utils, $http, $state, $sce) {
			return {
				templateUrl: 'templates/directive-shortcut.html',
				restrict: 'A',
				scope: {
					item: '=ngBody',
					category: '=ngCategory',
					favMethod: '=ngFavMethod',
					favData: '=ngFavData',
				},
				link: function postLink(scope, element, attrs) {

					scope.countAPI = api.itemsCount;

					scope.trustUrl = function(url) {
						return $sce.trustAsResourceUrl(url);
					};

					scope.coverImg = api.cover + '/' + scope.item.id + '?cache=' + Date.now();

					scope.actions = {
						default: function() {
							return true;
						},
						increaseStats: function() {
							var openOutsideFrame = utils.openInNewWindow;
							if(openOutsideFrame.indexOf(parseInt(scope.item.docType.imgFlagId)) >= 0) {
								$http.get([api.items, '/', scope.item.id].join(''))
										.success(function() {
											return true;
										});
							}

							return true;
						},
						log: function(value) {
							console.warn('--------> click: %s', value);
						}
					};

					scope.getStateUrl = function(id) {
						return $state.href('report', {id:id});
					};

                    scope.getUrl = function(url) {
                        return url.replace(window.location.origin + '/', '');
                    }
				}
			};
		});
