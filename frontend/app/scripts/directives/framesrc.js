'use strict';

/**
 * @ngdoc directive
 * @name mainApp.directive:frameSrc
 * @description
 * # frameSrc
 */
angular.module('mainApp')
		.directive('frameSrc', function ($parse) {
			return {
				restrict: 'A',
				link: function postLink(scope, element, attrs) {
					var frameSrc = $parse(attrs.frameSrc)(scope);

					element.attr('src', frameSrc);

					scope.$on('$destroy', function () {
						element.unbind();
					});
				}
			};
		});
