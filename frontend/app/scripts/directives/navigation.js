'use strict';

/**
 * @ngdoc directive
 * @name mainApp.directive:navigation
 * @description
 * # navigation
 */
angular.module('mainApp')
		.directive('navigation', function (User, $timeout, api, $http, $state, $stateParams, $compile, ENV, $rootScope, $log) {
			return {
				templateUrl: 'templates/directive-navigation.html',
				restrict: 'E',
				scope: {},
				link: function postLink(scope, element) {

					/**
					 * Define an empty 'user' object
					 * to be accessible from the view side.
					 * @type {{}}
					 */
					scope.user = {};
					scope.categories = [];

					scope.loadCategories = function() {
						$http.get(api.categoriesShort)
							.success(function(data) {
								scope.categories = data.filter(function(item) {
                                    return !item.displayHome;
                                });
							});
					};

					/**
					 * Reload categories bar if the new one is added
					 * or any removed / updated.
					 */
					$rootScope.$on('categories:update', function() {
						scope.loadCategories();
					});


					var body = document.body;
					var sideNav = document.querySelector('.sideNav');

					body.insertBefore(sideNav, body.childNodes[0]);

					scope.ui = function(data) {
						/**
						 * We want user data
						 * only if this data object
						 * is not empty.
						 *
						 * Otherwise, stop executing
						 * the remaining code.
						 */
						if (Object.keys(data).length === 0) {
							return;
						}

                        scope.loadCategories();

						/**
						 * Place the all data
						 * that we have about the current user.
						 *
						 * TODO - is it necessary to store the whole data? Or maybe only firstName, lastName and isSuperUser? TBC
						 */
						scope.user = data;
						scope.user.isSuperUser = User.isSuperUser();

						scope.avatar = api.avatar + '?cache=' + Date.now();

						/*
						 Using FastClick library for eliminating the 300ms delay between a physical tap
						 and the firing of a click event on mobile browsers:
						 https://github.com/ftlabs/fastclick
						 */
						FastClick.attach(document.body);


						/* TODO needs to be move to a proper place.. */
						$('.js-sideNavTrigger,.sideNav__link').unbind('click').on('click', function() {
							$('.sideNav').toggleClass('sideNav--pulled');
							$('.app').toggleClass('app--pushed');
						});

						/*
						 Using jQuery jBox plugin for Tooltip functionality
						 https://github.com/StephanWagner/jBox/
						 */
						$timeout(function() {
							if(document.querySelector('.jBox-container') === null) {
								$('[data-js="dropdown"]').each(function(idx, el) {
									$(el).jBox('Tooltip', {
										trigger: 'click',
										fixed: true,
										closeOnClick: true,
										addClass: 'dropdown font-type-2 no-smooth',
										content: $compile(angular.element($(el).find('.dropdown__body').html()))(scope)
									});
								});
							}
						});

						scope.skipTo = function(category, event) {
							var fixedOffset = 60;

				              // remove iframe first to prevent IE error with PDF
				              var frames = angular.element('.iframe iframe');
				              if (frames.length) {
				                for (var l=0; l<frames.length; l++) {
				                  frames[l].src = '';
				                }
				              }

							if ($state.current.name === 'dashboard') { // TODO refactor
				                if (category.id === 'home') {
				                  $state.go('home');
				                } else {
				                  var $categoryEl = $('#' + scope.getCategory(category));
				                  if ($categoryEl && $categoryEl.offset() && $categoryEl.offset().top) {
				                    $('html, body').animate({
				                      scrollTop: $categoryEl.offset().top - $('header').outerHeight() + fixedOffset
				                    }, 1000);
				                  }
				                }
							} else {
								$state.go('dashboard', {
									categoryId: category.id
								});
							}
							event.preventDefault();
							event.stopPropagation();
						};

                        scope.getCategory = function(category) {
                            return category.overrideWithDirective
                                ? category.overrideWithDirective
                                : 'category-' + category.id;
                        }

                        scope.isActive = function(isActive) {
                            return isActive === undefined || isActive === true;
                        };

						scope.hideMobileMenu = function() {
							$('.sideNav').removeClass('sideNav--pulled');
							$('.app').removeClass('app--pushed');
							$log.info('Hiding mobile menu.');
						};
					};

					if (Object.keys(User.data).length > 0) {
                        scope.ui(User.data);
					};

					/**
					 * Set user first name and second name
					 * in our navigation after its
					 * came from API via broadcast service.
					 *
					 * Broadcasting starts in app.js and signin.js.
					 *
					 * If we got the all necessary data,
					 * we are alos ready to load the other
					 * navigation components.
					 */
					$rootScope.$on('user', function(event, data) {
						scope.ui(data);
                        $rootScope.$broadcast('session:confirm');
					});

					scope.contents = {
						environment: ENV
					};

					scope.states = {
						modals: {
							about: false,
							contact: false
						}
					};

					scope.actions = {
						about: {
							open: function() {
								scope.states.modals.about = true;
							},
							close: function() {
								scope.states.modals.about = false;
							}
						},
						contact: {
							open: function() {
								scope.states.modals.contact = true;
							},
							close: function() {
								scope.states.modals.contact = false;
							}
						},
						goto: function(state, params) {
							state = state || 'dashboard';
							params = params || {};

							$state.go(state, params);
						}
					};

					$rootScope.$on('footer:listener', function(event, data) {
						data = data || 'about';
						scope.actions[data].open();
					});
				}
			};
		});
