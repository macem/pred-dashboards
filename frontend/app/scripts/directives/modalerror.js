'use strict';

/**
 * @ngdoc directive
 * @name mainApp.directive:modalError
 * @description
 * # modalError
 */
angular.module('mainApp')
		.directive('modalError', function () {
			return {
				templateUrl: 'templates/directive-modal-error.html',
				restrict: 'A',
				controller: function($scope, $element) {
					var main = this;
					var element = $element[0];

					main.css = {
						classes: {
							hide: 'hide'
						}
					};

					main.actions = {
						close: function() {
							element.classList.add(main.css.classes.hide);
						}
					};
				},
				controllerAs: 'main',
				link: function postLink(scope, element, attrs) {

				}
			};
		});
