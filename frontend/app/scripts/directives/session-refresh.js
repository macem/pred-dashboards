'use strict';

/**
 * @ngdoc directive
 * @name mainApp.directive:sessionRefresh
 * @description
 * # sessionRefresh
 */
angular.module('mainApp')
    .directive('sessionRefresh', function(api, User, $http, $rootScope, $timeout, $state) {
        var debounce = 6000; //in milisec
        var SIGIN_ROUTE = 'signin';

        function refreshToken(user) {
            $http
                .post(api.refreshUser, {
                    userName: user.username,
                    expires: user.token_expires
                })
                .success(function (data, status) {
                    console.log('-> refreshed: ', data);
                    User.setData(data, false);
                });
        }

        function setEndSessionConfirm(scope, user) {
            if (user && User.sessionExpireTime() > 0) {
                $timeout.cancel(scope.timeoutEnd);

                scope.timeoutEnd = $timeout(function() {
                    if (window.confirm('This session expired, do You want to continue session?')) {
                        refreshToken(User.getData());
                    } else {
                        $timeout(function () {
                            User.setData({});
                            $state.go(SIGIN_ROUTE);
                        }, 100);
                    }
                }, User.sessionExpireTime());
            }
        }

        return {
            restrict: 'A',
            link: function postLink(scope, element, attr) {
                $rootScope.$on('session:cancel', function(){
                    $timeout.cancel(scope.timeout);
                    $timeout.cancel(scope.timeoutEnd);
                });

                $rootScope.$on('session:confirm', function(){
                    setEndSessionConfirm(scope, User.getData());
                });

                // when user click we wait 3000ms and then refresh token
                element.bind('click', function(e) {
                    if ($state.current.name === SIGIN_ROUTE) {
                        return $timeout.cancel(scope.timeout);
                    }

                    $timeout.cancel(scope.timeout);
                    scope.timeout = $timeout(function() {
                        refreshToken(User.getData());
                    }, debounce);

                    setEndSessionConfirm(scope, User.getData());
                });
            }
        };
    });
