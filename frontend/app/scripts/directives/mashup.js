'use strict';

/**
 * @ngdoc directive
 * @name mainApp.directive:mashup
 * @description
 * # mashup
 */
angular.module('mainApp')
  .directive('mashup', function (api) {
			return {
				templateUrl: 'templates/directive-mashup.html',
				restrict: 'E',
				scope: {
					item: '=ngBody',
					query: '=ngQuery'
				},
				link: function postLink(scope, element, attrs) {
					scope.imgPath = api.cover;
				}
			};
  });
