'use strict';

/**
 * @ngdoc directive
 * @name mainApp.directive:message
 * @description
 * # message
 */
angular.module('mainApp')
		.directive('message', function ($log) {
			return {
				templateUrl: 'templates/directive-message.html',
				restrict: 'E',
				transclude: true,
				scope: {
					title: '@',
					type: '@'
				},
				link: function postLink(scope, element) {
					var element = element[0];

					scope.actions = {
						remove: function() {
							scope.$destroy();
							element.parentNode.removeChild(element);
						}
					};

					scope.$on('$destroy', function() {
						$log.info('Message Box: destroyed.');
					});
				}
			};
		});
