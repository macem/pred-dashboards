'use strict';

/**
 * @ngdoc directive
 * @name mainApp.directive:modalReportEdition
 * @description
 * # modalReportEdition
 */
angular.module('mainApp')
		.directive('modalReportEdition', function (Watchers, $log, $q, api, $http, $timeout, $state) {
			return {
				templateUrl: 'templates/directive-modal-report-edition.html',
				restrict: 'E',
				scope: {
					report: '=ngReport',
					createNew: '=ngCreateNew',
					show: '=ngCustomShow'
				},
				controller: function ($scope, $element) {

				},
				controllerAs: 'main',
				link: function postLink(scope, element, attrs, main) {
					var html = element[0];
					var origin = {};

					main.contents = {};

					main.css = {
						classes: {},
						selectors: {
							coverPhoto: 'coverPhoto',
							main: 'div.editReport'
						}
					};

					main.states = {
						wasAlreadyClosed: false
					};

					main.settings = {
						TAG: '[modalReportEdition]'
					};

					main.promises = {
						collection: []
					};

					main.load = {
						categories: function () {
							return $http.get(api.categoriesShort)
									.success(function (categories) {
										$timeout(function() {
											main.contents.categories = categories;
											origin.categories = categories;
										});
									});
						},
						docTypes: function () {
							return $http.get(api.docTypes)
									.success(function (docTypes) {
										main.contents.docTypes = docTypes;
										origin.docTypes = docTypes;
									});
						},
						accessGroups: function () {
							return $http.get(api.accessGroups)
									.success(function (accessGroups) {
										main.contents.accessGroups = accessGroups;
										origin.accessGroups = accessGroups;
									});
						},
						all: function () {
							for (var toLoad in main.load) {
								if (toLoad !== 'all' && main.load.hasOwnProperty(toLoad) && main.load[toLoad] instanceof Function) {
									main.load[toLoad].call();
								}
							}
						}
					};

					main.actions = {
						destroy: function () {
							$log.info('[%s] Destroying <modal-report-edition>.', main.settings.TAG);
							main.watchers.collection.unregister();
							main.watchers.collection = null;
						},
						chosen: {
							bindAll: function() {
								$timeout(function() {
									$(main.css.selectors.main + " .js-select").chosen();
								});
							},
							unbindAll: function() {
								$(main.css.selectors.main + " .js-select").chosen('destroy');
							}
						},
						save: function() {
							var record,
								formData,
								keys,
								file,
								httpOptions;

							$log.info('%s Saving...', main.settings.TAG);
							file = document.getElementById(main.css.selectors.coverPhoto);
							file = file.files[0];

							record = angular.copy(main.contents.report);
							keys = Object.keys(record);

							formData = new FormData();

							keys.forEach(function(key) {
								if(key !== 'coverPhoto') {
									var value = record[key];

									if(value === null) {
										value = '';
									}
									if(value instanceof Object && value.hasOwnProperty('id')) {
										value = value.id;
									}
									formData.append(key, value);
								}
							});

							formData.append('cover', file);

							httpOptions = {
								transformRequest: angular.identity,
								headers: {
									'Content-Type': undefined
								}
							};

							if(!scope.createNew) {
								$http.put(api.items + '/' + main.contents.report.id, formData, httpOptions)
										.success(function(data, status){
											$state.go($state.current, {}, {reload: true});
										});
							} else {
								$http.post(api.items, formData, httpOptions)
										.success(function(data, status){

										});
							}

						},
						close: function() {
							$log.info('[%s] Closing.', main.settings.TAG);
							scope.show = false;
							main.states.wasAlreadyClosed = true;
							main.actions.chosen.unbindAll();
						},
						reset: function() {

							$log.info(main.settings.TAG, 'Reset form.');

							/**
							 * We need to remove 'viewValue' from our negative-validated inputs
							 * to be sure that everything is 'clean' now.
							 */

							var keys = Object.keys(scope.form);


							keys.forEach(function(key) {
								if(/^\$/.test(key) && scope.form[key] && scope.form[key].hasOwnProperty('$viewValue')) {
									delete scope.form[key].$viewValue;
								}
							});

							/**
							 * Reset validation
							 * results.
							 */
							scope.form.$setUntouched();
							scope.form.$setPristine();

							/**
							 * Clear view value
							 * for fields with bad validation.
							 */
							main.contents.report = null;
							main.contents.report = undefined;

							var edit = document.querySelector('.editReport');
							var inputs = document.querySelectorAll('input[type="text"]');

							var keys = Object.keys(inputs);
							keys.forEach(function(key) {
								inputs[key].value = '';
							});

						}
					};

					main.watchers = {
						collection: new Watchers(scope, [
							{
								model: ['report', 'createNew'],
								watch: function (newObj, oldObj, scope) {
									if (typeof newObj !== 'undefined' && newObj !== null) {

										/**
										 * Check if every model is defined.
										 */
										var isUndefined = function (value) {
											return typeof value === 'undefined';
										};

										if (!newObj.some(isUndefined)) {
											if (!newObj[1]) {

												$timeout(function () {
													main.contents.report = angular.copy(scope.report);
													scope.form.$setUntouched();
													scope.form.$setPristine();
												});
											}
										}

									}
								}
							},
							{
								model: 'show',
								watch: function(newValue, oldValue) {
									if (typeof newValue !== 'undefined' && newValue !== null && main.states.wasAlreadyClosed) {

										try {
											delete main.contents.report;
											delete main.contents.categories;
											delete main.contents.docTypes;
											delete main.contents.accessGroups;
										} catch(e) {
											$log.error('Could not delete the necessary properties: [%O]', e);
										}

										main.contents.categories = angular.copy(origin.categories);
										main.contents.docTypes = angular.copy(origin.docTypes);
										main.contents.accessGroups = angular.copy(origin.accessGroups);
										main.contents.report = angular.copy(scope.report);
									}

									main.actions.chosen.unbindAll();
									main.actions.chosen.bindAll();
								}
							}
						])
					};

					main.load.all();
					scope.$on('$destroy', main.actions.destroy);
				}
			};
		});
