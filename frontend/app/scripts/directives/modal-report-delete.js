'use strict';

/**
 * @ngdoc directive
 * @name mainApp.directive:modalDelete
 * @description
 * # modalDelete
 */
angular.module('mainApp')
		.directive('modalReportDelete', function ($http, api, $state) {
			return {
				templateUrl: 'templates/directive-modal-report-delete.html',
				restrict: 'E',
				scope: {
					title: '=ngReportTitle',
					id: '=ngReportId',
					show: '=ngCustomShow'
				},
				controller: function($scope) {
					var main = this;
					main.actions = {
						remove: function() {
							$http.delete(api.items + '/' + $scope.id)
								.success(function(data, status){
									$scope.show = false;
									$state.go('dashboard');
								});
						}
					};
				},
				controllerAs: 'main',
				link: function postLink(scope, element, attrs) {
					if(!attrs.ngReportTitle || !attrs.ngReportId) {
						throw new Error('You need to define two attributes with this directive: "ng-report-title" and "ng-report-id"!');
					}
				}
			};
		});
