'use strict';

/**
 * @ngdoc directive
 * @name mainApp.directive:maintenance
 * @description
 * # maintenance
 */
angular.module('mainApp')
		.directive('maintenance', function ($http, api, $rootScope) {
			return {
				templateUrl: 'templates/directive-maintenance.html',
				restrict: 'A',
				link: function postLink(scope, element) {

					var main = scope;

					element = element[0] || element;

					main.contents = {
						isActive: false
					};

					scope.$on('maintenance', function(event, arg) {
						main.contents = arg;

						if (arg.isActive) {
							element.classList.remove('hide');
                            $rootScope.$broadcast('refreshPositionMenu', 125);
						} else {
							element.classList.add('hide');
                            $rootScope.$broadcast('refreshPositionMenu', 65);
						}
					});

					/**
					 * Check for any pending maintence.
					 */
					$http.get(api.maintenance.get)
							.success(function(info) {
								if (info.hasOwnProperty('isActive')) {
									main.contents = info;
									if (info.isActive) {
										element.classList.remove('hide');
                                        $rootScope.$broadcast('refreshPositionMenu', 125);
									}
								}
							});
				}
			};
		});
