'use strict';

/**
 * @ngdoc service
 * @name mainApp.User
 * @description
 * # User
 * Service in the mainApp.
 */
angular.module('mainApp')
		.service('User', function User($rootScope, $cookies, $timeout) {
			var user = {
				/**
				 * 'data' is an object,
				 * where we store the data
				 * that we receive from API
				 * after the user logs-on
				 * to our application.
				 *
				 * Initially this object
				 * should be empty.
				 */
				data: {},
				setData: function(data, broadcast) {
					user.data = data;

					/**
					 * Now we need to broadcast this change
					 * to the <navigation> directive and 'appui' controller.
					 */
					if (broadcast === undefined || broadcast ) {
                        $timeout(function() {
                            $rootScope.$broadcast('user', user.data);
                        });
                    }
				},
				getData: function() {
					return user.data;
				},
				isSuperUser: function() {
					return user.getData().isSuperUser;
				},

                sessionExpireTime: function() {
                    return user.getData().token_expires - new Date().getTime();
                },

				/**
				 * Custom cookies,
				 * where the cookie's key
				 * contain 'username' value.
				 */
				cookies: {
					getCustomKey: function(key) {
						return user.data.username + '|' + key;
					},
					get: function(key) {
						return $cookies[user.cookies.getCustomKey(key)] || undefined;
					},
					set: function(key, value) {
						$cookies[user.cookies.getCustomKey(key)] = value;
					},
					remove: function(key) {
						$cookies[user.cookies.getCustomKey(key)] = undefined;
					}
				}
			};

			return user;
		});
