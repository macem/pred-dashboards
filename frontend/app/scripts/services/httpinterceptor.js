'use strict';

/**
 * @ngdoc service
 * @name mainApp.httpInterceptor
 * @description
 * # httpInterceptor
 * Factory in the mainApp.
 */
angular.module('mainApp')
		.factory('httpInterceptor', function ($q, GlobalSelectors, api, notification, language, $injector, $log, $timeout, User, preloader) {
            var SIGIN_ROUTE = 'signin';

			/**
			 * It is not desirable to show
			 * the preloader on every request.
			 *
			 * Some requests, like 'signOut',
			 * have their own 'preloading' mechanism
			 * so there is no need to duplicate
			 * such a functionality.
			 */

			/**
			 * Main Config object.
			 *
			 * Exclude HTML templates from being preloaded
			 * on GUI area?
			 * @type {{excludePreloadingHTML: boolean}}
			 */
			var config = {
				excludePreloadingHTML: true
			};

			var TAG = 'httpInterceptor';

			/**
			 * Get the 'excluded' list from
			 * 'api' service.
			 */
			var excluded = api.excludePreloaderFor();

			/**
			 * Exclude error notification
			 * for these API calls.
			 */
			var excludeErrorsFor = api.excludeErrorsFor();

			/**
			 * Function to check if
			 * given url is not excluded.
			 * @param url
			 * @returns {boolean}
			 */
			var isExcluded = function(url) {
				url = url || '';
				return excluded.indexOf(url) >= 0 || (config.excludePreloadingHTML ? /(.html)$/ig.test(url) : false);
			};

			var isErrorExcluded = function(url) {
				url = url || '';
				return excludeErrorsFor.indexOf(url) >= 0;
			};

			var requests = {
				collection: [],
				add: function(url) {
					preloader.show();
					this.collection.push(url);
				},
				remove: function(url) {
					this.collection = _.without(this.collection, url);
					this.checkPending();
				},
				removeAll: function() {
					this.collection.length = 0;
					this.checkPending();
				},
				checkPending: function() {
					var _self = this;
					if(_self.collection.length === 0) {
						preloader.hide();
					}
					$log.info('Pending requests: %d', this.collection.length || 0);
				}
			};

			return {
				'request': function(config) {

					if (!isExcluded(config.url)) {
                        requests.add(config.url);
                    }

					/**
					 * Set timeout
					 */
                    console.log('-> request: ', User.getData().name, User.getData().token_expires - new Date().getTime());

					/**
					 * Show the preloader whenever the valid HTTP request
					 * is pending.
					 */
					return $timeout(function() {
						return config;
					}, 100);
				},
				'requestError': function(rejection) {
					/**
					 * Hide the preloader if any error appea.
					 */

					requests.remove(rejection.config.url);

					return $q.reject(rejection);
				},
				'response': function(response) {
					requests.remove(response.config.url);
					return response;
				},
				'responseError': function(rejection) {
					var message,
						status,
						connectionError,
                        noHideMessage = false;

                    var params = new URLSearchParams(window.location.search);
                    var code = params.get('code');
                    if (code) {
                        window.sessionStorage.setItem('key', code);
                    }

					requests.removeAll();

					connectionError = document.getElementById('connectionError');
					if (connectionError !== null) {
						connectionError.classList.add(GlobalSelectors.hide);
					}

					/**
					 * We want to redirect users to the 'signin'
					 * page when the response code is === 403 or === 401.
					 */
					if ((rejection.status === 403 || rejection.status === 401) && !code) {
    					console.log('ERROR: http error sigin');
                        var state = $injector.get('$state');

						if (state.current.name !== SIGIN_ROUTE) {
                            User.setData({});
                            state.go(SIGIN_ROUTE);
                        } else {
                            User.setData({}, false);
                        }
					}

					/**
					 * Usually, our back-end API
					 * should return an error not only
					 * as a server 'status code' but
					 * also as a JSON object.
					 */

					/**
					 * Check if we have a JSON object
					 * with any error code & message...
					 * If not, we will rely on
					 * server's native response status and message.
					 */
                    if (!angular.isObject(rejection.data) || Object.keys(rejection.data).length === 0) {
                        status = rejection.status;
                        message = status + ': ' + rejection.statusText;
                    } else {
                        status = rejection.data.code || rejection.code;
                        message = status + ': ' + (typeof rejection.data.message === 'object'
                            ? rejection.data.message.summary
                            : (rejection.data.message || rejection.data.error_description));
                    }

					/**
					 * Catch 'no internet connection' error.
					 * It happens when status code is equal 0 (ZERO).
					 */
					switch (status) {
                        case -1:
                            message = language.apiError;
                            noHideMessage = true;
                            break;
						case 0:
							message = language.noInternetConnection;
							if (connectionError !== null) {
								connectionError.classList.remove(GlobalSelectors.hide);
							}
                            noHideMessage = true;
							break;
						case 403:
							message = language.invalidLogin;
							break;
                        case 401:
                            message = language.invalidLogin;
                            break;
                        case 500:
                            message = language.serverError;
                            break;
					}

                    console.log('Err ->', rejection);

					/**
					 * Call notification service
					 * to display the proper error notification.
					 *
					 * It will only happen when requested
					 * url is not excluded from error notifications.
					 *
					 * Please see the 'api' service for more
					 * details.
					 */
					if (!isErrorExcluded(rejection.config.url) && !code && (status != 403 && status != 401) || status === 500) {
						console.log('ERROR: ', status);
                        notification.push(message, notification.types.error, noHideMessage);
					}

					/**
					 * Hide the preloader if we have the response error.
					 */
					/*if(!preloader.classList.contains(GlobalSelectors.hide)) {
						preloader.classList.add(GlobalSelectors.hide);
					}*/
					return $q.reject(rejection);
				}
			};
		});
