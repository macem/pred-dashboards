'use strict';

/**
 * @ngdoc service
 * @name mainApp.notification
 * @description
 * # notification
 * Service in the mainApp.
 */
angular.module('mainApp')
		.service('notification', function notification($timeout) {

			/**
			 * Init empty notification collection.
			 * It will be used to store timeouts
			 * that are responsible for removing the old
			 * elements (efter few seconds);
			 * @type {Array}
			 */
			var notifyCollection = [],

			/**
			 * Selectors.
			 * REMEMBER: some of those selectors are
			 * connected with CSS part.
			 * Be careful in editing it.
			 * @type {string}
			 */
			selectors = {
				messageIdPrefix: 'notification-custom-',
				messageDefaultClass: 'notification-default',
				notificationService: 'notification-service',
				iconHolder: 'icon-holder',
				messageBody: 'notification-body',
				materialDesignIcon: 'mdi',
				materialDesignIconPref: 'mdi-'
			},

			/**
			 * We need to create the unique
			 * ID for each notification message.
			 * @type {number}
			 */
			currentMessageId = 0,

			/**
			 * Expire notification after
			 * X ms...
			 * @type {number}
			 */
			notifyExpirationAfter = 6000;

			return {
				init: function() {
					var notifyHolder = document.createElement('div');
					notifyHolder.setAttribute('id', selectors.notificationService);
					document.body.appendChild(notifyHolder);
				},
				types: {
					alert: 'alert',
					warning: 'warning',
					error: 'error'
				},
				push: function(messageBody, messageType, messageShow) {
					currentMessageId++;
					var className,
						iconName;

					/**
					 * Set 'alert' type as default
					 * in case if messageType attribute is not
					 * defined;
					 * @type {*|string}
					 */
					messageType = messageType || this.types.alert;

					switch(messageType) {
						case this.types.alert:
							className = 'blue';
							iconName = 'bell';
							break;
						case this.types.warning:
							className = 'orange';
							iconName = 'flag-variant';
							break;
						case this.types.error:
							className = 'red';
							iconName = 'flash';
							break;
						default:
							className = 'silver';
							iconName = 'comment-outline';
					}

					/**
					 * In this step we want to check
					 * if there is any pending message
					 * which is the same as the new one
					 * that we want to push.
					 */
					var existingElements = document.getElementById(selectors.notificationService).querySelectorAll('.' + className);
					var isAnythingExisting = false;

					for(var element in existingElements) {
						if(existingElements.hasOwnProperty(element) && element !== 'length') {
							var node = existingElements[element];
							if(node.textContent === messageBody) {
								isAnythingExisting = true;
							}
						}
					}
					/**
					 * Pushing new notification
					 * to the notifyCollection array.
					 */
					if(!isAnythingExisting) {
						notifyCollection.push((function () {

							/**
							 * Define variables.
							 */
							var message,
									icon,
									iconHolder,
									body;

							/**
							 * Create main notification container.
							 */
							message = document.createElement('div');
							message.setAttribute('id', selectors.messageIdPrefix + currentMessageId);
							message.classList.add(selectors.messageDefaultClass);
							message.classList.add(className);

							/**
							 * Create a proper icon.
							 */
							icon = document.createElement('i');
							icon.classList.add(selectors.materialDesignIcon, selectors.materialDesignIconPref + iconName);

							/**
							 * Create icon container.
							 */
							iconHolder = document.createElement('div');
							iconHolder.classList.add(selectors.iconHolder);
							iconHolder.appendChild(icon);

							/**
							 * Create notification's body.
							 * This is the place where notification
							 * text will be placed in.
							 */
							body = document.createElement('div');
							body.classList.add(selectors.messageBody);
							body.innerHTML = messageBody;

							/**
							 * Add containers
							 * to the main notification container.
							 */
							message.appendChild(iconHolder);
							message.appendChild(body);


							document.getElementById(selectors.notificationService).appendChild(message);

							/**
							 * Destroy notification
							 * after 'notifyExpirationAfter' (ms)
							 */
                            if (!messageShow) {
                                $timeout(function () {
                                    /**
                                     * Using jQuery's animation
                                     * to remove the element from DOM
                                     * in a nice way.
                                     */
                                    $(message).fadeOut(function () {
                                        message.parentNode.removeChild(message);
                                    });
                                }, notifyExpirationAfter);
                            }
						})());
					}
				}
			};
		});
