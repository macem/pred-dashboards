'use strict';

/**
 * @ngdoc service
 * @name mainApp.MashupPolicy
 * @description
 * # MashupPolicy
 * Constant in the mainApp.
 */
angular.module('mainApp')
		.constant('MashupPolicy', {
			/**
			 * Put the valid docType's
			 * flagId to the array collection.
			 * e.g.: [1, 2]
			 */
			disallowReportTypes: [3, 4]
		});
