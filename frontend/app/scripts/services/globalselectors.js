'use strict';

/**
 * @ngdoc service
 * @name mainApp.GlobalSelectors
 * @description
 * # GlobalSelectors
 * Service in the mainApp.
 */
angular.module('mainApp')
		.service('GlobalSelectors', function GlobalSelectors() {
			return {
				preloader: 'mainPreloader',
				hide: 'hide',
				mainHeader: 'mainHeader',
				star: {
					empty: 'icon--star-2',
					full: 'icon--star-1'
				},
				box: {
					hovered: 'box--hovered',
					description: 'div.box__description'
				},
				switch: 'input.js-switch',
				switchery: 'span.switchery'
			};
		});
