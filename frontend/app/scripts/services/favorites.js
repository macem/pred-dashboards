'use strict';

/**
 * @ngdoc service
 * @name mainApp.favorites
 * @description
 * # favorites
 * Service in the mainApp.
 */
angular.module('mainApp')
		.service('favorites', function favorites(api, $http) {
			var favorites = {
				data: {},
				fav: [],
				toggle: function(event, id, callback) {

					callback = callback || function(c) {
						return c;
					};

					$http.put(api.favorites + '/' + id)
						.success(function(data, status, headers, config) {
								favorites.update(callback);
						});
				},
				mapData: function(data) {
					var output = {};
					return _.indexBy(data, 'id');
				},
				update: function(callback) {
					callback = callback || function(c) {
						return c;
					};

					$http.get(api.favorites)
						.success(function(data, status, headers, config) {
							favorites.data = favorites.mapData(data.contents);
							favorites.fav = data.contents;
							callback(favorites.data, favorites.fav);
						});
				}
			};

			return favorites;
		});
