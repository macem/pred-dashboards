'use strict';

/**
 * @ngdoc service
 * @name mainApp.preloader
 * @description
 * # preloader
 * Service in the mainApp.
 */
angular.module('mainApp')
		.service('preloader', function preloader(GlobalSelectors) {
			var preloader = document.getElementById(GlobalSelectors.preloader);

			return {
				show: function() {
          if (!preloader.classList) {
            preloader.classList = [];
          }
					if(preloader.classList && preloader.classList.contains(GlobalSelectors.hide)) {
						preloader.classList.remove(GlobalSelectors.hide);
					}
				},
				hide: function() {
          if (!preloader.classList) {
            preloader.classList = [];
          }
					if(preloader.classList && !preloader.classList.contains(GlobalSelectors.hide)) {
						preloader.classList.add(GlobalSelectors.hide);
					}
				}
			};
		});
