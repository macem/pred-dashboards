'use strict';

/**
 * @ngdoc service
 * @name mainApp.api
 * @description
 * # api
 * Service in the mainApp.
 */
angular.module('mainApp')
		.service('api', function api(ENV) {
			var mainAPI = {
				signIn: ENV.apiEndpoint + '/Signin',
				signOut: ENV.apiEndpoint + '/Signout',
				currentUser: ENV.apiEndpoint + '/Signin/currentUser',
                refreshUser: ENV.apiEndpoint + '/Signin/refresh',
                config: ENV.apiEndpoint + '/Signin/config',
				categories: ENV.apiEndpoint + '/Categories',
				categoriesFull: ENV.apiEndpoint + '/Categories',
				categoriesShort: ENV.apiEndpoint + '/Categories/list',
				categoriesSort: ENV.apiEndpoint + '/Categories/sort',
                categoryDisable: ENV.apiEndpoint + '/Categories/disable',
                categoryEnable: ENV.apiEndpoint + '/Categories/enable',
				categoriesContentOrdering: ENV.apiEndpoint + '/Categories/contentOrdering',
				items: ENV.apiEndpoint + '/Items',
				itemsCount: ENV.apiEndpoint + '/Items/count',
				itemsDisable: ENV.apiEndpoint + '/Items/disable',
				itemsEnable: ENV.apiEndpoint + '/Items/enable',
				docTypes: ENV.apiEndpoint + '/DocTypes',
				uploader: ENV.apiEndpoint + '/Items/uploader',
				cover: ENV.apiEndpoint + '/Items/cover',
				avatar: ENV.apiEndpoint + '/User/photo',
				favorites: ENV.apiEndpoint + '/Favorites',
				favoritesOrder: ENV.apiEndpoint + '/Favorites/order',
				mashups: ENV.apiEndpoint + '/Mashups',
				mashupsAdd: ENV.apiEndpoint + '/Mashups/add',
				accessGroups: ENV.apiEndpoint + '/AccessGroups',
				accessGroupsOrder: ENV.apiEndpoint + '/AccessGroups/order',
				maintenance: {
					get: ENV.apiEndpoint + '/Maintenance',
					set: ENV.apiEndpoint + '/Maintenance/setStatus'
				},
				/**
				 * Exclude one or more API call from
				 * being preloaded on UI.
				 *
				 * E.g.:
				 * [mainAPI.signOut, mainAPI.currentUser]
				 *
				 * We compare this data in 'httpinterceptor' service.
				 * @returns {*[]}
				 */
				excludePreloaderFor: function() {
					return [mainAPI.signOut, mainAPI.currentUser, mainAPI.refreshUser];
				},

				/**
				 * Exclude one or more API call from
				 * being printed as the notification
				 * message in the UI.
				 *
				 * E.g.:
				 * [mainAPI.currentUser]
				 * @returns {*[]}
				 */
				excludeErrorsFor: function() {
					return [mainAPI.currentUser];
				}
			};

			return mainAPI;
		});
