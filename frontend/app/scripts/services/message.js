'use strict';

/**
 * @ngdoc service
 * @name mainApp.message
 * @description
 * # message
 * Service in the mainApp.
 */
angular.module('mainApp')
		.service('message', function message($compile, $rootScope) {
			return function (title, message, type) {
				type = type || 'success';

				var container = document.getElementById('mainView'),
					output = $compile('<message title="' + title + '" type="' + type + '">' + message + '</message>')($rootScope);

				try {
					setTimeout(function() {
						container.insertBefore(output[0], container.firstChild);
					},250);
				} catch (e) {
					console.log('Cannot print a new message!');
				}
			};
		}
);
