'use strict';

/**
 * @ngdoc service
 * @name mainApp.language
 * @description
 * # language
 * Constant in the mainApp.
 */
angular.module('mainApp')
		.constant('language', {
			DOMElementNotFound: 'I could not find one of the following DOM elements: ',
			noInternetConnection: 'The connection to the server failed.',
			DOMErrorDashboard: 'DOM error. Details: ',
			oneReport: 'report',
			manyReports: 'reports',
			error: 'Error: ',
			item: 'item',
			items: 'items',
			invalidCredentials: 'Please check your user name or password and try again.',
            invalidLogin: 'You are not logged in. Please log in using pREDiLogin',
            serverError: 'Sorry but there is a problem with server, try again later.',
            apiError: 'Sorry but api is not working correctly, please restart api server.'
		});
