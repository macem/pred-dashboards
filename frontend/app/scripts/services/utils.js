'use strict';

/**
 * @ngdoc service
 * @name mainApp.utils
 * @description
 * # utils
 * Service in the mainApp.
 */
angular.module('mainApp')
		.service('utils', function utils($state, $timeout, ipCookie, User, $log) {
			var utils = {
				openInNewWindow: [3, 4], // doctypes
				escapeRegExp: function(string) {
					return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
				},
				openReport: function(reportId, url, docType) {
					var openOutsideFrame = utils.openInNewWindow;

					if(openOutsideFrame.indexOf(parseInt(docType)) >= 0) {
						window.open(url, '_new');
					} else {
						$state.go('report', {id: reportId});
					}
				},
				isExternalResource: function(reportId, url, docType) {
					var openOutsideFrame = utils.openInNewWindow;
					return openOutsideFrame.indexOf(parseInt(docType)) >= 0;
				},
				toggleFullScreen: function(element) {
					if (!document.mozFullScreen && !document.webkitFullScreen) {
						if (element.mozRequestFullScreen) {
							element.mozRequestFullScreen();
						} else {
							element.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
						}
					} else {
						if (document.mozCancelFullScreen) {
							document.mozCancelFullScreen();
						} else {
							document.webkitCancelFullScreen();
						}
					}
				},
				openInFullMode: function(element) {
					utils.toggleFullScreen(element);
				},
				removeFromDOM: function(element) {
					return element.parentElement.removeChild(element);
				},
				cookies: {
					set: function(key, value) {
						if(!key) {
							throw new Error('Cookie\'s key was not specified!');
						}

						value = value || '';

						if(value instanceof Object) {
							try {
								value = JSON.stringify(value);
							} catch(e) {
								$log.warn(e);
							}
						}

						return ipCookie(key, value, { expires: 365 });
					},
					get : function(key) {
						if(!key) {
							throw new Error('Cookie\'s key was not specified!');
						}

						var data = ipCookie(key);
						if(/(^{(.*)}$|^\[(.*)\]$)/g.test(data)) {
							try {
								data = JSON.parse(data);
							} catch (e) {
								$log.warn(e);
							}
						}

						return data || undefined;
					},
          remove: function (key, options) {
            if (!options) {
              options = {};
            }
            return ipCookie.remove(key, options);
          }
				}
			};

			return utils;
		});
