'use strict';

/**
 * @ngdoc service
 * @name mainApp.switchery
 * @description
 * # switchery
 * Constant in the mainApp.
 */
angular.module('mainApp')
		.constant('switchery', {
			color: '#019966',
			secondaryColor: '#ccc',
			size: 'small'
		});
