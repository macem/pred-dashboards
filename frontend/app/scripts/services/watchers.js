'use strict';

/**
 * @ngdoc service
 * @name mainApp.watchers
 * @description
 * # watchers
 * Service in the mainApp.
 */
angular.module('mainApp')
		.service('Watchers', function watchers($log) {

			var Watchers = function(scope, collection) {
				if(!scope || !collection) {
					throw new Error('Watchers requires \'scope\' and \'collection\' attributes!');
				}

				if(!Array.isArray(collection)) {
					throw new TypeError('\'collection\' attribute need to be an array!');
				}

				this.scope = scope;
				this.watchers = [];

				this.register(collection);
			};

			Watchers.prototype.register = function(collection) {

				var _self = this;

				if(!Array.isArray(collection)) {
					throw new TypeError('Collection must be an array');
				}

				$log.info('Registering watchers:');

				collection.forEach(function(watcher) {
					if(!watcher.hasOwnProperty('model') || !watcher.hasOwnProperty('watch')) {
						throw new TypeError('Empty \'model\' or/and \'watch\' property.');
					}

					var finalWatcher;

					if(watcher.model instanceof Array) {
						finalWatcher = _self.scope.$watchGroup(watcher.model, watcher.watch, true);
					} else {
						finalWatcher = _self.scope.$watch(watcher.model, watcher.watch, true);
					}

					if(_self.watchers.push(finalWatcher)) {
						$log.info('\t- \'%O\' => OK.', watcher.model);
					} else {
						$log.warn('\t- \'%O\': Error.', watcher.model);
					}
				});

			};

			Watchers.prototype.unregister = function() {
				$log.info('Unregistering watchers.');
				var _self = this;

				this.watchers.forEach(function(watcher, index) {
					watcher();

					if(++index === _self.watchers.length) {
						_self.watchers.length = 0;
					}
				});
			};

			return Watchers;

		});
