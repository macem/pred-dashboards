"use strict";

 /**
	WARNING: This file is generated automatically.
	You should NEVER provide any changes to this manually!
	All the magic happens in the Gruntfile.js file.
**/

angular.module('config', [])

.constant('ENV', {package:{version:'2.0.0',description:'pRED Portfolio Dashboard'},name:'development',apiEndpoint:'http://preddashboard-dev.roche.com:1337/api/v1'})

;