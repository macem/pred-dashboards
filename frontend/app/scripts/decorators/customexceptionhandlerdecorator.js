'use strict';

/**
 * @ngdoc function
 * @name mainApp.decorator:Customexceptionhandler
 * @description
 * # Customexceptionhandler
 * Decorator of the mainApp
 */
angular.module('mainApp')
		.config(function ($provide) {
			/**
			 * This will display an alert message when things go wrong
			 * on the code side (error exceptions).
			 *
			 * TODO To consider: sending error messages to some endpoint?
			 */
			var modal = document.getElementById('modal-error');
			$provide.decorator('$exceptionHandler',
					function ($log, $delegate) {
						return function (exception, cause) {
							if(modal) {
								var textNode = modal.querySelector('div.log');
								modal.classList.remove('hide');
								textNode.textContent = exception.stack;
							}
							$delegate(exception, cause);
						};
					}
			);
		});