'use strict';

/**
 * @ngdoc function
 * @name mainApp.controller:AdministrationCtrl
 * @description
 * # AdministrationCtrl
 * Controller of the mainApp
 */
angular.module('mainApp')
		.controller('AdministrationCtrl', function ($scope, $rootScope, $http, api, $interval, $timeout, $state, User) {

			var TAG,
				tabs,
				cookiesKeys;

			/**
			 * Unique ID for the main
			 * controller.
			 * @type {string}
			 */
			TAG = 'Admin';

			/**
			 * Define indexes for
			 * each tabs.
			 *
			 * We will use this indexes
			 * to set the active tab,
			 * regarding to actual state.
			 * @type {{reports: number, categories: number, groups: number}}
			 */
			tabs = {
				reports: 0,
				categories: 1,
				types: 2,
				groups: 3,
				maintenance: 4
			};

			/**
			 * Keys for stored cookies.
			 * @type {{introMessage: string}}
			 */
			cookiesKeys = {
				introMessage: TAG + '|introMessage'
			};

			$scope.activeTab = 0;

			/**
			 * There is nothing to show
			 * on default 'administration' state,
			 * so we need to redirect user to the
			 * first tab (reports).
			 */
			if($state.current.name === 'administration') {
				$state.go('administration.reports');
			} else {
				/**
				 * Set active tab index
				 */
				$scope.activeTab = tabs[$state.current.name.split('.')[1]];
			}


			/**
			 * States for some
			 * DOM elements.
			 * @type {{}}
			 */
			$scope.states = {
				message: angular.isDefined(User.cookies.get(cookiesKeys.introMessage)) ? JSON.parse(User.cookies.get(cookiesKeys.introMessage)) : true,
			};

			/**
			 * Define main actions.
			 */
			$scope.action = {
				closeMessage: function() {
					User.cookies.set(cookiesKeys.introMessage, false);
					$scope.states.message = false;
				}
			};

			/**
			 * DOM selectors
			 * @type {{coverPhoto: string}}
			 */
			var selectors = {
				coverPhoto: 'coverPhoto'
			};

			$scope.form = {
				title: 'Title XXXX',
				descriptionShort: 'Description YYYY',
				embedURL: 'http://'
			};

			/**
			 * Getting all categories
			 * to be places in category selector
			 * in our view.
			 */
			$http.get(api.categoriesShort)
					.success(function(data, status, headers, config) {
						$scope.categories = data;
					});

			/**
			 * Get doc types.
			 */
			$http.get(api.docTypes)
					.success(function(data, status, headers, config) {
						$scope.docTypes = data;
					});

			/**
			 * Bind event for 'add item'
			 * button.
			 */
			$scope.addItem = function() {

				/**
				 * Reference to 'coverPhoto' file input.
				 */
				var file = document.getElementById(selectors.coverPhoto);

				/**
				 * Select only the first one
				 * (there should be always only one picture
				 * in 'files' collection since the file input
				 * has no multiple option enabled.
				 */
				file = file.files[0];

				/**
				 * The FormData object lets you compile a set of key/value pairs to send using XMLHttpRequest.
				 * It is primarily intended for use in sending form data, but can be used independently from
				 * forms in order to transmit keyed data. The transmitted data is in the same format that
				 * the form's submit() method would use to send the data if the form's encoding type
				 * were set to multipart/form-data.
				 * @type {Window.FormData}
				 */
				var formData = new FormData();

				/**
				 * Append the necessary data to
				 * the FormData object.
				 *
				 * REMEMBER: you should always append your
				 * plain-text data first.
				 *
				 * THEN you can append your 'file' object.
				 */
				formData.append('title', $scope.form.title);
				formData.append('descriptionShort', $scope.form.descriptionShort);
				formData.append('embedURL', $scope.form.embedURL);
				formData.append('docType', $scope.form.docType);
				formData.append('category', $scope.form.category);

				/**
				 * Now we can append our 'file' object.
				 */
				formData.append('cover', file);

				/**
				 * Send our request
				 * to the proper API method.
				 */
				$http.post(api.items, formData, {
					transformRequest: angular.identity,
					headers: {
						'Content-Type': undefined
					}
				})
				.success(function(){
						$scope.form = {
							title: undefined,
							descriptionShort: undefined,
							embedURL: undefined,
							docType: undefined
						}
				});
			};

			/**
			 * Register '$stateChangeStart' event.
			 * @type {function()|*}
			 */
			var stateListener = $rootScope.$on('$stateChangeStart', function(event, toState) {
				$scope.activeTab = tabs[toState.name.split('.')[1]];
			});

			/**
			 * !!! When this view is destroyed,
			 * we need to unregister '$stateChangeListener' !!!
			 */
			$scope.$on('$destroy',function() {
				stateListener();
			});
		});
