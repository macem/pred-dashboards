'use strict';

/**
 * @ngdoc function
 * @name mainApp.controller:CategoryCtrl
 * @description
 * # CategoryCtrl
 * Controller of the mainApp
 */
angular.module('mainApp')
		.controller('CategoryCtrl', function ($scope, $stateParams) {
			var main = this;

			/**
			 * Getting the all parameters
			 * passed through an URL.
			 * @type {{selectedCategoryId: *}}
			 */
			main.input = $stateParams;



		});
