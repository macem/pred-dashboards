'use strict';

/**
 * @ngdoc function
 * @name mainApp.controller:ReportCtrl
 * @description
 * # ReportCtrl
 * Controller of the mainApp
 */
angular.module('mainApp')
		.controller('ReportCtrl', function ($scope, $timeout, $stateParams, $state, $http, api, favorites, GlobalSelectors, switchery, utils, User) {

			var main = this;

			main.attributes = {
				reportId: 'data-report-id',
				categoryId: 'category-id'
			};

			main.contents = {
				states: {
					enabled: 'Enabled',
					disabled: 'Disabled',
					modalDelete: false,
					modalEdit: false
				}
			};

			favorites.update(function(data, favs) {
				main.contents.favData = data;
				main.contents.favorites = favs;
			});

			main.settings = {
				TAG: 'ReportCtrl',
				switcher: switchery,
				isAdmin: User.isSuperUser()
			};

			$scope.$on('user', function(event, data) {
				main.settings.isAdmin = User.isSuperUser();
			});

			main.selectors = {
				currentState: 'label#currentState'
			};

			main.hasAdminRole = function() {
				return main.settings.isAdmin/* && main.frame.sharepointAccess*/;
			};

			main.actions = {
				admin: {
					remove: function(reportId) {
						main.contents.states.modalDelete = true;
					},
					edit: function() {
						main.contents.states.modalEdit = true;
					}
				},
				orderReports: function() {

				},
				getFullModeUrl: function(url) {
					return url && url.replace('action=embedview', 'action=view');
				},
				openInFullMode: function(reportId) {
					var element = document.querySelector('iframe[frame-report-id="' + reportId + '"]');
					utils.openInFullMode(element);
				},
				getUserData: function() {
					return Object.keys(User.data).length > 0;
				},
                checkIsSharepoint: function(url) {
                    return url.includes('glob.1sharepoint.roche.com');
                },
                getAuth: function() {
					return "https://glob.1sharepoint.roche.com/team/pRED-Portfolio-Dashboard/Dashboard%20Pictures/access-pred.jpg?d=" + new Date().getTime();
				},
				toggleFavorites: function(event, id) {
					event.stopPropagation();
					event.preventDefault();

					favorites.toggle(event, id, function(data, favs) {
						main.contents.favData = data;
						main.contents.favorites = favs;
					});
				},
				disable: {
					set: function(state, recordId) {
						var method = state ? api.itemsDisable : api.itemsEnable;

						$http.get(method + '/' + recordId)
							.success(function(data, status){
								console.info(main.settings.TAG, 'Disabled: ' + state);
							});
					}
				},
				switchery: {
					collection: [],
					clickListener: function(event) {
						var switchInput = event.target.parentElement.parentElement.querySelector(GlobalSelectors.switch);

						if(switchInput.getAttribute('checked') === 'checked') {
							switchInput.setAttribute('checked', null);
						} else {
							switchInput.setAttribute('checked', 'checked');
						}

						var state = switchInput.getAttribute('checked') === 'checked' ? false : true;
						var recordId = switchInput.getAttribute(main.attributes.reportId);

						var label;

						if(!state) {
							label = main.contents.states.enabled;
						} else {
							label = main.contents.states.disabled;
						}

						document.querySelector(main.selectors.currentState).innerText = label;

						main.actions.disable.set(state, recordId);
					},
					bindAll: function() {
						main.actions.switchery.unbindAll(function() {
							$(GlobalSelectors.switch).each(function(idx, el) {
								var switchery = new Switchery(el, {
									color: main.settings.switcher.color,
									secondaryColor: main.settings.switcher.secondaryColor,
									size: main.settings.switcher.size
								});
								main.actions.switchery.collection.push(switchery);
							});

							var switchery = document.querySelectorAll(GlobalSelectors.switchery);
							var keys = Object.keys(switchery);

							keys.forEach(function(key) {
								if(angular.isObject(switchery[key])) {
									switchery[key].addEventListener('click', main.actions.switchery.clickListener);
								}
							});
						});
					},
					unbindAll: function(callback) {

						var total = main.actions.switchery.collection.length;

						if(total === 0 && angular.isDefined(callback)) {
							return callback();
						}

						main.actions.switchery.collection.forEach(function(switcher, index) {
							switcher.destroy();

							if(++index === total) {
								if(angular.isDefined(callback)) {
									main.actions.switchery.collection.length = 0;

									var switchery = document.querySelectorAll(GlobalSelectors.switchery);
									var keys = Object.keys(switchery);
									keys.forEach(function(key) {
										var element = switchery[key];

										if(angular.isObject(element)) {
											element.parentNode.removeChild(element);
										}
									});
									return callback();
								}
							}
						});
					}
				}
			};

			main.toggleFavorites = function(event, id) {
				event.stopPropagation();
				event.preventDefault();

				favorites.toggle(event, id, function(data, favs) {
					main.contents.favData = data;
					main.contents.favorites = favs;
				});
			};

			/**
			 * Set reference
			 * to iframe.
			 */

             main.frame = {
				sharepointAccess: false,
                //src: null,
				load: function() {
					var frame = document.getElementById('embedFrame');
					var image = document.createElement('img');
					var container = document.querySelector('.container');

                    /**
                     * Get report details.
                     */
                    $http.get(api.items + '/' + $stateParams.id)
                    .success(function(data) {
                        main.contents.report = data;
                        $timeout(function() {
                            if (main.actions.checkIsSharepoint(data.embedURL)) {
                                image.src = main.actions.getAuth();
                                image.className = "sharepoint-auth";
                                container.appendChild(image);

                                image.addEventListener('load', function() {
                                    main.frame.sharepointAccess = true;
                                    main.actions.switchery.bindAll();
                                    frame.setAttribute('src', data.embedURL);
                                });

                                image.addEventListener('error', function(e) {
                                    console.log('ERROR:', e);
                                });
                            } else {
                                main.actions.switchery.bindAll();
                                frame.setAttribute('src', data.embedURL);
                            }
                        }, 600);
                    })
                    .error(function(data, status, headers, config) {
                        if(status === 404) {
                            $state.go('notfound');
                        }
                        console.info('Cannot load data for report.');
                    });

					return frame;
				}
			};
			main.frame.load();
		});
