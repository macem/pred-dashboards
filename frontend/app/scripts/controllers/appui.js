'use strict';

/**
 * @ngdoc function
 * @name mainApp.controller:AppuiCtrl
 * @description
 * # AppuiCtrl
 * This controller is responsible for
 * managing <navigation> and <app-footer>.
 * Generally, these shouldn't be visible
 * on the Sign-In screen.
 */
angular.module('mainApp')
		.controller('AppuiCtrl', function ($scope) {
			$scope.visibility = false;

			$scope.$on('user', function(event, arg) {
				if (Object.keys(arg).length > 0) {
					$scope.visibility = true;
				} else {
					$scope.visibility = false;
				}
			});
		});
