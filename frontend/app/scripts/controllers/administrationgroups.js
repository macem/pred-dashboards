'use strict';

/**
 * @ngdoc function
 * @name mainApp.controller:AdministrationgroupsCtrl
 * @description
 * # AdministrationgroupsCtrl
 * Controller of the mainApp
 */
angular.module('mainApp')
		.controller('AdministrationgroupsCtrl', function ($scope, $http, api, $timeout) {
			var main,
				sortable;

			main = this;

			main.types = {
				form: {
					edition: 'edition'
				}
			};

			main.form = {
				edition: {},
				reset: function(formId) {
					if(main.form.hasOwnProperty(formId)) {
						console.info('Reset: ' + formId);

						/**
						 * We need to remove 'viewValue' from our negative-validated inputs
						 * to be sure that everything is 'clean' now.
						 */
						var keys = Object.keys($scope.form);
						keys.forEach(function(key) {
							if(/^\$/.test(key) && $scope.form[key] && $scope.form[key].hasOwnProperty('$viewValue')) {
								delete $scope.form[key].$viewValue;
							}
						});

						/**
						 * Reset validation
						 * results.
						 */
						$scope.form.$setUntouched();
						$scope.form.$setPristine();

						/**
						 * Clear view value
						 * for fields with bad validation.
						 */

						main.form[formId] = undefined;

						var edit = document.querySelector('.editReport');
						var inputs = document.querySelectorAll('input[type="text"]');

						var keys = Object.keys(inputs);
						keys.forEach(function(key) {
							if(key !== 'length') {
								inputs[key].value = '';
							}
						});

					}
				}
			};

			main.selectors = {
				group: 'div.group',
				groups: 'div.groups'
			};

			main.states = {
				removalModal: false,
				editionModal: false
			};

			main.attributes = {
				groupId: 'group-id'
			};

			main.contents = {
				activeRecord: null
			};

			main.ordering = {
				current: [],
				get: function() {
					return this.current;
				},
				update: function() {
					var groups = document.querySelectorAll(main.selectors.group);
					var keys = Object.keys(groups);

					main.ordering.current.length = 0;

					keys.forEach(function(key, index) {
						var group = groups[key];

						if(angular.isObject(group)) {
							main.ordering.current.push(group.getAttribute('group-id'));
						}

						if(++index === keys.length) {
							var options = {
								order: main.ordering.get()
							};

							$http.put(api.accessGroupsOrder, options);
						}
					});
				}
			};

			main.sortable = {
				update: function() {
					main.sortable.destroy();
					$('.js-sortable').each(function(idx, el) {
						sortable = new Sortable($(el)[0], {
							animation: 300,
							draggable: '.js-sortable__draggable',
							handle: '.js-sortable__handle',
							sort: !($(el).data('groupSortable') == ""),
							group: {
								name: $(el).data('groupName') || null,
								pull: $(el).data('groupPull') || true,
								put: $(el).data('groupPut') || true
							},
							onUpdate: function(event) {
								main.ordering.update();
							},
							onAdd: function(event) {
								main.ordering.update();
							}
						});
					});
				},
				destroy: function() {
					if(angular.isDefined(sortable)) {
						console.info('sortable: destroyed.');
						sortable.destroy();
					}
				}
			};

			main.actions = {
				tags: {
					collection: [],
					update: function() {
						main.actions.tags.collection.length = 0;
						var elements = document.querySelector('ul.chosen-choices').querySelectorAll('li');
						for(var tag in elements) {
							if(elements.hasOwnProperty(tag) && tag !== 'length') {
								if(elements[tag].textContent.length) {
									main.actions.tags.collection.push(elements[tag].textContent);
								}
							}
						}
						main.form.edition.aciveDirectoryId = main.actions.tags.collection.join(',');
					},
					load: function() {
						var dropdown = document.querySelector('[name="reportAccessGroup"]');
						var contents = main.form.edition.aciveDirectoryId.split(',');

						dropdown.innerHTML = '';

						contents.forEach(function(tag) {
							var select = document.createElement('option');
							select.setAttribute('selected', true);
							select.textContent = tag;
							dropdown.appendChild(select);
							$('[name="reportAccessGroup"]').trigger('chosen:updated');
						});
					},
					unload: function() {
						document.querySelector('[name="reportAccessGroup"]').innerHTML = '';
						$('.search-choice').remove();
					}
				},
				bind: {
					tags: function() {
						$("#state-admin-groups .js-select, #state-admin-groups .js-select--tags").chosen({
							disable_search_threshold: -1,
							search_contains: true
						});

						$('#state-admin-groups .js-select--tags + .chosen-container input.default').on('keydown', function(e) {
							var keycode = (e.keyCode ? e.keyCode : e.which);
							var value = $(this).val();

							var keys = [188, 13, 32];

							if(keys.indexOf(keycode) >= 0) {
								$(this).parents('.chosen-container').prev().
										append('<option selected="true">' + value + '</option>').
										trigger('chosen:updated');
								setTimeout(function() {
									$('li.search-field input').val('');
								});

							}
						});

					},
					all: function() {
						for(var toBind in main.actions.bind)  {
							if(main.actions.bind.hasOwnProperty(toBind) && toBind !== 'all') {
								main.actions.bind[toBind]();
							}
						}
					}
				},
				activeRecord: {
					get: function() {
						return main.contents.activeRecord || null;
					},
					set: function(recordId) {
						main.contents.activeRecord = recordId;
					},
					clear: function() {
						main.contents.activeRecord = null;
					}
				},
				load: {
					groups: function() {
						$http.get(api.accessGroups)
							.success(function (data, status) {
								main.contents.groups = data || [];
								$timeout(function() {
									main.sortable.update();
									main.ordering.update();
								})
							});
					},
					all: function() {
						var toLoad = main.actions.load;
						var keys = Object.keys(toLoad);

						keys.forEach(function(key) {
							if(key !== 'all' && typeof toLoad[key] === 'function') {
								console.info('Loading: ' + key);
								toLoad[key]();
							}
						});
					}
				},
				group: {
					edit: {
						open: function(groupId) {
							main.actions.activeRecord.set(groupId);
							main.states.editionModal = true;
							$http.get(api.accessGroups + '/' + groupId)
								.success(function (data, status) {
									main.form.edition = angular.copy(data);
									main.actions.tags.load();
								});
						},
						close: function() {
							main.actions.activeRecord.clear();
							main.states.editionModal = false;
							main.form.reset(main.types.form.edition);
						},
						save: function() {
							main.actions.tags.update();
							var toInsert = main.form[main.types.form.edition];

              toInsert.name = toInsert.name.trim();
              toInsert.aciveDirectoryId = toInsert.aciveDirectoryId.split(',').map(function (el) {
                return el.trim();
              }).join(',');

							$http.put(api.accessGroups + '/' + main.actions.activeRecord.get(), toInsert)
								.success(function (data, status) {
									main.actions.group.edit.close();
									main.actions.load.groups();
								});
						}
					},
					add: {
						open: function() {
							main.actions.activeRecord.clear();
							main.actions.tags.unload();
							main.states.editionModal = true;
						},
						close: function() {
							main.states.editionModal = false;
							main.form.reset(main.types.form.edition);
						},
						save: function() {
							main.actions.tags.update();
							var toInsert = angular.copy(main.form.edition);
							toInsert.name = toInsert.name.trim();
							toInsert.aciveDirectoryId = toInsert.aciveDirectoryId.split(',').map(function (el) {
							  return el.trim();
              }).join(',');
							$http.post(api.accessGroups, toInsert)
								.success(function (data, status) {
									main.actions.group.add.close();
									main.actions.load.groups();
								});
						}
					},
					remove: {
						open: function(groupId, groupName) {
							main.actions.activeRecord.set(groupId);
							main.contents.activeGroupName = groupName;
							main.states.removalModal = true;
						},
						save: function() {
							$http.delete(api.accessGroups + '/' + main.actions.activeRecord.get())
								.success(function(data) {
									var groupList = document.querySelector(main.selectors.groups);
									var groupBox = document.querySelector(main.selectors.groups).querySelector(main.selectors.group + '[' + main.attributes.groupId + '="' + main.actions.activeRecord.get() + '"]');

									groupList.removeChild(groupBox);

									main.actions.group.remove.close();
									main.actions.load.groups();

								});
						},
						close: function() {
							main.states.removalModal = false;
							main.actions.activeRecord.clear();
						}
					}
				}
			};

			$scope.$on('$destroy', function() {
				main.sortable.destroy();
			});

			main.actions.load.all();
			main.actions.bind.all();
		});
