'use strict';

/**
 * @ngdoc function
 * @name mainApp.controller:AdministrationmaintenanceCtrl
 * @description
 * # AdministrationmaintenanceCtrl
 * Controller of the mainApp
 */
angular.module('mainApp')
		.controller('AdministrationmaintenanceCtrl', function ($scope, api, $http, $q, GlobalSelectors, switchery, $timeout, $rootScope) {
			var main = this;

			main.css = {
				classes: {

				},
				selectors: {

				}
			};

			main.states = {};
			main.contents = {};
			main.promises = {
				loaders: []
			};
			main.settings = {
				TAG: 'AdministrationmaintenanceCtrl',
				switcher: switchery
			};

			main.actions = {
				setActive: function(event) {
					$timeout(function() {
						$(event.target.parentNode.querySelector('span.switchery')).click();
					});
				},
				save: function() {
					$http.put(api.maintenance.set, main.contents.current)
						.success(function(maintenance) {
							main.contents.current = maintenance;
							$rootScope.$broadcast('maintenance', angular.copy(maintenance));
						});
				},
				load: {
					maintenance: function() {
						return $http.get(api.maintenance.get)
								.success(function(maintenance) {
									main.contents.current = maintenance;
								});
					},
					all: function(callback) {
						var loaders = this;
						var keys = Object.keys(loaders);

						main.promises.loaders.length = 0;

						keys.forEach(function(key, index) {
							var loader = loaders[key];

							if(key !== 'all' && typeof(loader) === 'function') {
								main.promises.loaders.push(loader());
							}

							if(++index === keys.length) {
								callback(main.promises.loaders);
							}
						});

					}
				},
				switchery: {
					collection: [],
					clickListener: function(event) {
						var switchInput = event.target.parentElement.parentElement.querySelector(GlobalSelectors.switch);

						if(switchInput.getAttribute('checked') === 'checked') {
							switchInput.setAttribute('checked', null);
						} else {
							switchInput.setAttribute('checked', 'checked');
						}

						var state = switchInput.getAttribute('checked') === 'checked' ? true : false;
						$scope.$apply(function () {
							main.contents.current.isActive = state;
						});

					},
					bindAll: function() {
						main.actions.switchery.unbindAll(function() {
							$(GlobalSelectors.switch).each(function(idx, el) {
								main.actions.switchery.collection.push(new Switchery(el, {
									color: main.settings.switcher.color,
									secondaryColor: main.settings.switcher.secondaryColor,
									size: main.settings.switcher.size
								}));
							});

							var switchery = document.querySelectorAll(GlobalSelectors.switchery);
							var keys = Object.keys(switchery);

							keys.forEach(function(key) {
								if(angular.isObject(switchery[key])) {
									switchery[key].addEventListener('click', main.actions.switchery.clickListener);
								}
							});
						});
					},
					unbindAll: function(callback) {

						var total = main.actions.switchery.collection.length;

						if(total === 0 && angular.isDefined(callback)) {
							return callback();
						}

						main.actions.switchery.collection.forEach(function(switcher, index) {
							switcher.destroy();

							if(++index === total) {
								if(angular.isDefined(callback)) {
									main.actions.switchery.collection.length = 0;

									var switchery = document.querySelectorAll(GlobalSelectors.switchery);
									var keys = Object.keys(switchery);
									keys.forEach(function(key) {
										var element = switchery[key];

										if(angular.isObject(element)) {
											element.parentNode.removeChild(element);
										}
									});
									return callback();
								}
							}
						});
					}
				}
			};

			main.actions.load.all(function(loaders) {
				$q.all(loaders)
					.then(function() {
						$timeout(function() {
							main.actions.switchery.bindAll();
						});
					});
			});

			$scope.$on('$destroy', function() {
				main.actions.switchery.unbindAll(function() {
					console.info(main.settings.TAG, 'Switchery: destroyed');
				});
			});
		});
