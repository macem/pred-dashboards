'use strict';

/**
 * @ngdoc function
 * @name mainApp.controller:SigninCtrl
 * @description
 * # SigninCtrl
 * Controller of the mainApp
 */
angular.module('mainApp')
		.controller('SigninCtrl', function ($scope, $compile, $timeout, $http,
                                        api, $state, notification, User, $rootScope, $document, utils) {

    $scope.isLocalhost = $document[0].location.href.indexOf('localhost') >= 0;

    var selectors = {
        navigation: 'div.nav',
        footer: 'div.foot'
    };

    $http.get(api.config).success(function(data) {
        $scope.config = data;
    });

    /**
        * Provide a methods
        * to compile our directives
        * on demand.
        *
        * We will use this methods later,
        * after the access will be granted.
        *
        * @type {{header: Function, footer: Function}}
        */
    var htmlComponents = {
        header: function () {
            return $compile('<navigation></navigation>')($scope);
        },
        footer: function () {
            return $compile('<app-footer></app-footer>')($scope);
        }
    };

    /**
        * Define some UI states
        * to be served in our view.
        * @type {{}}
        */
    $scope.UIStates = {};
    $scope.UIStates.isLoginButtonActive = true;

    /**
        * Create an empty user object
        * for entered (on UI) credentials.
        * @type {{}}
        */
    $scope.user = {};

    /**
     * Define actions to be available
     * in our view.
     * @type {{sendCredentials: Function, showHeadAndFoot: Function}}
     */
    $scope.actions = {

        prediLogin: function () {
            // TODO rewrite pingFed to iframe without reload page
            // var iframe = document.querySelector('iframe#appLogin');

            $document[0].location.href =
                'https://' + $scope.config.host + '/as/authorization.oauth2?client_id=' + $scope.config.client
                + '&response_type=code&scope=profile openid&redirect_uri=' + $scope.config.redirect;
        },

        // for localhost tests only
        prediLoginTest: function () {
            /*utils.cookies.set('bioinfo_bas', 'AZdQsZUZfJvJTYZRPZSNZJQGZZ', {
                domain: '.roche.com',
                expires: 183
            });
            $timeout(function () {
                $document[0].location.href = 'http://localhost:1338';
            }, 100);*/
        },

        sendCredentials: function (code) {
            $scope.UIStates.isLoginButtonActive = false;

            $http.post(api.signIn, {
                key: code
            }).success(function (data, status, headers, config) {
                /**
                 * We expect that the response code
                 * will be equal to 200.
                 * If not, we want to show an error message
                 * to the user's browser.
                 */
                if (status !== 200) {
                    notification.push('Error: ' + status.message, notification.types.error);
                } else {
                    /**
                     * Access granted!
                     * Now we can switch our user
                     * to the dashboard.
                     *
                     * BUT before we need to
                     * set our User.data object
                     * to store the necessary information
                     * about the user.
                     */

                    User.setData(data);
                }

                /**
                 * Whatever happens,
                 * unblock our 'Sign In' button.
                 * @type {boolean}
                 */
                $scope.UIStates.isLoginButtonActive = true;
            }).error(function (data, status, headers, config) {
                /**
                 * Whatever happens,
                 * unblock our 'Sign In' button.
                 * @type {boolean}
                 */
                console.log('Error->: ' + status.message, notification.types.error);
                $scope.UIStates.isLoginButtonActive = true;
            });
        },
        showHeadAndFoot: function () {
            /**
             * Looking for
             * the navigation holders.
             * @type {*|jQuery|HTMLElement}
             */
            var navigation = $(selectors.navigation);
            var footer = $(selectors.footer);

            /**
             * And replace with
             * our <navigation> and <app-footer> directives...
             * (but only if needed)
             */
            if (navigation.length) {
                //navigation.replaceWith(htmlComponents.header());
                //footer.replaceWith(htmlComponents.footer());
            }
        }
    };
});
