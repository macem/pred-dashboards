'use strict';

/**
 * @ngdoc function
 * @name mainApp.controller:MashupcreateCtrl
 * @description
 * # MashupcreateCtrl
 * Controller of the mainApp
 */
angular.module('mainApp')
		.controller('MashupcreateCtrl', function ($scope, $timeout, utils, api, $http, favorites, notification, $state, User, GlobalSelectors, language, MashupPolicy, preloader) {

			/**
			 * Init local variables.
			 */
			var sortable,
				selectors,
				currentOrder,
				filter,
				typesToClass,
				cookiesKeys,
				TAG,
				main;

			main = {};
			main.css = {
				classes: {
					hideBox: 'hideBox',
					hideTwin: 'hideTwin',
					hideByQuery: 'hideByQuery',
					hideByDocType: 'hideByDocType',
					ngHide: 'ng-hide'
				},
				selectors: {
					panelIncluded: 'div#included',
					panelExcluded: 'div.mashup__aside',
					box: 'div.boxElement',
					sortable: 'div.js-sortable',
					favIconEnabled: 'span.icon--star-1',
					chosen: {
						select: '.js-select--tags + .chosen-container input.default',
						tags: '.js-select, .js-select--tags'
					},
					group: 'div.mashup__list'
				}
			};

			main.attributes = {
				recordId: 'data-record-id',
				categoryId: 'category-id',
				ngRepeat: 'ng-repeat',
				ngShow: 'ng-show',
				docTypeId: 'doctype-id'
			};

			/**
			 * Define empty order array
			 * for the view init.
			 * @type {Array}
			 */
			currentOrder = [];

			/**
			 * Custom TAG for
			 * this controller.
			 * @type {string}
			 */
			TAG = 'MashupCreate';

			/**
			 * Mapping to the proper
			 * CSS selectors for each
			 * docType.
			 * @type {{Tibco Spotfire: string, Business Objects: string, Power Point: string, Excel: string}}
			 */
			typesToClass = {
				'Tibco Spotfire': 'box--type-1',
				'Business Objects': 'box--type-2',
				'Power Point': 'box--type-3',
				'Excel': 'box--type-4',
				'Custom': 'box--type-6',
				'PDF': 'box--type-7',
        'Tableau': 'box--type-8',
			};

			/**
			 * DOM selectors.
			 * @type {{singleElement: string, list: string, hideFilter01: string, hideFilter02: string}}
			 */
			selectors = {
				singleElement: 'div.box',
				list: '#list',
				hideFilter01: 'hideFilter01',
				hideFilter02: 'hideFilter02',
				hideBox: 'hideBox',
				isSearchEmpty: 'div.mashup__list:not([class*=hideBox])',
				emptyResultsMessage: 'div#emptyResultsMessage',
				removeTopPadding: 'removeTopPadding',
				notHidden: '.boxElement:not([class*=hideFilter]):not([class*="ng-hide"])',
				groups: 'div.mashup__list',
				mashupList: '#mashup',
				counter: 'span#counter',
				dragMessage: '#dragMessage',
				used: 'used',
				removed: 'removed',
				chosen: {
					select: '.js-select--tags + .chosen-container input.default',
					tags: '.js-select, .js-select--tags'
				}
			};

			cookiesKeys = {
				introMessage: TAG + '|introMessage'
			};

			/**
			 * Filters for available dashboard's elements.
			 * @type {{type: Array, title: string}}
			 */
			filter = {
				type: []
			};

			/**
			 * Set proper path for images.
			 * @type {mainAPI.cover|*|exports.cover|b.cover}
			 */
			$scope.imgPath = api.cover;

			$scope.disallowedReportTypes = MashupPolicy.disallowReportTypes;

			/**
			 * Load favorites
			 */
			favorites.update(function(data, favs) {
				$scope.favData = data;
				$scope.favorites = favs;

				$timeout(function() {
					sort();
				});
			});

			$scope.states = {
				message: angular.isDefined(User.cookies.get(cookiesKeys.introMessage)) ? JSON.parse(User.cookies.get(cookiesKeys.introMessage)) : true,
				save: false
			};

			/**
			 * Get current order
			 * for contents in mashup.
			 * @param callback
			 */
			var getOrder = function(callback) {

				var favHolder = document.querySelector(selectors.mashupList);

				if(favHolder === null) {
					console.warn('Could not find ' + selectors.mashupList);
					callback([]);
				}

				var reports = favHolder.querySelectorAll(selectors.singleElement);

				if(Object.keys(reports).hasOwnProperty('length')) {
					var newOrder = [],
						keys = Object.keys(reports);

					keys.forEach(function(key) {
						if(angular.isObject(reports[key])) {
							var report = reports[key];
							newOrder.push(report.parentElement.getAttribute('data-record-id'));
						}
					});
					callback(newOrder);
				} else {
					callback([]);
				}
			};

			var countIncludedReports = function() {
				var mashupList = document.querySelector(selectors.mashupList);

				if(mashupList === null) {
					console.warn('Could not find ' + selectors.mashupList);
					return;
				}

				var reports = mashupList.querySelectorAll(selectors.singleElement);
				var counter = document.querySelector(selectors.counter);
				var dragMessage = document.querySelector(selectors.dragMessage);

				counter.innerText = reports.length + ' ' + (reports.length === 1 ? 'item' : 'items');
				reports.length > 0 ? dragMessage.classList.add('hidden') : dragMessage.classList.remove('hidden');
			};

			var hideEmptyGroups = function() {

				var list = document.querySelector(selectors.list);
				var groups = list.querySelectorAll(selectors.groups);

				var keys = Object.keys(groups);
				var pos = 0;

				keys.forEach(function(key) {
					var group = groups[key];

					if(angular.isObject(groups[key])) {
						if(group.querySelectorAll(selectors.notHidden).length === 0) {
							groups[key].classList.add(selectors.hideBox);
							groups[key].classList.remove(selectors.removeTopPadding);
						} else {
							groups[key].classList.remove(selectors.hideBox);
							if(pos === 0) {
								groups[key].classList.add(selectors.removeTopPadding);
							} else {
								groups[key].classList.remove(selectors.removeTopPadding);
							}
							pos++;
						}
					}
				});


				/**
				 * Check if we have any
				 * search results.
				 * If not, we will display
				 * some message, e.g.: 'Sorry, we could not find XYZ'
				 * @type {boolean}
				 */
				var isSearchEmpty = document.querySelectorAll(selectors.isSearchEmpty).length === 0;
				var emptyResultsMessage = document.querySelector(selectors.emptyResultsMessage);

				if(isSearchEmpty) {
					emptyResultsMessage.classList.remove(selectors.hideBox);
				} else {
					emptyResultsMessage.classList.add(selectors.hideBox);
				}
			};


			/**
			 * Apply the 'sortable' option
			 * to the all necessary elements.
			 */
			var sort = function() {
				$('.js-sortable').each(function(idx, el) {

					var sortable = new Sortable($(el)[0], {
						animation: 300,
						draggable: '.js-sortable__draggable',
						handle: '.js-sortable__handle',
						sort: !!$(el).data('groupSortable'),
						group: {
							name: $(el).data('groupName') || 'default',
							put: $(el).data('groupPut') ? ($(el).data('groupPut')).split(',') : ['default']
						},
						onUpdate: function(event) {
							getOrder(function(order) {
								/**
								 * Set current order
								 * (Array).
								 */
								currentOrder = order;

								/**
								 * Count reports
								 * that are visible
								 * on the right panel.
								 */
								countIncludedReports();

								/**
								 * Hide reports in left
								 * pane which are already used
								 * in the right panel.
								 */
								hideAlreadyUsed();

								/**
								 * Check if 'create' form
								 * is valid.
								 */
								form.validate();

								hideEmptyGroups();
							});

						},
						onAdd: function(event) {
							getOrder(function(order) {
								/**
								 * Set current order
								 * (Array).
								 */
								currentOrder = order;

								/**
								 * Count reports
								 * that are visible
								 * on the right panel.
								 */
								countIncludedReports();

								/**
								 * Hide reports in left
								 * pane which are already used
								 * in the right panel.
								 */
								hideAlreadyUsed();

								/**
								 * Check if 'create' form
								 * is valid.
								 */
								form.validate();

								hideEmptyGroups();
							});
						}
					});
				});
			};

			/**
			 * Add 'hidden' class
			 * to the elements which are not on the
			 * filter.type list.
			 */
			var applyFilters = function() {

				var visibleElements = document.querySelectorAll(main.css.selectors.panelExcluded + ' ' + main.css.selectors.box + ':not([class*="' + main.css.classes.hideTwin + '"]):not([class*="' + main.css.classes.ngHide + '"])');

				var keys = Object.keys(visibleElements);

				keys.forEach(function(key) {
					var record = visibleElements[key];

					if(angular.isObject(record)) {
						if(filter.type.indexOf(record.getAttribute(main.attributes.docTypeId))  < 0 && filter.type.length > 0) {
							record.classList.add(selectors.hideFilter02);
						} else {
							record.classList.remove(selectors.hideFilter02);
						}
					}
				});


			};

			/**
			 * Push selected filters
			 * to 'filter.type' object.
			 * @param event
			 */
			var setFilters = function(event) {
				var options = event.delegateTarget.parentElement.querySelectorAll('li.select2-selection__choice');
				var keys = Object.keys(options);


				/**
				 * Clear the current 'type'
				 * collection.
				 * @type {number}
				 */
				filter.type.length = 0;

				keys.forEach(function(key) {
					var title = options[key].title.trim();
					if(angular.isDefined(title)) {
						filter.type.push(typesToClass[title]);
					}
					title = undefined;
				});

			};

			var hideAlreadyUsed = function() {
				var list = document.querySelector(selectors.list);

				currentOrder.forEach(function(report) {
					report = report || '';

					var found = list.querySelectorAll('div[data-record-id="' + report + '"]');
					var keys = Object.keys(found);

					keys.forEach(function(key) {
						if(angular.isObject(found[key])) {
							found[key].classList.add(selectors.used);
						}
					});
				});
			};

			var releaseUsed = function(reportId) {
				reportId = reportId || '';

				var list = document.querySelector(selectors.list);

				var found = list.querySelectorAll('div[data-record-id="' + reportId + '"]');
				var keys = Object.keys(found);

				keys.forEach(function(key) {
					if(angular.isObject(found[key])) {
						found[key].classList.remove(selectors.used);
					}
				});
			};

			/**
			 * Define actions
			 * to be used in the controller's view.
			 * @type {{remove: Function, save: Function, closeMessage: Function}}
			 */
			$scope.action = {
				remove: function(reportId) {
					var report = document.querySelector(selectors.mashupList).querySelector('div[data-record-id="' + reportId + '"]');
					var categoryUID = report.getAttribute('data-category-uid');
					var category = document.querySelector('div[category-id="' + categoryUID + '"]');

					category.appendChild(report);
					countIncludedReports();
					getOrder(function(order) {

						/**
						 * Set current order
						 * (Array).
						 */
						currentOrder = order;

						/**
						 * Apply 'sort' option for
						 * the necessary elements.
						 */
						sort();

						/**
						 * So, this report should
						 * be available again on the left
						 * list.
						 */
						releaseUsed(reportId);

						/**
						 * Check if 'create' form
						 * is valid.
						 */
						form.validate();

						hideEmptyGroups();
					});
				},
				save: function() {
					$http.post(api.mashupsAdd, {
						name: $scope.name,
						description: $scope.description,
						contents: currentOrder
					})
						.success(function(data) {
							if(angular.isDefined(data.id)) {
								$state.go('mashup.view', {
									id: data.id,
									created: true
								})
							} else {
								notification.push(language.error + data, notification.types.error);
							}
						});

				},
				closeMessage: function() {
					User.cookies.set(cookiesKeys.introMessage, false);
					$scope.states.message = false;
				},
				toggleFavorites: function(event, id) {
					event.stopPropagation();
					event.preventDefault();

					var star = event.target.querySelector('span');

					favorites.toggle(event, id, function(data, favs) {
						/*
						$scope.favData = data;
						$scope.favorites = favs;

						$timeout(function() {
							sort();
							hideAlreadyUsed();
						});
						*/

						if(star.classList.contains(GlobalSelectors.star.full)) {
							star.classList.remove(GlobalSelectors.star.full);
							star.classList.add(GlobalSelectors.star.empty);
						} else {
							star.classList.remove(GlobalSelectors.star.empty);
							star.classList.add(GlobalSelectors.star.full);
						}
					});
				}
			};

			/**
			 * Custom filter query
			 * - watcher.
			 */
			$scope.$watch('query', function(newQuery, oldQuery) {
				if(angular.isDefined(newQuery)) {

					/**
					 * Remove double spaces.
					 */
					newQuery = newQuery.replace(/\s+/igm, ' ');

					var visibleElements = document.querySelector(selectors.list).querySelectorAll(selectors.singleElement);
					var keys = Object.keys(visibleElements);
					var pattern = new RegExp(utils.escapeRegExp(newQuery), 'i');

					keys.forEach(function(key) {
						var item = visibleElements[key];

						if(angular.isObject(item)) {
							var title = item.querySelector('h4').textContent.trim() || '';
							var description = item.querySelector(GlobalSelectors.box.description).textContent.trim() || '';

							/*if(pattern.test(description) && newQuery.length > 0) {
								item.classList.add(GlobalSelectors.box.hovered);
							} else {
								item.classList.remove(GlobalSelectors.box.hovered);
							}*/

							//if (pattern.test(title) || pattern.test(description)) {
							if (pattern.test(title)) {
								item.parentElement.classList.remove(selectors.hideFilter01);
							} else {
								item.parentElement.classList.add(selectors.hideFilter01);
							}
						}
					});

					hideEmptyGroups();
				}
			}, true);

			var form = {
				isValid: function() {
					return (angular.isDefined($scope.name) && $scope.name.length) > 0 && currentOrder.length > 0;
				},
				validate: function() {
					$scope.$evalAsync(function() {
						if(form.isValid()) {
							$scope.states.save = true;
						} else {
							$scope.states.save = false;
						}
					});
				}
			}
			var isFormValid = function() {
				return (angular.isDefined($scope.name) && $scope.name.length) > 0 && currentOrder.length > 0;
			};

			var validateForm = function() {
				$scope.$evalAsync(function() {
					if(isFormValid()) {
						$scope.states.save = true;
					} else {
						$scope.states.save = false;
					}
				});
			};

			// TODO
			$scope.$watch('name', function(newQuery, oldQuery) {
				if(angular.isDefined(newQuery)) {
					form.validate();
				}
			}, true);


			/**
			 * Getting the list of
			 * available categories with
			 * related items.
			 */
			$http.get(api.categoriesFull)
				.success(function (data, status, headers, config) {
					$scope.categories = data;
					//$scope.categoriesAsId = _.pluck(data, 'name').join(',');
					/*
					 Using Sortable plugin for sortable functionality
					 https://github.com/RubaXa/Sortable
					 */
					$timeout(function() {
						sort();
					},1000);
				});

			$http.get(api.docTypes)
					.success(function (data, status, headers, config) {
						$scope.docTypes = data;
						$timeout(function() {
							$(selectors.chosen.tags).chosen({
								disable_search_threshold: -1,
								search_contains: true
							})
									.change(function(event, status) {
										if(status.selected) {
											filter.type.push(status.selected);
										} else {
											var index = filter.type.indexOf(status.deselected);
											filter.type.splice(index, 1);
										}
										//main.actions.applyDocTypeFilter();
										applyFilters();
										hideEmptyGroups();
									});

							$(selectors.chosen.select).on('keydown', function(e) {
								var keycode = (e.keyCode ? e.keyCode : e.which);
								var value = $(this).val();

								if(keycode == '13') {
									$(this).parents('.chosen-container').prev().
											append('<option selected>' + value + '</option>').
											trigger('chosen:updated');
								}
							});
						},1000);
					});

			$timeout(function() {

				/*
				 Using jQuery Select2 plugin for autocompletes and selects
				 https://github.com/ivaynberg/select2
				 */

				sort();
				//hideEmptyGroups();

			});

			/**
			 * Remove the sortable functionality completely
			 * when directive is destroyed.
			 */
			$scope.$on('$destroy', function() {
				/**
				 * Try to destroy sortable
				 * object.
				 */
				/*try {
					sortable.destroy();
				} catch(e) {
					console.warn('Could not destroy SORTABLE widget.');
				}*/
			});
		});
