'use strict';

/**
 * @ngdoc The 'Signout' controller is responsible for logging out.
 * @name mainApp.controller:SignoutCtrl
 * @description
 * # SignoutCtrl
 * Controller of the mainApp
 */
angular.module('mainApp')
		.controller('SignoutCtrl', function ($scope, User, api, $http, $state, $rootScope, $timeout, preloader) {
			$scope.firstName = User.getData().firstName || 'Dear User';

            $http.get(api.signOut)
				.success(function(data, status, headers, config) {
                    var SIGIN_ROUTE = 'signin';

					/**
					 * For the security reason
					 * whe should clear the user data
					 * stored by 'User' service.
					 * It will also hide the <navigation> and <app-footer>
					 * directives. See details directly in:
					 * - User Service
					 * - AppuiCtrl
					 */
					User.setData({});
                    window.sessionStorage.removeItem('key');

					/**
					 * Go to 'SignIn' view.
					 */
                    $timeout(function () {
                        preloader.hide();
                        $rootScope.$broadcast('session:cancel');
                        $state.go(SIGIN_ROUTE);
                    }, 100);
				});
		});
