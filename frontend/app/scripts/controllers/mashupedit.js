'use strict';

/**
 * @ngdoc function
 * @name mainApp.controller:MashupeditCtrl
 * @description
 * # MashupeditCtrl
 * Controller of the mainApp
 */
angular.module('mainApp')
		.controller('MashupeditCtrl', function ($scope, $http, api, $q, $log, MashupPolicy, $stateParams, $timeout, $state, favorites, GlobalSelectors) {

			var main = this;

			/**
			 * Define parent objects.
			 * @type {Array}
			 */
			main.promises = [];
			main.filters = {
				types: [],
				query: ''
			};
			main.contents = {};
			main.actions = {};
			main.settings = {};
			main.css = {};
			main.sortables = [];

			/**
			 * Element attributes.
			 * @type {{}}
			 */
			main.attributes = {
				docTypeId: 'doctype-id',
				ngRepeat: 'ng-repeat',
				ngShow: 'ng-show'
			};

			/**
			 * CSS
			 */
			main.css.classes = {
				filters: {
					hide: 'hideFilter01'
				},
				hide: 'hide'
			};

			main.css.ids = {
				counter: 'counter',
				intro: 'intro',
				panel: {
					available: 'available',
					active: 'active'
				}
			};

			main.css.selectors = {
				report: 'div.boxElement:not(.in-trash)',
				chosen: {
					select: '.js-select--tags + .chosen-container input.default',
					tags: '.js-select, .js-select--tags'
				}
			};

			/**
			 * Contents
			 */
			main.contents.recordId = angular.copy($stateParams.id);

			/**
			 * Settings
			 */
			main.settings.disallowedReportTypes = angular.copy(MashupPolicy.disallowReportTypes);

			/**
			 * Actions
			 */
			main.actions.start =  function() {
				main.actions.load.all();
			};

			main.actions.remove = function(id) {
				$log.info('Report id=%s deleted.', id);

				var hasItsTwin = document.getElementById(main.css.ids.panel.available).querySelector('div[data-record-id="' + id + '"]'),
					hasItsTwin = (hasItsTwin !== null),
					findElement = document.getElementById(main.css.ids.panel.active).querySelector('div[data-record-id="' + id + '"]');

				if(findElement !== null) {
					if((findElement.getAttribute('data-existing') || findElement.getAttribute('existing')) !== null) {
						findElement.classList.add('in-trash');
					} else {
						// clear the angular attributes before moving inside the DOM tree.
						findElement.removeAttribute(main.attributes.ngRepeat)
						findElement.removeAttribute(main.attributes.ngShow);

						var findParentCategory = document.getElementById(main.css.ids.panel.available).querySelector('div[category-id="' + findElement.getAttribute('data-category-uid') + '"]');
						findParentCategory.appendChild(findElement);
						$log.debug('moving %O to %O', findElement, findParentCategory);
					}
				} else {
					$log.error('Could not find report with id=%s!', id);
				}

				$timeout(function() {
					main.actions.hideTwins();
				});
			};

			/**
			 * Actions :: Favorites
			 * @type {{}}
			 */
			main.actions.favorites = {};
			main.actions.favorites.toggle = function(event, id) {
				event.stopPropagation();
				event.preventDefault();

				var star = event.target.querySelector('span');

				favorites.toggle(event, id, function(data, favs) {
					if(star.classList.contains(GlobalSelectors.star.full)) {
						star.classList.remove(GlobalSelectors.star.full);
						star.classList.add(GlobalSelectors.star.empty);
					} else {
						star.classList.remove(GlobalSelectors.star.empty);
						star.classList.add(GlobalSelectors.star.full);
					}
				});
			};


			main.actions.load = {};
			main.actions.load.categories = function() {
				$log.info('[load] categories');
				return $http.get(api.categoriesFull)
						.success(function(data) {
							main.contents.categories = data;
						});
			};

			main.actions.load.favorites = function() {
				$log.info('[load] favorites');
				return $http.get(api.favorites)
						.success(function(data) {
							main.contents.favorites = data;

							favorites.update(function(finalData, originalData) {
								main.contents.favData = finalData;
							});
						});
			};

			main.actions.load.docTypes = function() {
				$log.info('[load] docTypes');
				return $http.get(api.docTypes)
						.success(function(data) {
							main.contents.docTypes = data;
						});
			};

			main.actions.load.selectedMashup = function() {
				$log.info('[load] selectedMashup');
				return $http.get(api.mashups + '/' + main.contents.recordId)
						.success(function(data) {
							main.contents.mashup = data;
							//main.contents.mashup.contents = _.indexBy(main.contents.mashup.contents, 'id');
						});
			};

			main.actions.load.all = function() {

				$log.info('Loading HTTP data...');

				for(var data in main.actions.load) {
					if(main.actions.load.hasOwnProperty(data) && data !== 'all') {
						main.promises.push(main.actions.load[data]());
					}
				}

				$q.all(main.promises)
					.then(
						function() {
							$log.info('All HTTP data was loaded.');
							$timeout(function() {
								main.actions.bindJS.all();
							});
						},
						function(reason) {
							$log.error(reason);
							alert('HTTP error?');
						}
					);
			};

			/**
			 * Filters actions.
			 * @type {{}}
			 */
			main.actions.filters = {};
			main.actions.filters.apply = function() {
				var selected = main.filters.types;
				$log.warn('Selected filters %O', selected);

				var availableReports = document.getElementById(main.css.ids.panel.available).querySelectorAll(main.css.selectors.report);
				var pattern = new RegExp(main.filters.query, 'igm');

				for(var report in availableReports) {
					if(availableReports.hasOwnProperty(report) && report !== 'length') {
						var element = availableReports[report];
						if(element instanceof Object) {
							var docType = element.getAttribute(main.attributes.docTypeId) || null;
							var title = element.querySelector('h4 a').textContent.trim();

							if((pattern.test(title) && (selected.indexOf(docType) >= 0 || selected.length === 0))) {
								element.classList.remove(main.css.classes.filters.hide);
							} else {
								element.classList.add(main.css.classes.filters.hide);
							}
						}
					}
				}

				$timeout(function() {
					main.actions.hideEmptyGroups();
				});
			};

			main.actions.save = function() {
				console.log(main.contents.order);
				var update = {
					name: main.contents.mashup.name,
					description: main.contents.mashup.description,
					contents: main.contents.order
				};

				$http.put(api.mashups + '/' + main.contents.recordId, update)
						.success(function(data) {
							$state.go('mashup.view', {
								id: main.contents.recordId, created: true
							});
						});
			};

			main.actions.update = {};
			main.actions.update.counter = function(value) {
				var counter = document.getElementById(main.css.ids.counter);
				var intro = document.getElementById(main.css.ids.intro);

				if(value === 0 && !isNaN(value)) {
					intro.classList.remove(main.css.classes.hide);
				} else {
					intro.classList.add(main.css.classes.hide);
				}

				counter.innerText = value;
				main.contents.counter = value;
			};

			main.actions.hideTwins = function() {
				$log.info('Hide possible twins in the left column.');
				var elements = {
					right: document.getElementById(main.css.ids.panel.active).querySelectorAll(main.css.selectors.report),
					left: document.getElementById(main.css.ids.panel.available).querySelectorAll(main.css.selectors.report)
				};

				elements.right = _.mapObject(elements.right, function(val, key) {
					if(val instanceof Object) {
						return val.getAttribute('data-record-id') || val.getAttribute('record-id');
					}
				});

				elements.right = _.values(elements.right);
				elements.right = elements.right.filter(function(v) {
					return v !== null && typeof v !== 'undefined';
				});
				main.actions.update.counter(elements.right.length);
				main.contents.order = elements.right;

				for(var el in elements.left) {
					if(elements.left.hasOwnProperty(el) && el !== 'length') {
						var elm = elements.left[el];

						if(elm instanceof Object) {
							if (elements.right.indexOf(elm.getAttribute('data-record-id') || elm.getAttribute('record-id')) >= 0) {
								elm.classList.add('hide');
							} else {
								elm.classList.remove('hide');
							}
						}
					}
				};

				elements = null;

				$timeout(function() {
					main.actions.hideEmptyGroups();
				});
			};

			/**
			 * Hide empty groups
			 */
			main.actions.hideEmptyGroups = function() {
				$log.info('Hidding empty groups.');
				var groups = document.querySelectorAll('div[category-id]');
				[].forEach.call(groups, function(group) {
					var any = group.querySelectorAll('div.boxElement:not(.hide):not(.ng-hide):not(.hideFilter01)');
					if(any.length === 0) {
						group.classList.add(main.css.classes.hide);
					} else {
						group.classList.remove(main.css.classes.hide);
					}
				});
			};

			/**
			 * Unbind JavaScript stuff...
			 */
			main.actions.unbindJS = {};
			main.actions.unbindJS.draggable = function() {
				main.sortables.forEach(function(sortable) {
					sortable.destroy();
				});

				main.sortables.length = 0;
			};

			/**
			 * Bind JavaScript stuff...
			 * @type {{}}
			 */
			main.actions.bindJS = {};
			main.actions.bindJS.filters = function() {
				$log.info('[bindJS] Binding filters...');

				$(main.css.selectors.chosen.tags).chosen({
					disable_search_threshold: -1,
					search_contains: true
				}).change(function(event, status) {
					if(status.selected) {
						main.filters.types.push(status.selected);
					} else {
						var index = main.filters.types.indexOf(status.deselected);
						main.filters.types.splice(index, 1);
					}
					main.actions.filters.apply();
				});

				$(main.css.selectors.chosen.select).on('keydown', function(e) {
					var keycode = (e.keyCode ? e.keyCode : e.which);
					var value = $(this).val();

					if(keycode == '13') {
						$(this).parents('.chosen-container').prev().
								append('<option selected>' + value + '</option>').
								trigger('chosen:updated');
					}
				});

			};

			main.actions.bindJS.draggable = function() {
				main.actions.unbindJS.draggable();
				$('.js-sortable').each(function(idx, el) {

					main.sortables.push(
						new Sortable($(el)[0], {
							animation: 300,
							draggable: '.js-sortable__draggable',
							handle: '.js-sortable__handle',
							sort: !!$(el).data('groupSortable'),
							group: {
								name: $(el).data('groupName') || 'default',
								put: $(el).data('groupPut') ? ($(el).data('groupPut')).split(',') : ['default']
							},
							onUpdate: function(event) {
								//main.actions.sortable.appendTasksAfter();
								main.actions.hideTwins();
							},
							onAdd: function(event) {
								//main.actions.sortable.appendTasksAfter();
								main.actions.hideTwins();
							}
						})
					);
				});

				$timeout(function() {
					main.actions.hideTwins();
				});
			};

			main.actions.bindJS.all = function() {
				$log.info('Binding JavaScript plugins and so on...');

				for(var script in main.actions.bindJS) {
					if(main.actions.bindJS.hasOwnProperty(script) && script !== 'all' && script !== 'length') {
						main.actions.bindJS[script]();
					}
				}
			};

			/**
			 * Watchers
			 */
			main.watchers = {
				collection: [
					$scope.$watch('main.filters.query', function(newValue, oldValue) {
						if(angular.isDefined(newValue)) {
							main.actions.filters.apply();
						}
					}, true)
				],
				unregister: function() {
					$log.info('Unregistering available watchers...');
					main.watchers.collection.forEach(function(watcher) {
						watcher();
					});

					main.watchers.collection.length = 0;
				}
			};

			/**
			 * On destroy.
			 */
			$scope.$on('$destroy', function() {
				$log.info('$destroy');
				main.watchers.unregister();
			});

			/**
			 * Start view.
			 */
			main.actions.start();

		});
