'use strict';

/**
 * @ngdoc function
 * @name mainApp.controller:AdministrationcategoriesCtrl
 * @description
 * # AdministrationcategoriesCtrl
 * Controller of the mainApp
 */
angular.module('mainApp')
	.controller('AdministrationtypesCtrl', function ($scope, api, User, $http, $state, $timeout, language, $rootScope, GlobalSelectors, switchery) {

	var sort,
		selectors,
		sortable,
		sortableDestroy,
		currentOrder,
		attributes,
		countVisible,
		activeRecord,
		main;


	activeRecord = null;
    main = $scope;

	$scope.modalTypes = {
		add: 'add',
		edit: 'edit'
	};

	$scope.states = {
		modalVisible: false,
		promptModalVisible: false
	};

	/*main.settings = {
		switcher: switchery
	};

	main.cssClasses = {
		record: {
		  disabled: 'icon--complete',
		  enabled: 'icon--padlock',
		  switchery: 'switchery'
		}
	};*/

    $scope.isUndefined = function (thing) {
        return (typeof thing === "undefined");
    };

	selectors = {
		sortable: 'div.js-sortable',
		row: 'div.row',
		counter: 'span#counter',
		typeList: 'div#typeList',
		hideBox: 'hideBox',
		withoutHideBox: ':not(.hideBox)',
		switchery: 'span.switchery',
		switchInput: 'input.js-switch'
	};

	attributes = {
		typeId: 'type-id'
	};

	countVisible = function() {
		var list = document.querySelector(selectors.sortable);
		var categories = list.querySelectorAll(selectors.row + selectors.withoutHideBox);
		var counter = document.querySelector(selectors.counter);

		counter.innerText = (categories.length || 0) + ' ' + (categories.length === 1 ? language.item : language.items);
	};

	/*currentOrder = {
		_order: [],
		set: function(config) {
			config = config || {
				saveToBackend: false
			};

			var list = document.querySelector(selectors.sortable);
			var categories = list.querySelectorAll(selectors.row + selectors.withoutHideBox);

			var keys = Object.keys(categories);
			var tmpArray = [];

			keys.forEach(function(key, index) {
				var category = categories[key];

				if(angular.isObject(category)) {
					if(category.getAttribute(attributes.categoryId)) {
						tmpArray.push(category.getAttribute(attributes.categoryId));
					}
				}

				if(++index >= keys.length) {
					currentOrder._order = tmpArray;
					countVisible();

					if(config.saveToBackend) {
						currentOrder.save();
					}
				}
			});
		},

		get: function() {
			return currentOrder._order;
		},
		save: function() {
			console.info('saved: ', currentOrder.get());
			console.info('ordering: saved.');
			$http.put(api.categoriesSort, {
				order: currentOrder.get()
			});
		}
	};*/

	/**
	 * Apply the 'sortable' option
	 * to the all necessary elements.
	 */
	/*sort = function() {
		sortableDestroy();
		$('.js-sortable').each(function(idx, el) {
			sortable = new Sortable($(el)[0], {
				animation: 300,
				draggable: '.js-sortable__draggable',
				handle: '.js-sortable__handle',
				sort: !($(el).data('groupSortable') == ""),
				group: {
					name: $(el).data('groupName') || null,
					pull: $(el).data('groupPull') || true,
					put: $(el).data('groupPut') || true
				},
				onUpdate: function(event) {
					currentOrder.set({
						saveToBackend: true
					});
				},
				onAdd: function(event) {
					currentOrder.set({
						saveToBackend: true
					});
				}
			});
		});
		currentOrder.set();
	};*/

	/**
	 * Sometimes we need
	 * to destroy sortable
	 * mechanism....
	 */
	/*sortableDestroy = function() {
		if(angular.isDefined(sortable)) {
			console.info('sortable: destroyed.');
			sortable.destroy();
		}
	};*/

    /**
     * Main actions
     * @type {{add: Function, edit: Function, remove: Function}}
     */
    $scope.action = {
        _modalType: null,
        _setTypeData: function(type) {
            $scope.typeId = type.id;
            $scope.typeTitle = type.title || '';
            $scope.typeImgFlagId = type.imgFlagId || '';
            $scope.typeSearchBy = angular.isDefined(type.searchBy) ? type.searchBy : false;
        },
        _checkId: function(id) {
            return $scope.types.find(function(type) {
                return type.id !== $scope.typeId && type.imgFlagId === id;
            }) || false;
        },
        reloadList: function() {
            $http.get(api.docTypes)
                .success(function (data, status, headers, config) {
                    $scope.types = data;
                    $timeout(function() {
                        //sort();
                        countVisible();
                        //$scope.action.switchery.bindAll();
                    })
                });
        },
        add: function() {
            activeRecord = null;
            $scope.states.modalVisible = true;
            $scope.action._modalType = $scope.modalTypes.add;
        },
        edit: function(type) {
            activeRecord = type.id;
            $scope.action._setTypeData(type);
            $scope.states.modalVisible = true;
            $scope.action._modalType = $scope.modalTypes.edit;
        },
        remove: function(type) {
            activeRecord = type.id;
            $scope.action._setTypeData(type);
            $scope.states.promptModalVisible = true;
        },
        delete: function() {
            var record = {
                id: activeRecord
            };

            $http.delete(api.docTypes + '/' + activeRecord)
                    .success(function(data) {
                        var typeList = document.querySelector(selectors.typeList);
                        var categoryBox = document.querySelector(selectors.typeList).querySelector(selectors.row + '[' + attributes.typeId + '="' + activeRecord + '"]');

                        typeList.removeChild(categoryBox);

                        $scope.docTypes = data;
                        $scope.action._setTypeData({});
                        $scope.action.close();
                        $scope.action.reloadList();

                        // $rootScope.$broadcast('categories:update');
                    });
        },
        save: function() {
            var record;

            if ($scope.action._checkId($scope.typeImgFlagId)) {
                return alert('Sorry, type with ImgFlagId:' + $scope.typeImgFlagId + 'exists, try another id.');
            }

            /**
             * Check if any record is active
             * (so it means that we just want to edit this record, not just add new one).
             */
            if (activeRecord !== null) {
                // save our active record
                record = {
                    title: $scope.typeTitle.trim(),
                    imgFlagId: $scope.typeImgFlagId,
                    searchBy: $scope.typeSearchBy
                };


                $http.put(api.docTypes + '/' + activeRecord, record)
                    .success(function(data) {
                        $scope.action.reloadList();
                        $scope.action.close();
                        //$rootScope.$broadcast('categories:update');
                    });

            } else {
                // add as new record
                record = {
                    title: $scope.typeTitle.trim(),
                    imgFlagId: $scope.typeImgFlagId,
                    searchBy: $scope.typeSearchBy
                };

                $http.post(api.docTypes, record)
                    .success(function(data) {
                        $scope.action.reloadList();
                        $scope.action.close();
                        //$rootScope.$broadcast('categories:update');
                    });
            }
        },
        close: function() {
            activeRecord = null;
            $scope.action._modalType = null;
            $scope.states.modalVisible = false;
            $scope.states.promptModalVisible = false;
            $scope.action._setTypeData({});
        },
        /*switchery: {
          collection: [],
          clickListener: function(event) {
            //event.target.removeEventListener('click', main.actions.switchery.clickListener, false);
            var switchInput = event.target.parentElement.parentElement.querySelector(GlobalSelectors.switch);

            if(switchInput.getAttribute('checked') === 'checked') {
              switchInput.setAttribute('checked', null);
            } else {
              switchInput.setAttribute('checked', 'checked');
            }

            var state = switchInput.getAttribute('checked') === 'checked' ? false : true;
            var recordId = switchInput.getAttribute(attributes.typeId);

            main.action.disable.set(state, recordId);
          },
          bindAll: function() {
            main.action.switchery.unbindAll(function() {
              $(GlobalSelectors.switch).each(function(idx, el) {
                var switchery = new Switchery(el, {
                  color: main.settings.switcher.color,
                  secondaryColor: main.settings.switcher.secondaryColor,
                  size: main.settings.switcher.size
                });
                main.action.switchery.collection.push(switchery);
              });

              var switchery = document.querySelectorAll(GlobalSelectors.switchery);
              var keys = Object.keys(switchery);

              keys.forEach(function(key) {
                if(angular.isObject(switchery[key])) {
                  switchery[key].addEventListener('click', main.action.switchery.clickListener);
                }
              });
            });
          },
          unbindAll: function(callback) {

            var total = main.action.switchery.collection.length;

            if(total === 0 && angular.isDefined(callback)) {
              return callback();
            }

            main.action.switchery.collection.forEach(function(switcher, index) {
              switcher.destroy();

              if(++index === total) {
                if(angular.isDefined(callback)) {
                  main.action.switchery.collection.length = 0;

                  var switchery = document.querySelectorAll(GlobalSelectors.switchery);
                  var keys = Object.keys(switchery);
                  keys.forEach(function(key) {
                    var element = switchery[key];

                    if(angular.isObject(element)) {
                      element.parentNode.removeChild(element);
                    }
                  });
                  return callback();
                }
              }
            });
          }
        },*/
        /*disable: {
          set: function(state, recordId) {

            var method = state ? api.categoryDisable : api.categoryEnable;

            $http.get(method + '/' + recordId)
              .success(function(data, status){
                console.info('Category Disabled: ' + state);
              });
          }
        }*/
	};

	$scope.$on('$destroy', function() {
		/*sortableDestroy();
        main.action.switchery.unbindAll(function() {
          console.info('Switchery: destroyed');
        });*/
	});

	$timeout(function() {
		$scope.action.reloadList();
	})
});
