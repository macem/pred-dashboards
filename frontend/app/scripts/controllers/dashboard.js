'use strict';

/**
 * @ngdoc function
 * @name mainApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the mainApp
 */
angular.module('mainApp')
		.controller('DashboardCtrl', function ($scope, $timeout, $http, GlobalSelectors, notification, language, api, utils, favorites, $q, $stateParams, $rootScope, message) {

    var sortable,
        mainHeader,
        dashboardSubheader,
        filterQuery,
        filterDocTypes,
        _keys;

    _keys = {
        cookie: {
            filterQuery: 'filter-query',
            filterDocTypes: 'filter-doctypes'
        }
    };

    $scope.query = utils.cookies.get(_keys.cookie.filterQuery);
    filterDocTypes = utils.cookies.get(_keys.cookie.filterDocTypes);

    $scope.categoryId = $stateParams.categoryId;
    $scope.visibleObjects = {};

    $scope.isUndefined = function (thing) {
        return (typeof thing === "undefined");
    };

    var getNewOrder = function() {
        var favHolder = document.querySelector(selectors.favHolder);

        if(favHolder === null) {
            console.warn('Could not find ' + selectors.favHolder);
            return [];
        }

        var reports = favHolder.querySelectorAll(selectors.singleElement);

        if(Object.keys(reports).hasOwnProperty('length')) {
            var newOrder = [],
                    keys = Object.keys(reports);

            keys.forEach(function(key) {
                if(angular.isObject(reports[key])) {
                    var report = reports[key];
                    newOrder.push(report.getAttribute('data-report-id'));
                }
            });

            return newOrder;
        } else {
            return [];
        }
    };
    var sort = function() {
        try {

            $('.js-sortable').each(function(idx, el) {

                var sortable = new Sortable($(el)[0], {
                    animation: 300,
                    draggable: '.js-sortable__draggable',
                    handle: '.js-sortable__handle',
                    sort: !!$(el).data('groupSortable'),
                    group: {
                        name: $(el).data('groupName') || 'default',
                        put: $(el).data('groupPut') ? ($(el).data('groupPut')).split(',') : ['default']
                    },
                    onUpdate: function(event) {
                        var newOrder = getNewOrder();
                        var options = {
                            order: newOrder
                        };

                        $http.put(api.favoritesOrder, options);
                    }
                });
            });

        } catch (e) {
            console.info(e);
        }
    };

    favorites.update(function(data, favs) {
        $scope.favData = data;
        $scope.favorites = favs;

        $timeout(function() {
            sort();
            countVisibleReports();
        });
    });

    $scope.isCategoryActive = function(overrideWithDirective) {
        var category = $scope.categories && $scope.categories.find(function(item) {
                return item.overrideWithDirective === overrideWithDirective;
            });
        return category && category.isActive;
    };
    $scope.getCategoryName = function(overrideWithDirective) {
        var category = $scope.categories && $scope.categories.find(function(item) {
                return item.overrideWithDirective === overrideWithDirective;
            });
        return category && category.name;
    };

    $scope.toggleFavorites = function(event, id) {
        event.stopPropagation();
        event.preventDefault();

        var favo = document.querySelector('#favorites');
        var originalHeight = favo.clientHeight;

        favorites.toggle(event, id, function(data, favs) {
            $scope.favData = data;
            $scope.favorites = favs;

            $timeout(function() {
                var currentHeight = favo.clientHeight;
                var diff = currentHeight - originalHeight;

                window.scrollTo(0, window.pageYOffset + diff);

                sort();
                countVisibleReports();
            });
        });
    };

    /**
     * CSS selectors.
     * @type {{dashboardSubheader: string, singleElement: string}}
     */
    var selectors = {
        dashboardSubheader: 'dashboardSubheader',
        singleElement: 'div.box',
        hideFilter01: 'hideFilter01',
        hideFilter02: 'hideFilter02',
        reportCounter: '#reportCounter',
        favHolder: '#favHolder',
        groups: 'div.collapsible:not(.group-mashup):not(.group-favorites)',
        notHidden: '.boxElement:not([class*=hideFilter]):not([class*="ng-hide"])',
        hideBox: 'hideBox',
        isSearchEmpty: 'div.collapsible:not([class*=hideBox])',
        emptyResultsMessage: 'div#emptyResultsMessage',
        removeTopPadding: 'removeTopPadding',
        chosen: {
            select: '.js-select--tags + .chosen-container input.default',
            tags: '.js-select, .js-select--tags'
        },
        categoryDisabled: 'nav__item--disabled'
    };

    /**
     * Filters for available dashboard's elements.
     * @type {{type: Array, title: string}}
     */
    var filter = {
        type: []
    };

    /**
     * Mapping to the proper
     * CSS selectors for each
     * docType.
     * @type {{Tibco Spotfire: string, Business Objects: string, Power Point: string, Excel: string}}
     */
    var typesToClass = {
        'Tibco Spotfire': 'box--type-1',
        'Business Objects': 'box--type-2',
        'Power Point': 'box--type-3',
        'Excel': 'box--type-4',
        'Custom': 'box--type-6',
        'PDF': 'box--type-7',
        'Shortcut': 'box--type-8',
        'Tableau': 'box--type-9',
    };

    $scope.imgPath = api.cover;

    var appendSpecials = function() {

        var moveDOMElemens = [
            {
                from: '#favorites',
                to: '.favorites'
            },
            {
                from: '#mashups',
                to: '.mashups'
            }
        ];

        moveDOMElemens.forEach(function(settings) {
            var moveFrom = document.querySelector(settings.from);
            var moveTo = document.querySelector(settings.to);

            /*if ($(moveTo).hasClass("ng-hide")) {
                $(moveFrom).addClass('ng-hide');
            } else {
                $(moveFrom).removeClass('ng-hide');
            }*/
            // TODO order
            /*if (moveFrom && moveFrom.parentNode && moveTo) {
                moveFrom.parentNode.replaceChild(moveFrom, moveTo);
            }*/
        });
    };

    /**
     * Count reports
     * which are visible
     * on dashboard (in case of filtering).
     */
    var countVisibleReports = function() {

        var visibleElements = document.querySelectorAll(selectors.notHidden).length;

        /**
         * Append the current
         * count of visible reports.
         * @type {string}
         */
        //[PPD-175] document.querySelector(selectors.reportCounter).innerText = visibleElements + ' ' + ((visibleElements > 1 || visibleElements === 0) ? language.manyReports : language.oneReport);

        /**
         * Check if we have any
         * search results.
         * If not, we will display
         * some message, e.g.: 'Sorry, we could not find XYZ'
         * @type {boolean}
         */
        var emptyResultsMessage = document.querySelector(selectors.emptyResultsMessage);

        if (!emptyResultsMessage || !emptyResultsMessage.classList) {
            emptyResultsMessage.classList = [];
        }

        if(visibleElements === 0) {
            emptyResultsMessage.classList.remove(selectors.hideBox);
        } else {
            emptyResultsMessage.classList.add(selectors.hideBox);
        }
    };

    /**
     * Add 'hidden' class
     * to the elements which are not on the
     * filter.type list.
     */
    var applyFilters = function() {
        var reports = document.querySelectorAll('[doctype-id]');

        for(var report in reports) {
            if(reports.hasOwnProperty(report) && report !== 'length') {
                var holder = reports[report];
                if(typeof holder.getAttribute === 'undefined') {
                    return;
                }
                if(filter.type.indexOf(holder.getAttribute('doctype-id')) >= 0 || filter.type.length === 0) {
                    holder.classList.remove(selectors.hideFilter02);
                } else {
                    holder.classList.add(selectors.hideFilter02);
                }
            }
        }
    };

    /**
     * Push selected filters
     * to 'filter.type' object.
     * @param event
     */
    var setFilters = function(event) {
        var options = event.delegateTarget.parentElement.querySelectorAll('li.select2-selection__choice');
        var keys = Object.keys(options);

        /**
         * Clear the current 'type'
         * collection.
         * @type {number}
         */
        filter.type.length = 0;

        keys.forEach(function(key) {
            var title = options[key].title.trim();
            if(angular.isDefined(title)) {
                filter.type.push(typesToClass[title]);
            }
            title = undefined;
        });

    };

    var saveDocTypeFilter = function() {
        //console.warn('saving %O', filter.type);
        utils.cookies.set(_keys.cookie.filterDocTypes, filter.type);
    };

    var applyDocTypeFilter = function() {
        console.info('Bind filters.');

        var applyChanges = function() {
            saveDocTypeFilter();
            applyFilters();
            countVisibleReports();
        };

        $(selectors.chosen.tags).chosen({
            disable_search_threshold: -1,
            search_contains: true
        })
        .change(function(event, status) {
            if(status.selected) {
                filter.type.push(status.selected);
            } else {
                var index = filter.type.indexOf(status.deselected);
                filter.type.splice(index, 1);
            }
            applyChanges();
        });

        $(selectors.chosen.select).on('keydown', function(e) {
            var keycode = (e.keyCode ? e.keyCode : e.which);
            var value = $(this).val();

            if (keycode == '13') {
                $(this).parents('.chosen-container').prev().
                        append('<option selected>' + value + '</option>').
                        trigger('chosen:updated');
            }
        });

        applyChanges();
    };

    var scrollToSelectedCategory = function() {
        if ($stateParams.categoryId !== null) {
            var fixedOffset = 60;

            $timeout(function() {
                try {
                    $('html, body').animate({
                        scrollTop: $('#category-' + $stateParams.categoryId).offset().top - $('header').outerHeight() + fixedOffset
                    }, 1000);
                } catch (e) {}
            });
        }
    };

    $timeout(function() {

        /**
         * Append 'dashboardSubheader' (directive-dashboard-inside.html)
         * to the 'mainHeader' (directive-navigation.html)
         */

        mainHeader = document.getElementById(GlobalSelectors.mainHeader);
        dashboardSubheader = document.getElementById(selectors.dashboardSubheader);
        if (mainHeader === null || dashboardSubheader === null) {
            // notification.push(language.DOMElementNotFound, GlobalSelectors.mainHeader, selectors.dashboardSubheader)
            $timeout(function() {
                mainHeader = document.getElementById(GlobalSelectors.mainHeader);
                dashboardSubheader = document.getElementById(selectors.dashboardSubheader);
                if (mainHeader && dashboardSubheader) {
                    mainHeader.appendChild(dashboardSubheader);
                }
            }, 3500);
        } else {
            mainHeader.appendChild(dashboardSubheader);
        }

    });


    /**
     * Getting the list of
     * available categories with
     * related items.
     */
    var _categories = $http.get(api.categoriesFull)
            .success(function (data, status, headers, config) {
                $scope.categories = data.filter(function(item) {
                    return !item.displayHome;
                });
              });

    var _mashups = $http.get(api.mashups)
            .success(function (data, status, headers, config) {
                $scope.mashups = data;
            });

    var _docTypes = $http.get(api.docTypes)
            .success(function (data, status, headers, config) {
                $scope.docTypes = data;
            });

    /**
     * Chaining HTTP requests.
     * We need to init some
     * jQuery plugins after
     * all data is loaded
     * and rendered in the
     * HTML view.
     */
    $q.all([
        _categories,
        _mashups,
        _docTypes
    ]).then(function(results) {
        /*
            Using Sortable plugin for sortable functionality
            https://github.com/RubaXa/Sortable
            */
        $timeout(function() {

            if (filterDocTypes instanceof Array && filterDocTypes.length > 0) {
                filter.type = filterDocTypes;
                var chosenOptions = document.querySelectorAll('select.docTypeFilter option');

                filterDocTypes.forEach(function(docType) {
                    var docTypes = _.sortBy($scope.docTypes, 'title');
                    var find = _.findWhere(docTypes, {id: docType});
                    var index = docTypes.indexOf(find);
                    chosenOptions[index].setAttribute('selected', 'selected');
                });
            }

            sort();
            applyDocTypeFilter();

            /*
                Using custom JS code for collapsible functionality.
                Feel free to change / modify the following code
                */
            $(".js-collapsible .collapsible__trigger").on('click', function(e) {
                if(!e.target.getAttribute('data-prevent')) {
                    $(this).parents('.js-collapsible').toggleClass('is-collapsed');
                }
            });

            /**
             * Count visible
             * reports.
             */
            $timeout(function() {
                countVisibleReports();
                scrollToSelectedCategory();
                document.body.scrollTop = document.documentElement.scrollTop = 0;
                filterWithQuery($scope.query);
                appendSpecials();
            }, 100);
        });
    });

    var filterWithQuery = function(newQuery) {
        /**
         * Remove double spaces.
         */
        newQuery = newQuery || '';
        newQuery = newQuery.replace(/\s+/igm, ' ');

        utils.cookies.set(_keys.cookie.filterQuery, newQuery);

        var visibleElements = document.querySelectorAll(selectors.singleElement);
        var keys = Object.keys(visibleElements);
        var pattern = new RegExp(utils.escapeRegExp(newQuery), 'i');

        keys.forEach(function(key) {
            var item = visibleElements[key];

            if(angular.isObject(item)) {
                var title = item.querySelector('h4').textContent.trim() || '';
                var description = item.querySelector(GlobalSelectors.box.description).textContent.trim() || '';

                if(pattern.test(description) && newQuery.length > 0) {
                    item.classList.add(GlobalSelectors.box.hovered);
                } else {
                    item.classList.remove(GlobalSelectors.box.hovered);
                }

                if (pattern.test(title) || pattern.test(description)) {
                    item.parentElement.classList.remove(selectors.hideFilter01);

                } else {
                    item.parentElement.classList.add(selectors.hideFilter01);
                }
            }
        });

        countVisibleReports();
    };

    /**
     * Custom filter query
     * - watcher.
     */
    $scope.$watch('query', function(nQuery, oQuery) {
        if (angular.isDefined(nQuery) || angular.isDefined(oQuery)) {
            var newQuery = (nQuery === oQuery) ? oQuery : nQuery;
            if (newQuery === null) {
                return;
            }

            filterWithQuery(newQuery);
        }
    }, true);

    $scope.openReport = function(reportId, url, docType) {
        return utils.openReport(reportId, url, docType);
    };

    /**
     * Remove the sortable functionality completely
     * when directive is destroyed.
     */
    $scope.$on('$destroy', function() {
        /**
         * Try to destroy sortable
         * object.
         */
        try {
            sortable.destroy();
        } catch(e) {
            console.warn('Could not destroy SORTABLE widget.');
        }

        /**
         * Try to remove custom subheader
         * from the <header> element,
         * because we will use it only
         * on dashboard view.
         */
        try {
            mainHeader.removeChild(dashboardSubheader);
        } catch(e) {
            console.warn('Could not remove dashboard\'s SUBHEADER from the <header> section.');
        }
    });

    $scope.actions = {
        clearAllFilters: function() {
            $scope.query = '';
            $('header .search-choice-close').click();
        },
        tellToNav: function(value) {
            $rootScope.$emit('footer:listener', value);
        }
    };

});
