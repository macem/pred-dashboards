'use strict';

/**
 * @ngdoc function
 * @name mainApp.controller:GesturesCtrl
 * @description
 * # GesturesCtrl
 * Controller of the mainApp
 */
angular.module('mainApp')
		.controller('GesturesCtrl', function ($scope, $state) {
            $scope.state = $state;

			var main = this;
			main.actions = {
				menu: {
					open: function() {
						$('.sideNav').addClass('sideNav--pulled');
						$('.app').addClass('app--pushed');
					},
					close: function() {
						$('.sideNav').removeClass('sideNav--pulled');
						$('.app').removeClass('app--pushed');
					}
				}
			};
		});
