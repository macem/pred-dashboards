'use strict';

/**
 * @ngdoc function
 * @name mainApp.controller:MashuviewCtrl
 * @description
 * # MashuviewCtrl
 * Controller of the mainApp
 */
angular.module('mainApp')
		.controller('MashupviewCtrl', function ($scope, $http, api, $stateParams, $state, $timeout, $compile, User, utils, message) {

			var main = this;

			main.settings = {
				view: {
					columns: {
						quantity: 1
					}
				}
			};
			main.contents = {};
			main.states = {
				showConfirmationBox: $stateParams.created ? true : false,
				isSuperUser: angular.copy(User.isSuperUser()),
				showRemoval: false
			};

			main.css = {
				classes: {

				},
				selectors: {
					dropdown: 'div.mashupView [data-js="dropdown"]'
				}
			};

			main.actions = {
				confirmation: {
					open: function() {
						main.states.showConfirmationBox = true;
					},
					close: function() {
						main.states.showConfirmationBox = false;
					}
				},
				view: {
					bindDropdown: function() {
						$timeout(function() {
							$(main.css.selectors.dropdown).each(function(idx, el) {
								$(el).jBox('Tooltip', {
									trigger: 'click',
									fixed: true,
									closeOnClick: true,
									addClass: 'dropdown font-type-2 no-smooth',
									position: {
										x: 'center',
										y: 'bottom'
									},
									content: $compile(angular.element($(el).find('.dropdown__body').html()))($scope)
								});
							});
						});
					},
					setColumns: function(quantity, event) {
						main.settings.view.columns.quantity = quantity;
					}
				},
				favorites: {
					add: function(mashupId) {
						mashupId = mashupId | 0;
						alert('Not implemented yet.');
					}
				},
				mashup: {
					remove: function(mashupId) {
						$http.delete(api.mashups + '/' + mashupId)
							.success(function() {
								main.states.showRemoval = false;
								$state.go('dashboard', {categoryId:''});
								$timeout(function() {
									message('Mashup has been deleted', 'Your mashup "' + angular.copy(main.contents.mashup.name) + '" has been deleted.', 'success');
								},250);
							});
					},
					openInFullMode: function(url, reportId) {
						var element = document.querySelector('iframe[frame-report-id="' + reportId + '"]');
						utils.openInFullMode(element);
					}
				}
			};

			if($stateParams.id.length !== 0) {
				main.actions.view.bindDropdown();

				$http.get(api.mashups + '/' + $stateParams.id)
					.success(function(data, status) {
						main.contents.mashup = data;
					})
					.error(function(data, status, headers, config) {
						if(status === 404) {
							$state.go('notfound');
						}
					});
			} else {
				$state.go('notfound');
			}
		});
