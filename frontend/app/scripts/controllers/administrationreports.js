'use strict';

/**
 * @ngdoc function
 * @name mainApp.controller:AdministrationreportsCtrl
 * @description
 * # AdministrationreportsCtrl
 * Controller of the mainApp
 */
angular.module('mainApp')
		.controller('AdministrationreportsCtrl', function ($scope, $timeout, $http, api, $q, $state, GlobalSelectors, switchery, $log) {
			var loadReports,
				main;

			main = this;

			main.contents = {
				recordTitle: null,
				counter: {
					active: 0,
					inactive: 0
				}
			};

			main.states = {
				modal: {
					edit: false,
					remove: false
				},
				activeRecord: null
			};

			main.types = {
				form: {
					edition: 'edition'
				}
			};

			main.settings = {
				TAG: '[' + angular.copy($state.current.name) + '] ',
				switcher: switchery
			};

			main.cssClasses = {
				record: {
					disabled: 'icon--complete',
					enabled: 'icon--padlock',
					switchery: 'switchery'
				},
				group: {
					sortable: 'js-sortable'
				}
			};

			main.selectors = {
				coverPhoto: 'coverPhoto',
				switchery: 'span.switchery',
				switchInput: 'input.js-switch',
				sortable: 'div.js-sortable',
				row: 'div.row',
				modal: {
					edit: 'div.editReport'
				}
			};

			main.attributes = {
				reportId: 'data-report-id',
				categoryId: 'data-group-put',
				recordId: 'record-id'
			};

			main.form = {
				edition: {

				},
				reset: function(formId) {
					if(main.form.hasOwnProperty(formId)) {
						console.info(main.settings.TAG, 'Reset: ' + formId);

						/**
						 * We need to remove 'viewValue' from our negative-validated inputs
						 * to be sure that everything is 'clean' now.
						 */
						var keys = Object.keys($scope.form);
						keys.forEach(function(key) {
							if(/^\$/.test(key) && $scope.form[key] && $scope.form[key].hasOwnProperty('$viewValue')) {
								delete $scope.form[key].$viewValue;
							}
						});

						/**
						 * Reset validation
						 * results.
						 */
						$scope.form.$setUntouched();
						$scope.form.$setPristine();

						/**
						 * Clear view value
						 * for fields with bad validation.
						 */

						main.form[formId] = undefined;

						var edit = document.querySelector(main.selectors.modal.edit);
						var inputs = document.querySelectorAll('input[type="text"]');

						var keys = Object.keys(inputs);
						keys.forEach(function(key) {
							inputs[key].value = '';
						});

					}
				}
			};

			main.promises = [];

			main.actions = {
				count: function() {
					$timeout(function() {
						var getSwitchers = document.querySelectorAll(main.selectors.switchInput);
						var active = 0;
						var inactive = 0;

						for (var report in getSwitchers) {
							if(getSwitchers.hasOwnProperty(report) && report !== 'length') {
								var switcher = getSwitchers[report];
								if(switcher.getAttribute('checked') && switcher.getAttribute('checked') !== 'null') {
									active++;
								} else {
									inactive++;
								}
							}
						}

						main.contents.counter.active = active;
						main.contents.counter.inactive = inactive;

						$log.info('Reports: active = %d, inactive = %d', active, inactive);
					});
				},
				sort: {
					collection: [],
					saveOrdering: function(categoryId) {
						var reportsInCategory = document.querySelectorAll(main.selectors.sortable + '[' + main.attributes.categoryId + '="' + categoryId + '"] ' + main.selectors.row);
						main.actions.sort.collection.length = 0;

						for(var report in reportsInCategory) {
							if(reportsInCategory.hasOwnProperty(report) && report !== 'length') {
								var recordId = reportsInCategory[report].getAttribute(main.attributes.recordId);
								main.actions.sort.collection.push(recordId);
							}
						}

						$http.put(api.categoriesContentOrdering + '/' + categoryId, { contentOrdering: main.actions.sort.collection });
					},
					bindAll: function() {
						$('.js-sortable').each(function(idx, el) {
							main.actions.sort.collection.push(new Sortable($(el)[0], {
								animation: 300,
								draggable: '.js-sortable__draggable',
								handle: '.js-sortable__handle',
								sort: !($(el).data('groupSortable') == ""),
								group: {
									name: $(el).data('groupName') || null,
									pull: $(el).data('groupPull') || true,
									put: $(el).data('groupPut') || true
								},
								onUpdate: function(event) {
									main.actions.sort.saveOrdering(event.from.getAttribute(main.attributes.categoryId));
								},
								onAdd: function(event) {
									console.log(event);
								}
							}));
						});
					},
					unbindAll: function() {

					}
				},
				switchery: {
					collection: [],
					clickListener: function(event) {
						//event.target.removeEventListener('click', main.actions.switchery.clickListener, false);
						var switchInput = event.target.parentElement.parentElement.querySelector(GlobalSelectors.switch);

						if(switchInput.getAttribute('checked') === 'checked') {
							switchInput.setAttribute('checked', null);
						} else {
							switchInput.setAttribute('checked', 'checked');
						}

						var state = switchInput.getAttribute('checked') === 'checked' ? false : true;
						var recordId = switchInput.getAttribute(main.attributes.reportId);

						main.actions.disable.set(state, recordId);
					},
					bindAll: function() {
						main.actions.switchery.unbindAll(function() {
							$(GlobalSelectors.switch).each(function(idx, el) {
								var switchery = new Switchery(el, {
									color: main.settings.switcher.color,
									secondaryColor: main.settings.switcher.secondaryColor,
									size: main.settings.switcher.size
								});
								main.actions.switchery.collection.push(switchery);
							});

							var switchery = document.querySelectorAll(GlobalSelectors.switchery);
							var keys = Object.keys(switchery);

							keys.forEach(function(key) {
								if(angular.isObject(switchery[key])) {
									switchery[key].addEventListener('click', main.actions.switchery.clickListener);
								}
							});
						});
					},
					unbindAll: function(callback) {

						var total = main.actions.switchery.collection.length;

						if(total === 0 && angular.isDefined(callback)) {
							return callback();
						}

						main.actions.switchery.collection.forEach(function(switcher, index) {
							switcher.destroy();

							if(++index === total) {
								if(angular.isDefined(callback)) {
									main.actions.switchery.collection.length = 0;

									var switchery = document.querySelectorAll(GlobalSelectors.switchery);
									var keys = Object.keys(switchery);
									keys.forEach(function(key) {
										var element = switchery[key];

										if(angular.isObject(element)) {
											element.parentNode.removeChild(element);
										}
									});
									return callback();
								}
							}
						});
					}
				},
				chosen: {
					bindAll: function() {

						$timeout(function() {
							$(main.selectors.modal.edit + " .js-select").chosen({
								disable_search_threshold: -1,
								search_contains: true
							});
						});

					},
					unbindAll: function() {
						$(main.selectors.modal.edit + " .js-select").chosen('destroy');
					}
				},
				load: {
					categoriesFull: function() {
						return $http.get(api.categories)
								.success(function (data, status) {
									main.contents.categories = data || [];
									$timeout(function() {
										main.actions.sort.bindAll();
										main.actions.count();
									});
								});
					},
					docTypes: function() {
						return $http.get(api.docTypes)
								.success(function (data, status) {
									main.contents.docTypes = data || [];
								});
					},
					accessGroups: function() {
						return $http.get(api.accessGroups)
								.success(function (data, status) {
									main.contents.accessGroups = data || [];
								});
					},
					all: function() {
						var toLoad = main.actions.load;
						var keys = Object.keys(toLoad);

						main.promises.length = 0;

						keys.forEach(function(key) {
							if(key !== 'all' && typeof toLoad[key] === 'function') {
								console.info(main.settings.TAG, 'Loading: ' + key);
								main.promises.push(toLoad[key]());
							}
						});

					}
				},
				activeRecord: {
					set: function(recordId) {
						main.states.activeRecord = recordId;
					},
					get: function() {
						return main.states.activeRecord;
					},
					clear: function() {
						main.states.activeRecord = null;
					}
				},
				edit: {
					open: function() {
						/**
						 * Show modal window.
						 * @type {boolean}
						 */
						main.states.modal.edit = true;

						/**
						 * If Edit Mode
						 */
						if(main.states.activeRecord !== null) {
							$http.get(api.items + '/' + main.states.activeRecord)
								.success(function (data, status) {

									/**
									 * Check if the given value is an object or not.
									 * If we have an 'Object', we should map rather its 'id',
									 * not string or something...
									 */

									var mapToValidObject =_.mapObject(data, function(val, key) {
										return val !== null && val.hasOwnProperty('id') ? val.id : val;
									});



									/**
									 * Let's put our mapped data
									 * to the form's inputs.
									 */
									main.form.edition = angular.copy(mapToValidObject);

									/**
									 * Bind chosen plugin.
									 */
									main.actions.chosen.bindAll();
								});
						} else {
							main.actions.chosen.bindAll();
						}
					},
					close: function() {
						/**
						 * Hide modal window.
						 * @type {boolean}
						 */
						main.states.modal.edit = false;

						/**
						 * Clear the current active record.
						 */
						main.actions.activeRecord.clear();

						/**
						 * Reset form
						 * and clear its validation.
						 */
						main.form.reset(main.types.form.edition);

						/**
						 * Unbind chosen plugin.
						 */
						main.actions.chosen.unbindAll();
					},
					save: function() {
						var record,
							formData,
							keys,
							file,
							httpOptions;

						console.info(main.settings.TAG, 'Saving...');

						file = document.getElementById(main.selectors.coverPhoto);
						file = file.files[0];

						record = angular.copy(main.form.edition);
            record.descriptionShort = record.descriptionShort.trim();
            record.title = record.title.trim();

						keys = Object.keys(record);

						formData = new FormData();

						keys.forEach(function(key) {
							if(key !== 'coverPhoto') {
								if(record[key] === null) {
									record[key] = '';
								}
								formData.append(key, record[key]);
							}
						});

						formData.append('cover', file);

						httpOptions = {
							transformRequest: angular.identity,
							headers: {
								'Content-Type': undefined
							}
						};

						if(main.states.activeRecord !== null) {
							$http.put(api.items + '/' + main.states.activeRecord, formData, httpOptions)
									.success(function(data, status){
										main.actions.edit.close();
										main.actions.load.categoriesFull();
										$timeout(function() {
											main.actions.switchery.bindAll();
										});
									});
						} else {
							$http.post(api.items, formData, httpOptions)
								.success(function(data, status){
										main.actions.edit.close();
										main.actions.load.categoriesFull();
										$timeout(function() {
											main.actions.switchery.bindAll();
										});
								});
						}

					}
				},
				remove: {
					open: function(recordId, recordTitle) {
						/**
						 * Show modal window.
						 * @type {boolean}
						 */
						main.states.modal.remove = true;

						/**
						 * Set active record.
						 */
						main.actions.activeRecord.set(recordId);

						/**
						 * Set current record's title.
						 */
						main.contents.recordTitle = recordTitle;
					},
					close: function() {
						/**
						 * Hide modal window.
						 * @type {boolean}
						 */
						main.states.modal.remove = false;

						/**
						 * Clear the current active record.
						 */
						main.actions.activeRecord.clear();
					},
					save: function() {
						$http.delete(api.items + '/' + main.actions.activeRecord.get())
							.success(function(data, status){
								/**
								 * Close modal.
								 */
								main.actions.remove.close();

								/**
								 * Reload report list.
								 */
								main.actions.load.categoriesFull();
							});
					}
				},
				disable: {
					set: function(state, recordId) {

						var method = state ? api.itemsDisable : api.itemsEnable;

						$http.get(method + '/' + recordId)
							.success(function(data, status){
								console.info(main.settings.TAG, 'Disabled: ' + state);
								main.actions.count();
							});
					}
				}
			};

			main.testor = function(report) {
				console.log('change');
				report.isActive = false;
			};

			/**
			 * Load all http data
			 * via defined in 'main.actions.load' functions.
			 */
			main.actions.load.all();

			/**
			 * When scope is destroyed,
			 * we should unbind/destroy
			 * the all unnecessary elements.
			 */
			$scope.$on('$destroy', function() {
				main.actions.switchery.unbindAll(function() {
					console.info(main.settings.TAG, 'Switchery: destroyed')
				});
			});

			/**
			 * Apply 'switchery' jQuery plugin
			 * when we have the all necessary data
			 * from API.
			 */
			$q.all(main.promises)
				.then(function() {
					console.info(main.settings.TAG, 'API data successfully loaded.');
					$timeout(function() {
						main.actions.switchery.bindAll();
					});
				});
		});
