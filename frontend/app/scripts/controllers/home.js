'use strict';

/**
 * @ngdoc function
 * @name mainApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the mainApp
 */
angular.module('mainApp')
		.controller('HomeCtrl', function ($scope, $timeout, $http, GlobalSelectors, notification, language, api, utils, favorites, $q, $stateParams, $rootScope, message) {

        $http.get(api.categoriesFull)
            .success(function (data, status, headers, config) {
                $scope.categories = data.filter(function(item) {
                    return item.displayHome;
                });
            });

		});