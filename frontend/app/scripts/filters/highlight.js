'use strict';

/**
 * @ngdoc filter
 * @name mainApp.filter:highlight
 * @function
 * @description
 * # highlight
 * Filter in the mainApp.
 */
angular.module('mainApp')
		.filter('highlight', function ($sce, utils) {
			return function(str, termsToHighlight) {
				str = str || '';
				str = str.replace(/\s+/igm, ' ');
				// Sort terms by length
				termsToHighlight.sort(function(a, b) {
					return b.length - a.length;
				});
				// Regex to simultaneously replace terms
				var regex = new RegExp('(' + utils.escapeRegExp(termsToHighlight.join('|').replace(/\s+/igm, ' ')) + ')', 'ig');
				return $sce.trustAsHtml(str.replace(regex, '<span class="match">$&</span>'));
			};
		});
