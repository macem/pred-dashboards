// Generated on 2015-05-11 using generator-angular 0.9.8
'use strict';

try {
	var os = require('os');
	var ifaces = os.networkInterfaces();
	var rocheComputerIP = ifaces[Object.keys(ifaces)[0]][1].address;
} catch(e) {
	var rocheComputerIP = 'ircilocal.roche.com';
}

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

	// Load grunt tasks automatically
	require('load-grunt-tasks')(grunt);

	// Time how long tasks take. Can help when optimizing build times
	require('time-grunt')(grunt);

	// Configurable paths for the application
	var appConfig = {
		app: require('./bower.json').appPath || 'app',
		dist: 'dist'
	};

	// Define the configuration for all the tasks
	grunt.initConfig({

		package: grunt.file.readJSON('package.json'),

		// Project settings
		yeoman: appConfig,

		// Watches files for changes and runs tasks based on the changed files
		watch: {
			bower: {
				files: ['bower.json'],
				tasks: ['wiredep']
			},
			js: {
				files: ['<%= yeoman.app %>/scripts/{,*/}*.js'],
				tasks: ['newer:jshint:all'],
				options: {
					livereload: '<%= connect.options.livereload %>'
				}
			},
			jsTest: {
				files: ['test/spec/{,*/}*.js'],
				tasks: ['newer:jshint:test', 'karma']
			},
			styles: {
				files: ['<%= yeoman.app %>/styles/{,*/}*.css'],
				tasks: ['newer:copy:styles', 'autoprefixer']
			},
			gruntfile: {
				files: ['Gruntfile.js']
			},
			livereload: {
				options: {
					livereload: '<%= connect.options.livereload %>'
				},
				files: [
					'<%= yeoman.app %>/{,*/}*.html',
					'.tmp/styles/{,*/}*.css',
					'<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
				]
			}
		},

		// The actual grunt server settings
		connect: {
			options: {
				port: 1338,
				// Change this to '0.0.0.0' to access the server from outside.
				hostname: '0.0.0.0',
				livereload: 35728
			},
			livereload: {
				options: {
					open: false,
					middleware: function (connect) {
						return [
							connect.static('.tmp'),
							connect().use(
									'/bower_components',
									connect.static('./bower_components')
							),
							connect.static(appConfig.app)
						];
					}
				}
			},
			test: {
				options: {
					port: 9001,
					middleware: function (connect) {
						return [
							connect.static('.tmp'),
							connect.static('test'),
							connect().use(
									'/bower_components',
									connect.static('./bower_components')
							),
							connect.static(appConfig.app)
						];
					}
				}
			},
			dist: {
				options: {
					open: true,
					base: '<%= yeoman.dist %>'
				}
			}
		},

		// Make sure code styles are up to par and there are no obvious mistakes
		jshint: {
			options: {
				jshintrc: '.jshintrc',
				reporter: require('jshint-stylish')
			},
			all: {
				src: [
					'Gruntfile.js',
					'<%= yeoman.app %>/scripts/{,*/}*.js'
				]
			},
			test: {
				options: {
					jshintrc: 'test/.jshintrc'
				},
				src: ['test/spec/{,*/}*.js']
			}
		},

		// Empties folders to start fresh
		clean: {
			dist: {
				files: [{
					dot: true,
					src: [
						'.tmp',
						'<%= yeoman.dist %>/{,*/}*',
						'!<%= yeoman.dist %>/.git*'
					]
				}]
			},
			server: '.tmp'
		},

		// Add vendor prefixed styles
		autoprefixer: {
			options: {
				browsers: ['last 1 version']
			},
			dist: {
				files: [{
					expand: true,
					cwd: '.tmp/styles/',
					src: '{,*/}*.css',
					dest: '.tmp/styles/'
				}]
			}
		},

		// Automatically inject Bower components into the app
		wiredep: {
			app: {
				src: ['<%= yeoman.app %>/index.html'],
				ignorePath: /\.\.\//
			}
		},

		// Renames files for browser caching purposes
		filerev: {
			dist: {
				src: [
					'<%= yeoman.dist %>/scripts/{,*/}*.js',
					'<%= yeoman.dist %>/styles/{,*/}*.css',
					'<%= yeoman.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
					'<%= yeoman.dist %>/styles/fonts/*'
				]
			}
		},

		// Reads HTML for usemin blocks to enable smart builds that automatically
		// concat, minify and revision files. Creates configurations in memory so
		// additional tasks can operate on them
		useminPrepare: {
			html: '<%= yeoman.app %>/index.html',
			options: {
				dest: '<%= yeoman.dist %>',
				flow: {
					html: {
						steps: {
							js: ['concat', 'uglifyjs'],
							css: ['cssmin']
						},
						post: {}
					}
				}
			}
		},

		// Performs rewrites based on filerev and the useminPrepare configuration
		usemin: {
			html: ['<%= yeoman.dist %>/{,*/}*.html'],
			css: ['<%= yeoman.dist %>/styles/{,*/}*.css'],
			options: {
				assetsDirs: ['<%= yeoman.dist %>', '<%= yeoman.dist %>/images']
			}
		},

		// The following *-min tasks will produce minified files in the dist folder
		// By default, your `index.html`'s <!-- Usemin block --> will take care of
		// minification. These next options are pre-configured if you do not wish
		// to use the Usemin blocks.
		// cssmin: {
		//   dist: {
		//     files: {
		//       '<%= yeoman.dist %>/styles/main.css': [
		//         '.tmp/styles/{,*/}*.css'
		//       ]
		//     }
		//   }
		// },
		// uglify: {
		//   dist: {
		//     files: {
		//       '<%= yeoman.dist %>/scripts/scripts.js': [
		//         '<%= yeoman.dist %>/scripts/scripts.js'
		//       ]
		//     }
		//   }
		// },
		// concat: {
		//   dist: {}
		// },

		imagemin: {
			dist: {
				files: [{
					expand: true,
					cwd: '<%= yeoman.app %>/images',
					src: '{,*/}*.{png,jpg,jpeg,gif}',
					dest: '<%= yeoman.dist %>/images'
				}]
			}
		},

		svgmin: {
			dist: {
				files: [{
					expand: true,
					cwd: '<%= yeoman.app %>/images',
					src: '{,*/}*.svg',
					dest: '<%= yeoman.dist %>/images'
				}]
			}
		},

		htmlmin: {
			dist: {
				options: {
					collapseWhitespace: true,
					conservativeCollapse: true,
					collapseBooleanAttributes: true,
					removeCommentsFromCDATA: true,
					removeOptionalTags: true
				},
				files: [{
					expand: true,
					cwd: '<%= yeoman.dist %>',
					src: ['*.html', 'templates/{,*/}*.html'],
					dest: '<%= yeoman.dist %>'
				}]
			}
		},

		// ng-annotate tries to make the code safe for minification automatically
		// by using the Angular long form for dependency injection.
		ngAnnotate: {
			dist: {
				files: [{
					expand: true,
					cwd: '.tmp/concat/scripts',
					src: ['*.js', '!oldieshim.js'],
					dest: '.tmp/concat/scripts'
				}]
			}
		},

		// Replace Google CDN references
		cdnify: {
			dist: {
				html: ['<%= yeoman.dist %>/*.html']
			}
		},

		// Copies remaining files to places other tasks can use
		copy: {
			dist: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= yeoman.app %>',
					dest: '<%= yeoman.dist %>',
					src: [
						'*.{ico,png,txt}',
						'.htaccess',
						'*.html',
						'views/{,*/}*.html',
						'templates/{,*/}*.html',
						'images/{,*/}*.{webp}',
						'fonts/*',
						'styles/init/**/*',
						'scripts/init/**/*',
                        'help/**/*'
					]
				}, {
					expand: true,
					cwd: '.tmp/images',
					dest: '<%= yeoman.dist %>/images',
					src: ['generated/*']
				}, {
					expand: true,
					cwd: 'bower_components/bootstrap/dist',
					src: 'fonts/*',
					dest: '<%= yeoman.dist %>'
				}]
			},
			styles: {
				expand: true,
				cwd: '<%= yeoman.app %>/styles',
				dest: '.tmp/styles/',
				src: '{,*/}*.css'
				//'test/spec/**/*.js'
			}
		},

		// Run some tasks in parallel to speed up the build process
		concurrent: {
			server: [
				'copy:styles'
			],
			test: [
				'copy:styles'
			],
			dist: [
				'copy:styles',
				'imagemin',
				'svgmin'
			]
		},

		// Test settings
		karma: {
			unit: {
				configFile: 'test/karma.conf.js',
				singleRun: true
			}
		},

		/**
		 * Custom environment variables.
		 * WARNING: The config.js file is generated
		 * only once per serve or build process.
		 *
		 * If you need to manually regenerate
		 * the proper config, use these commands from
		 * your command line:
		 *
		 * grunt ngconstant:development
		 * or
		 * ngconstant:production
		 */
		ngconstant: {
			// Options for all targets
			options: {
				space: '  ',
				wrap: '"use strict";\n\n /**\n\tWARNING: This file is generated automatically.\n\tYou should NEVER provide any changes to this manually!\n\tAll the magic happens in the Gruntfile.js file.\n**/\n\n{%= __ngModule %}',
				name: 'config',
				constants: {
					ENV: {
						package: {
							version: '<%= package.version %>',
							description: '<%= package.description %>'
						}
					}
				}
			},
			// Environment targets
			local: {
				options: {
					dest: '<%= yeoman.app %>/scripts/config.js'
				},
				constants: {
					ENV: {
						name: 'local',
                        apiEndpoint: 'http://localhost:1337/api/v1'
						// apiEndpoint: 'http://' + rocheComputerIP + ':1337/api/v1'
					}
				}
			},
			development: {
				options: {
					dest: '<%= yeoman.app %>/scripts/config.js'
				},
				constants: {
					ENV: {
						name: 'development',
                        apiEndpoint: 'http://preddashboard-dev.roche.com:1337/api/v1'
						// apiEndpoint: 'http://rkalv827881.kau.roche.com:1337/api/v1'
					}
				}
			},
            newdev: {
                options: {
                dest: '<%= yeoman.app %>/scripts/config.js'
                },
                constants: {
                ENV: {
                    name: 'newdev',
                    apiEndpoint: 'http://rkalvndjssan.kau.roche.com:1337/api/v1'
                }
                }
            },
            test: {
                options: {
                dest: '<%= yeoman.app %>/scripts/config.js'
                },
                constants: {
                ENV: {
                    name: 'test',
                    apiEndpoint: 'http://preddashboard-test.roche.com:1337/api/v1'
                    // apiEndpoint: 'http://rbalv533649.kau.roche.com:1337/api/v1'
                }
                }
            },
			production: {
				options: {
					dest: '<%= yeoman.app %>/scripts/config.js'
				},
				constants: {
					ENV: {
						name: 'production',
            apiEndpoint: 'http://preddashboard.roche.com:1337/api/v1'
						// apiEndpoint: 'http://rkalv077026.kau.roche.com:1337/api/v1'
					}
				}
			}
		}
	});


	// grunt serve:newdev
	grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {
		if (target === 'dist') {
			return grunt.task.run(['build', 'connect:dist:keepalive']);
		}

        if (!target) {
        target = 'local';
        }

		grunt.task.run([
			'clean:server',
			'ngconstant:' + target,
			'wiredep',
			'concurrent:server',
			'autoprefixer',
			'connect:livereload',
			'watch'
		]);
	});

	grunt.registerTask('server', 'DEPRECATED TASK. Use the "serve" task instead', function (target) {
		grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
		grunt.task.run(['serve:' + target]);
	});

	grunt.registerTask('test', [
		'clean:server',
		'concurrent:test',
		'autoprefixer',
		'connect:test',
		'karma'
	]);


	// grunt build --force [--target=production]
	grunt.registerTask('build', 'Building the output package...', function () {
		var target = grunt.option('target') || 'development';

		console.info('Target: %s', target);

		grunt.task.run([
			'clean:dist',
			'ngconstant:' + target,
			'wiredep',
			'useminPrepare',
			'concurrent:dist',
			'autoprefixer',
			'concat',
			'ngAnnotate',
			'copy:dist',
			//'cdnify',
			'cssmin',
			'uglify',
			'filerev',
			'usemin',
			'htmlmin'
		]);
	});

	/*grunt.registerTask('build', [
		'clean:dist',
		'ngconstant:development',
		'wiredep',
		'useminPrepare',
		'concurrent:dist',
		'autoprefixer',
		'concat',
		'ngAnnotate',
		'copy:dist',
		'cdnify',
		'cssmin',
		'uglify',
		'filerev',
		'usemin',
		'htmlmin'
	]);*/

	grunt.registerTask('default', [
		'newer:jshint',
		'test',
		'build'
	]);
};
