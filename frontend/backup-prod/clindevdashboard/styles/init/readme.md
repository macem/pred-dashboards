## Development
The following conventions and approaches are used for creating styles:
- **BEM** (Block Element Modifier) naming convention regarding CSS markup: https://bem.info/method/definitions/

- **OOCSS** (Object Oriented CSS) approach:
https://github.com/stubbornella/oocss/wiki

- **CSS lint tool** for better code regarding different rules:
https://github.com/CSSLint/csslint/wiki/Rules

## Compatibility
Created styles are fully compatible with the following browsers:
- IE 10+
- Chrome 31+
- Firefox 32+
- Safari 7+
- Safari iOS 7.1+
