#!/bin/bash
#this file is used for automatic executiopn via the init script when the server is rebooted
cd /data64/sis-sd/node/rkalv077026.kau.roche.com/predportfoliodashboard/resources
module use /apps64/etc/modulefiles/
module load node-v0.12.6
echo "Starting pRED Portfolio Dashboard [PROD]..."
./node_modules/forever/bin/forever -o ../logs/pPD_out.log -e ../logs/pPD_err.log start app.js --prod