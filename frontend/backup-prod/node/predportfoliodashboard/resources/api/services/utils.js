var http = require('http');
var q = require('q');

module.exports = {
	message: {
		error: {
			noRecord: 'Couldn\'t find the record with primary key = ',
			noAccess: 'You don\'t have permission to access this API method. Please contact with your pRed Portfolio Dashboard Administrator.',
			updateNotPossible: 'The update was not possible.',
			forbidden: 'Forbidden',
			timeout: 'Timeout'
		}
	},
	printMessage: function(code, message) {
		code = code || 200;
		message = message || null;

		return {
			code: code,
			message: message
		};
	},
	printErrorMessage: function(code, message) {
		return {
			code: code,
			message: message
		};
	},
	printExtendedErrorMessage: function(code, message, details) {
		return {
			code: code,
			message: message,
			details: details
		};
	},
	printUpdatedMessage: function(updatedObject) {
		return {
			code: 0,
			message: 'The record has been updated.',
			updated: updatedObject
		};
	},
	printUnauthorizedMessage: function() {
		return {
			code: sails.services.rescodes.clientError.forbidden,
			message: 'You don\'t have permission to access this API method. Please contact with your pRed Portfolio Dashboard Administrator.'
		};
	},
	allowedMethod: function(req, res, methods) {
		methods = methods || [];

		if(methods.indexOf(req.method) < 0) {
			return false;
		} else {
			return true;
		}
	},
	objectToJSON: function(object) {
		object = object || {};
		return JSON.parse(JSON.stringify(object))
	},
	groupsToIds: function(callback, req) {

		AccessGroups.find()
				.then(function(found) {

					var validGroups = [];
          var groupsCounter = 0;

          found.forEach(function (group) { // for every access group

            var ADgroups = group.aciveDirectoryId.split(','); // get ad-groups

              sails.services.utils.checkAccessGroupGRP(req.session.userData.username, ADgroups)
                .then(function (isAccessGroupMember) {

                  if (parseInt(isAccessGroupMember) == 1) {
                    validGroups.push(group.id);
                  }
                  groupsCounter++;

                  if (groupsCounter == found.length) {
                    callback(validGroups);
                  }

                });

          });

				});
	},


  /**
   * @param user (string) - user login
   * @param ADgroups (string) - '|' separated AD groups
   * @returns {*|promise}
     */
  checkAccessGroupGRP: function(user, ADgroups) {
    var deferred = q.defer();

    ADgroups = 'AD:' + ADgroups.map(function (groupName) {
        return groupName.trim();
      }).join('|AD:');

    var options = {
      host: 'predilogin.roche.com',
      connection: 'Close',
      port: 80,
      rejectUnauthorized: false,
      headers: {
        'Content-Type': 'application/json'
      },
      path: encodeURI('/GrpmanServlet/isMember?group=' + ADgroups + '&userid=' + user),
      method: 'GET'
    };
    sails.log.warn('AD Groups query OBJ: ', options);

    // FOR TESTS ONLY
    // var content = 'http://' + options.host + options.path;
    // var fs = require('fs');
    // fs.appendFile('C:/projects/access_groups.txt', content + "\n\n", function (err) {
    //  console.log(err);
    // });

    var output = '';

    var req = http.request(options, function(res) {

      res.setEncoding('utf8');

      res.on('data', function (chunk) {
        output += chunk;
      });

      res.on('end', function() {
        deferred.resolve(output);
      });

      res.on('error', function (err) {
        sails.log.warn('Error while getting user groups assignment, err: ' + JSON.stringify(err));
      });
      res.on('timeout', function (err) {
        sails.log.warn('Timeout while getting user groups assignment, err: ' + JSON.stringify(err));
      });

    });

    req.setTimeout(10000);

    req.on('error', function(e) {
      deferred.reject(e.message);
    });

    req.on('timeout', function(err) {
      req.log('>>>>>>>>>>>>>>>>> TIMEOUT: ' +  + JSON.stringify(err));
      req.abort();
      deferred.reject('Timeout');
    });

    //req.write(getData);
    req.end();

    return deferred.promise;
  },




	/*groupsToIds: function(groups, callback, req) {
		AccessGroups.find()
				.then(function(found) {
					console.log('Groups: available groups: ');

					var g = _.uniq(_.flatten(found.map(function(group) {
						return group.aciveDirectoryId.split(',');
					})));

					g.forEach(function(gname) {
						sails.services.utils.checkAccessGroup(req.session.userData.username, gname).then(function(v) {
							console.log(v);
						});
					});

					var registeredGroups = 	found.map(function(record) {
						if(groups.some(function(value) { return record.aciveDirectoryId.split(',').indexOf(value) >= 0;})) {
							return record.id;
						} else {
							return;
						}
					})
							.filter(function(value) {
								return value !== undefined;
							});

					callback(registeredGroups);
				});
	},*/

  // TODO: old version backup:
	checkAccessGroup: function(user, groupId) {
		var deferred = q.defer();

		var getData = JSON.stringify({
			'msg' : 'Hello World!'
		});

		var options = {
			host: 'predilogin.roche.com',
			connection: 'Close',
			port: 80,
			rejectUnauthorized: false,
			headers: {
				'Content-Type': 'application/json'
			},
			path: '/rds/IsAdMember?group=' + groupId + '&userid=' + user + '&recursive=1',
			method: 'GET'
		};

		var output = '';

		var req = http.request(options, function(res) {
			res.setEncoding('utf8');
			res.on('data', function (chunk) {
				output += chunk;
			});
			res.on('end', function() {
				deferred.resolve(output);
			})
		});

		req.setTimeout(10000);

		req.on('error', function(e) {
			deferred.reject(e.message);
		});

		req.on('timeout', function(err) {
			req.log('>>>>>>>>>>>>>>>>> TIMEOUT: ' +  + JSON.stringify(err));
			req.abort();
			deferred.reject('Timeout');
		});

		//req.write(getData);
		req.end();

		return deferred.promise;
	},
	isMobile: function(req) {
		if(!req) {
			return false;
		}

		var userAgent = req.header('user-agent');

		return /mobile/i.test(userAgent);
	},
	getBaseName: function(path) {
		var base = path.substring(path.lastIndexOf('/') + 1);
		if(base.lastIndexOf('.') !== -1) {
			base = base.substring(0, base.lastIndexOf('.'));
		}
		return base;
	}
};
