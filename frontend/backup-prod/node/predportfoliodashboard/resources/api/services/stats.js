var moment = require('moment');
module.exports = {
	general: {
		updateRecord: function(record, req) {
			var rec = record;
			if(record.hasOwnProperty('isActive')) {
				ReportStats.findOrCreate({
					reportId: rec.id
				}, { reportId: rec.id, timeline: {} })
						.exec(
						function(err, record){

							if(err) {
								return res.negotiate(err);
							}

							var currentMonth = moment().format('MM-YYYY');

							if(!record.hasOwnProperty('timeline')) {
								record.timeline = {};
							}

							if(!record.timeline.hasOwnProperty(currentMonth)) {
								record.timeline[currentMonth] = {};
								record.timeline[currentMonth].totalViews = 0;
								record.timeline[currentMonth].mobileViews = 0;
								record.timeline[currentMonth].users = {};
							}

							if(!record.timeline[currentMonth].users.hasOwnProperty(req.session.userData.username)) {
								record.timeline[currentMonth].users[req.session.userData.username] = 0;
							}

							record.timeline[currentMonth].totalViews += 1;
							record.timeline[currentMonth].users[req.session.userData.username] += 1;

							if(sails.services.utils.isMobile(req)) {
								record.timeline[currentMonth].mobileViews += 1;
							}


							record.save();

						});
			}
		}
	},
	reports: {
		increase: function(collection, req) {
			if(!collection instanceof Array) {
				throw new TypeError('Given collection need to be an array type, not %s!', typeof collection);
			}

			Items.find({
				id: collection
			}).exec(
				function(err, collection){

					if(err) {
						throw new Error(err);
					}

					collection.forEach(function(singleRecord) {
						sails.services.stats.general.updateRecord(singleRecord, req);
					});
				});
		}
	},
	map: {
		categories: function(source) {
			if(!source instanceof Array || source.length === 0 || !source[0].hasOwnProperty('items')) {
				return [];
			}

			var items =  _.pluck(source, 'items');
			items = _.uniq(items);

			var tmpArray = [];
			items.forEach(function(id) {
				tmpArray.push(_.pluck(id, 'id'));
			});

			return _.flatten(tmpArray);
		},
		mashups: function(source) {
			if(!source.hasOwnProperty('contents')) {
				return [];
			}

			var contents = source.contents;

			var items =  _.pluck(contents, 'id');
			items = _.uniq(items);

			return items;
		}
	}
};