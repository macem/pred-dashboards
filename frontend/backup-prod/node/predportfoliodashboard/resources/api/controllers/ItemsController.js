/**
 * ItemsController
 *
 * @description :: Server-side logic for managing items
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var path = require('path');
module.exports = {

	/**
	 * @route /{id}
	 * @name
	 *
	 * @method delete
	 * @description Deletes the given record.
	 * @parameter id:string:required:path
	 */

	/**
	 * @route /{id}
	 * @name
	 *
	 * @method get
	 * @description Get report by ID
	 * @parameter id:string:required:path
	 */
	findOne: function(req, res) {
		/**
		 * Check if 'id' parameter
		 * is in string format.
		 */
		req.validate({
			id: 'string'
		});

		Items.findOne({
			id: req.param('id')
		})
				.populate('category')
				.populate('docType')
				.populate('group')
				.exec(
				function(err, record){

					if(err) {
						return res.negotiate(err);
					}

					if(record) {
						sails.services.stats.general.updateRecord(record, req);
						return res.json(record);
					} else {
						return res.notFound();
					}
				});
	},

	/**
	 * @route /count/{id}
	 * @name count
	 *
	 * @method get
	 * @description Increase report stats and redirects to the external report's resource.
	 * @parameter id:string:required:path
	 */
	count: function(req, res) {
		/**
		 * Check if 'id' parameter
		 * is in string format.
		 */
		req.validate({
			id: 'string'
		});

		Items.findOne({
			id: req.param('id')
		})
				.populate('category')
				.populate('docType')
				.populate('group')
				.exec(
				function(err, record){

					if(err) {
						return res.negotiate(err);
					}

					if(record) {
						sails.services.stats.general.updateRecord(record, req);
						return res.redirect(record.embedURL);
					} else {
						return res.notFound();
					}

				});
	},

	/**
	 * @route /disable/{id}
	 * @name disable
	 *
	 * @method get
	 * @description Disable report's visibility to other users.
	 * @parameter id:string:required:path
	 */
	disable: function(req, res) {

		/**
		 * Check if client's method is allowed in this
		 * method.
		 */
		if(!sails.services.utils.allowedMethod(req, res, ['GET'])) {
			return res.json(
					sails.services.rescodes.clientError.methodNotAllowed,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.methodNotAllowed, req.__('methodNotAllowed'))
			);
		}

		/**
		 * Check if 'id' parameter
		 * is in string format.
		 */
		req.validate({
			id: 'string'
		});

		var criteria = {
			id: req.param('id')
		};


		var updateFields = {
			isActive: false
		};

		Items.update(criteria, updateFields)
				.then(function(updated) {

					/**
					 * If not found, resopnse with
					 * a proper error message.
					 */
					if(updated === undefined) {
						return res.json(
								404,
								sails.services.utils.printErrorMessage(-1, req.__('noRecord') + req.param('id'))
						);
					}

					return res.json(updated);

				})
				.catch(function (err) {
					return res.negotiate({aaa:err});
				});
	},
	/**
	 * @route /enable/{id}
	 * @name enable
	 *
	 * @method get
	 * @description Enable report's visibility to other users.
	 * @parameter id:string:required:path
	 */
	enable: function(req, res) {

		/**
		 * Check if client's method is allowed in this
		 * method.
		 */
		if(!sails.services.utils.allowedMethod(req, res, ['GET'])) {
			return res.json(
					sails.services.rescodes.clientError.methodNotAllowed,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.methodNotAllowed, req.__('methodNotAllowed'))
			);
		}

		/**
		 * Check if 'id' parameter
		 * is in string format.
		 */
		req.validate({
			id: 'string'
		});

		var criteria = {
			id: req.param('id')
		};

		var updateFields = {
			isActive: true
		};

		Items.update(criteria, updateFields)
				.then(function(updated) {

					/**
					 * If not found, resopnse with
					 * a proper error message.
					 */
					if(updated === undefined) {
						return res.json(
								404,
								sails.services.utils.printErrorMessage(-1, req.__('noRecord') + req.param('id'))
						);
					}

					return res.json(updated);

				})
				.catch(function (err) {
					return res.negotiate(err);
				});
	},

	/**
	 * @route /cover/{id}
	 * @name cover
	 *
	 * @method get
	 * @description Returns cover photo (binary data) for the giver report.
	 * @parameter id:string:required:path|Report ID
	 */
	cover: function(req, res) {
		/**
		 * Check if 'id' parameter
		 * is in string format.
		 */
		req.validate({
			id: 'string'
		});

		/**
		 * Find item by givern 'id'
		 * parameter.
		 */
		Items.findOne(req.param('id')).exec(function (err, item){
			/**
			 * In case of any error
			 * while querying the database,
			 * send an error message to the
			 * client.
			 */
			if(err) {
				return res.negotiate(err);
			}

			/**
			 * If item not found.
			 */
			if(!item) {
				return res.notFound();
			}

			/**
			 * Check if our item has
			 * a cover photo.
			 *
			 * Normally, it shouldn't happen
			 * but we will check - just in case...
			 */
			if(!item.coverFileDescriptor) {
				return res.notFound();
			}

			/**
			 * We need the Skipper Disk
			 * to make some operations on
			 * 'uploads' file system.
			 *
			 * You can find more information
			 * about Skipper Disk here:
			 * https://github.com/balderdashy/skipper-disk
			 * @type {exports}
			 */
			var SkipperDisk = require('skipper-disk');
			var fileAdapter = SkipperDisk();

			/**
			 * Stream the file down.
 			 */
			fileAdapter.read('../cover-storage/' + path.basename(item.coverFileDescriptor))
				.on('error', function (err){
					return res.serverError(err);
				})
				.pipe(res);
		});
	},
	/**
	 * CommentController.create()
	 */
	/*create: function (req, res) {
		return res.json({
			todo: 'Not implemented yet!'
		});
	},*/

	/**
	 * @route /selectFromCategory/{id}
	 * @name selectFromCategory
	 *
	 * @method get
	 * @description Select items from given category ID.
	 * @parameter id:string:required:path|Category ID
	 */
	selectFromCategory: function(req, res) {
		var id = req.param('id');

		Items.find().where({
			category: id
		}).populate('category').exec(function(err, item) {

			if(err) {
				res.json({
					error: err
				});
			}

			if(item === undefined) {
				res.notFound();
			}
			else {
				res.json(item);
			}

		});
	},

	/**
	 * @route /update/{id}
	 * @name update
	 *
	 * @method put
	 * @description Update report.
	 * @parameter id:string:required:path|Report ID
	 * @parameter title:string:required:formData|Title
	 * @parameter descriptionShort:string:required:formData|Description
	 * @parameter embedURL:string:required:formData|Report URL
	 * @parameter embedURLMobile:string:notrequired:formData|Report URL for mobile devices
	 * @parameter docType:string:required:formData|DocType ID
	 * @parameter category:string:required:formData|Category ID
	 * @parameter group:string:required:formData|Group ID
	 * @parameter refreshInformation:string:notrequired:formData|Refresh information
	 * @parameter cover:file:required:formData|Image file to set as a cover image.
	 */
	update: function(req, res) {

		/**
		 * Check if client's method is allowed in this
		 * method.
		 */
		if(!sails.services.utils.allowedMethod(req, res, ['PUT'])) {
			return res.json(
					sails.services.rescodes.clientError.methodNotAllowed,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.methodNotAllowed, req.__('methodNotAllowed'))
			);
		}

		/**
		 * Check if 'id' parameter
		 * is in string format.
		 */
		req.validate({
			id: 'string'
		});

		var criteria = {
			id: req.param('id')
		};


		/**
		 * Upload the given image.
		 */
		req.file('cover').upload({
			maxBytes: sails.config.maxUploadedFileSize || 10000000,
			dirname: '../../../cover-storage'
		},function whenDone(err, uploadedFiles) {
			/**
			 * Check if file was
			 * properly uploaded.
			 */
			if(err) {
				return res.json(sails.services.rescodes.serverError.internalError, sails.services.utils.printErrorMessage(sails.services.rescodes.serverError.internalError, err));
			}


			/**
			 * Create the object to be sent
			 * to the API.
			 * @type {{title: (*|exports.attributes.title|title|where.name.like.title|Q.title|jasmineRequire.HtmlReporter.HtmlReporter.specDone.title), descriptionShort: (*|exports.attributes.descriptionShort|descriptionShort), embedURL: (*|exports.attributes.embedURL|embedURL), docType: (*|exports.attributes.docType|docType)}}
			 */
			var updateTo = {
				title: req.body.title,
				descriptionShort: req.body.descriptionShort,
				embedURL: req.body.embedURL,
				embedURLMobile: req.body.embedURLMobile,
				docType: req.body.docType,
				category: req.body.category,
				addedBy: req.session.userData.id,
				group: req.body.group,
				refreshInformation: req.body.refreshInformation
			};

			if(uploadedFiles.length > 0) {
				updateTo.coverFileDescriptor = path.basename(uploadedFiles[0].fd);


				/**
				 * Process given image to be
				 * much smaller.
				 */


				var imageMagick;

				/**
				 * There is a serious problem with ImageMagick
				 * on Windows. This is the reason, why we shouldn't
				 * use this program on this OS.
				 *
				 * Istead of this, you should consider
				 * http://www.graphicsmagick.org/
				 */
				if(sails.config.useGraphicsMagick) {
					console.log('ImageMagick: false');
					imageMagick = require('gm');
				} else {
					console.log('ImageMagick: true');
					imageMagick = require('gm').subClass({ imageMagick: true });
				}

				imageMagick(uploadedFiles[0].fd)
						.resize(
						sails.config.thumbnails.maxSize.width,
						sails.config.thumbnails.maxSize.height
				)
					.write(uploadedFiles[0].fd, function (err) {
						if (!err) {
							/**
							 * If everything is OK.,
							 * update the proper record.
							 */
							Items.update(criteria, updateTo)
									.then(function(updated) {

										/**
										 * If not found, resopnse with
										 * a proper error message.
										 */
										if(updated === undefined) {
											return res.json(
													404,
													sails.services.utils.printErrorMessage(-1, req.__('noRecord') + req.param('id'))
											);
										}

										return res.json(updated);

									})
									.catch(function (err) {
										return res.negotiate(err);
									});
						} else {
							console.log('ImageMagick??');
							return res.json(sails.services.rescodes.serverError.internalError, sails.services.utils.printErrorMessage(sails.services.rescodes.serverError.internalError, err));
						}
					});
			} else {
				/**
				 * If everything is OK.,
				 * update the proper record.
				 */
				Items.update(criteria, updateTo)
						.then(function(updated) {

							/**
							 * If not found, resopnse with
							 * a proper error message.
							 */
							if(updated === undefined) {
								return res.json(
										404,
										sails.services.utils.printErrorMessage(-1, req.__('noRecord') + req.param('id'))
								);
							}

							return res.json(updated);

						})
						.catch(function (err) {
							return res.negotiate(err);
						});
			}

		});

	},
	/**
	 * @route /create
	 * @name create
	 *
	 * @method post
	 * @description Create new report.
	 * @parameter title:string:required:formData|Title
	 * @parameter descriptionShort:string:required:formData|Description
	 * @parameter embedURL:string:required:formData|Report URL
	 * @parameter embedURLMobile:string:notrequired:formData|Report URL for mobile devices
	 * @parameter docType:string:required:formData|DocType ID
	 * @parameter category:string:required:formData|Category ID
	 * @parameter group:string:required:formData|Group ID
	 * @parameter refreshInformation:string:notrequired:formData|Refresh information
	 * @parameter cover:file:required:formData|Image file to set as a cover image.
	 */
	create: function(req, res) {
		/**
		 * Check if client's method is allowed in this
		 * method.
		 */
		if(!sails.services.utils.allowedMethod(req, res, ['POST'])) {
			return res.json(
					sails.services.rescodes.clientError.methodNotAllowed,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.methodNotAllowed, req.__('methodNotAllowed'))
			);
		}


		/**
		 * Upload the given image.
		 */
		req.file('cover').upload({
			maxBytes: sails.config.maxUploadedFileSize || 10000000,
			dirname: '../../../cover-storage'
		},function whenDone(err, uploadedFiles) {
			/**
			 * Check if file was
			 * properly uploaded.
			 */
			if(err) {
				return res.json(sails.services.rescodes.serverError.internalError, sails.services.utils.printErrorMessage(sails.services.rescodes.serverError.internalError, err));
			}

			/**
			 * If no files were uploaded, respond with an error.
			 */
			if (uploadedFiles.length === 0){
				return res.json(sails.services.rescodes.clientError.badRequest, sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.badRequest, req.__('noFileWasUploaded')));
			}

			/**
			 * Create the object to be sent
			 * to the API.
			 * @type {{title: (*|exports.attributes.title|title|where.name.like.title|Q.title|jasmineRequire.HtmlReporter.HtmlReporter.specDone.title), descriptionShort: (*|exports.attributes.descriptionShort|descriptionShort), embedURL: (*|exports.attributes.embedURL|embedURL), docType: (*|exports.attributes.docType|docType)}}
			 */
			var data = {
				title: req.body.title,
				descriptionShort: req.body.descriptionShort,
				embedURL: req.body.embedURL,
				embedURLMobile: req.body.embedURLMobile,
				docType: req.body.docType,
				category: req.body.category,
				coverFileDescriptor: path.basename(uploadedFiles[0].fd),
				addedBy: req.session.userData.id,
				group: req.body.group,
				refreshInformation: req.body.refreshInformation
			};

			/**
			 * Process given image to be
			 * much smaller.
			 */


			var imageMagick;

			/**
			 * There is a serious problem with ImageMagick
			 * on Windows. This is the reason, why we shouldn't
			 * use this program on this OS.
			 *
			 * Istead of this, you should consider
			 * http://www.graphicsmagick.org/
			 */
			if(sails.config.useGraphicsMagick) {
				console.log('ImageMagick: false');
				imageMagick = require('gm');
			} else {
				console.log('ImageMagick: true');
				imageMagick = require('gm').subClass({ imageMagick: true });
			}

			imageMagick(uploadedFiles[0].fd)
					.resize(
					sails.config.thumbnails.maxSize.width,
					sails.config.thumbnails.maxSize.height
			)
				.write(uploadedFiles[0].fd, function (err) {
					if (!err) {
						/**
						 * If everything is OK.,
						 * create new record.
						 */
						Items.create(data)
								.exec(function createCB(err, created) {
									/**
									 * If everything is OK.,
									 * send a JSON response
									 * with the added record.
									 */
									if (!err) {
										return res.json(created);
									} else {
										/**
										 * Otherwise, response
										 * with an error message.
										 */
										return res.json(sails.services.rescodes.serverError.internalError, sails.services.utils.printErrorMessage(sails.services.rescodes.serverError.internalError, err));
									}
								});
					} else {
						console.log('ImageMagick??');
						return res.json(sails.services.rescodes.serverError.internalError, sails.services.utils.printErrorMessage(sails.services.rescodes.serverError.internalError, err));
					}
				});
		});

	}
};

