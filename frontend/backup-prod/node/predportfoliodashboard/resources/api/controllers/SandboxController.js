/**
 * ValidUsersController
 *
 * @description :: Server-side logic for managing Validusers
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

/**
 * @route abc
 */

module.exports = {
	access: function(req, res) {
		var requests = [];
		var access = {};

		if(!req.session.userData) {
			return res.json({error:'Access restricted.'})
		}

		AccessGroups.find().then(function(found) {
			var toCheck = _.pluck(found, 'aciveDirectoryId');
			var outputCheck = [];

			toCheck.map(function(val) {
				var contain = val.match(',');
				if(contain) {
					var single = val.split(',');
					single.forEach(function(v) {
						outputCheck.push(v);
					});
				} else {
					outputCheck.push(val);
				}
			});

			outputCheck.forEach(function(groupId) {
				requests.push(
						sails.services.utils.checkAccessGroup(req.session.userData.username, groupId)
								.then(function(data) {
									access[groupId] = data === '1' ? true : false;
								})
				);
			});

			sails.q.all(requests).then(function() {
				return res.json(access);
			}, function(err) {
				return res.negotiate({error: err});
			});
		})
		.catch(function(err) {
			res.negotiate(err);
		});

		//return res.json({ok:true});
	},
	// @route('buba/{id}', 'get')
	buba: function() {

	}
};

