/**
 * MashupsController
 *
 * @description :: Server-side logic for managing mashups
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	_config: {
		populate: true
	},

	/**
	 * @route /{id}
	 * @name
	 *
	 * @method delete
	 * @description Deletes the given record.
	 * @parameter id:string:required:path
	 */

	/**
	 * @route /
	 * @name
	 *
	 * @method get
	 * @description Get current user mashups.
	 */
	index: function(req, res) {
		/**
		 * Check if client's method is allowed in this
		 * method.
		 */
		if(!sails.services.utils.allowedMethod(req, res, ['GET'])) {
			return res.json(
					sails.services.rescodes.clientError.methodNotAllowed,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.methodNotAllowed, req.__('methodNotAllowed'))
			);
		}

		if(req.session.userData) {

			var popCriteria = {};

			if (!req.session.userData.isSuperUser) {
				popCriteria.group =  req.session.userData.mappedRoles.length > 0 ? req.session.userData.mappedRoles : ['xyzXYZJhndow'];
			}

			Mashups.find({
				userId: req.session.userData.id
			})
				.populate('contents', popCriteria)
				.exec(function findOneCB(err, found){
					if(err) {
						return res.json([]);
					}

					return res.json(found);
				});
 		} else {
			return res.json([]);
		}
	},
	/**
	 * @route /{id}
	 * @name
	 *
	 * @method get
	 * @description Get reports inside the given mashup.
	 * @parameter id:string:required:path|Mashup ID
	 */
	findOne: function(req, res) {

		var _ = require('underscore');
		var async = require('async');

		var criteria = {
			id: req.param('id')
		};

		async.auto({
			mashup: function(cb) {
				var popCriteria = {};

				if (!req.session.userData.isSuperUser) {
					popCriteria.group =  req.session.userData.mappedRoles.length > 0 ? req.session.userData.mappedRoles : ['xyzXYZJhndow'];
				}

				Mashups.findOne(criteria).populate('contents', popCriteria).exec(cb);
			},

			docTypes: ['mashup', function(cb, async_data) {
				DocTypes.find().exec(cb);
			}],

			categories: ['docTypes', function(cb, async_data) {
				Categories.find().exec(cb);
			}]

		},
		function allDone(err, async_data) {

			if(!async_data.mashup) {
				return res.json([]);
			}

			_.map(async_data.mashup.contents, function (record) {
				var docType = _.where(async_data.docTypes, { id: record.docType });
				record.docType = docType.pop();
				if(async_data.mashup.hasOwnProperty('ordering')) {
					record.position = async_data.mashup.ordering.indexOf(record.id);
				}
			});

			_.map(async_data.mashup.contents, function (record) {
				var category = _.where(async_data.categories, { id: record.category });
				record.category = category.pop();
				try {
					delete record.category.items;
					delete record.category.position;
				} catch (error) {
                    console.error(error);
                }
			});

			if(async_data.mashup.hasOwnProperty('ordering')) {
				try {
					delete async_data.mashup.ordering;
				} catch (error) {
                    console.error(error);
                }
			}

			var mappedResults = sails.services.stats.map.mashups(async_data.mashup, req);
			sails.services.stats.reports.increase(mappedResults, req);

			res.json(async_data.mashup);
		});
	},
	/**
	 * @route /add
	 * @name add
	 *
	 * @method post
	 * @description Adds new mashup for the current user.
	 * @parameter name:string:required:formData|Mashup Name
	 * @parameter description:string:required:formData|Description
	 * @parameter contents:array:required:formData|Collection of reports ID
	 * @parameter ordering:array:required:formData|Ordering (collection of reports ID)
	 */
	add: function(req, res) {
		/**
		 * Check if client's method is allowed in this
		 * method.
		 */
		if(!sails.services.utils.allowedMethod(req, res, ['POST'])) {
			return res.json(
					sails.services.rescodes.clientError.methodNotAllowed,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.methodNotAllowed, req.__('methodNotAllowed'))
			);
		}

		/**
		 * Create new record
		 */

		var record = {
			userId: req.session.userData.id,
			name: req.param('name'),
			description: req.param('description'),
			contents: req.param('contents'),
			ordering: req.param('contents')
		};

		if(!Array.isArray(record.contents)) {
			return req.json(
					sails.services.rescodes.clientError.badRequest,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.badRequest, req.__('badRequest'))
			)
		}

		Mashups.create(record)
				.exec(function createCB(err, created){

					if(err) {
						return res.negotiate(err);
					}

					/**
					 * At the end, we can send
					 * our new record to client.
					 */
					return res.json(created);
				});
	},

	/**
	 * @route /{id}
	 * @name
	 *
	 * @method put
	 * @description Updates given mashup.
	 * @parameter id:string:required:path|Mashup ID
	 * @parameter name:string:required:formData|Mashup Name
	 * @parameter description:string:required:formData|Description
	 * @parameter contents:array:required:formData|Collection of reports ID
	 * @parameter ordering:array:required:formData|Ordering (collection of reports ID)
	 */
	update: function (req, res) {


		var criteria = {
			id: req.param('id'),
			userId: req.session.userData.id
		};

		var order = req.body.contents || [];

		Mashups.update(criteria,{
			name: req.body.name,
			description: req.body.description,
			contents: order,
			ordering: order
		})
				.exec(function afterwards(err, updated){

					if(err) {
						return res.negotiate(err);
					}

					//updated.contents = updated.contents.toJSON();

					/**
					 * At the end, we can send
					 * our new record to client.
					 */
					return res.json(updated);
				});

	},

	/**
	 * @route /{id}
	 * @name
	 *
	 * @method delete
	 * @description Deletes the given mashup.
	 * @parameter id:string:required:path|Mashup ID
	 */
	destroy: function(req, res) {
		/**
		 * Check if 'id' parameter
		 * is in string format.
		 */
		req.validate({
			id: 'string'
		});

		var criteria = {
			userId: req.session.userData.id,
			id: req.param('id')
		};

		Mashups.destroy(criteria)
			.exec(function deleteCB(err){
					if(err) {
						return res.json(
								sails.services.rescodes.clientError.notFound,
								sails.services.utils.printMessage(sails.services.rescodes.clientError.notFound, req.__('notFound'))
						);
					}

					return res.json({
						destroyed:true
					});
			});
	}
};

