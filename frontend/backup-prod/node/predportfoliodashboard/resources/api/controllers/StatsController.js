/**
 * StatsController
 *
 * @description :: Server-side logic for managing Stats
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	index: function(req, res) {
		var _self = this;

		Categories.find().exec(function(err, categories) {
			var _categories = _.indexBy(categories, 'id');
			ReportStats.find().exec(function(err, records) {
				if(err) {
					return res.negotiate(err);
				}

				var stats = _.indexBy(records, 'reportId');

				Items.find().populate('category').populate('docType').exec(function(err, items) {
					var output = items.map(function(item) {
						if(stats.hasOwnProperty(item.id)) {
							var _stats = stats[item.id];
							try {
								delete _stats.reportId;
								delete _stats.createdAt;
								delete _stats.updatedAt;
								delete _stats.id;
							} catch(e) {
								console.log('Could not delete unnecessary data.');
							}

							item.stats = _stats.timeline || {};
						}

						var additional;

						try {
							additional = {
								category: item.category.toJSON().name || null,
								docType: item.docType.toJSON().title || null
							};
						} catch(e) {
							additional = {
								category: null,
								docType: null
							};
						}

						return {
							stats: item.stats || {},
							title: item.title,
							id: item.id,
							category: additional.category,
							docType: additional.docType
						};
					});
					_self.mashups(function(mashups) {
						return res.json({
							reports: output,
							mashups: mashups.mashups
						});
					});
				});

			});
		});
	},
	mashups: function(callback) {
		Mashups.find().exec(function(err, mashups) {
			var uniqueList = _.uniq(mashups, function(item, key, a) {
				return item.userId;
			});

			ValidUsers.find().exec(function(err, usrs) {
				var list = _.indexBy(usrs, 'id');

				var users = {};
				mashups.forEach(function(mashup) {

					var username;

					if(list.hasOwnProperty(mashup.userId)) {
						username = list[mashup.userId].username;
					} else {
						username = 'unknown';
					}

					if(!users.hasOwnProperty(username)) {
						users[username] = 0;
					}

					users[username] += 1;
				});

				callback({
					mashups: {
						totalUsers: uniqueList.length,
						details: users
					}
				});
			});
		});
	}/*,
	general: function(req, res) {
		ReportStats.find().sum('totalViews', 'mobileViews').exec(function(err, found) {
			if(err) {
				return res.negotiate(err);
			}

			Mashups.find().exec(function(err, mashups) {
				if(err) {
					return res.negotiate(err);
				}

				var uniqueList = _.uniq(mashups, function(item, key, a) {
					return item.userId;
				});

				ValidUsers.find().exec(function(err, usrs) {
					if(err) {
						return res.negotiate(err);
					}

					var list = _.indexBy(usrs, 'id');

					var users = {};
					mashups.forEach(function(mashup) {

						var username;

						if(list.hasOwnProperty(mashup.userId)) {
							username = list[mashup.userId].username;
						} else {
							username = 'unknown';
						}

						if(!users.hasOwnProperty(username)) {
							users[username] = 0;
						}

						users[username] += 1;
					});

					return res.json({
						reports: found.pop(),
						mashups: {
							totalUsers: uniqueList.length,
							details: users
						}
					});
				});
			});
		});
	}*/
};

