/**
 * Items.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
	types: {
		isUrl: function(url){
			url = url || '';
			return /^(https?|sapbi):\/\/.+/.test(url);
		}
	},
	attributes: {
		title: {
			type: 'string',
			defaultsTo: 'Untitled',
			required: true,
			minLength: 2
		},
		coverFileDescriptor: {
			type: 'string',
			required: false,
			minLength: 2
		},
		descriptionShort: {
			type: 'string',
			required: true,
			minLength: 2
		},
		embedURL: {
			type: 'string',
			required: true,
			minLength: 7,
			maxLength: 500,
			isUrl: true
		},
		embedURLMobile: {
			type: 'string',
			required: false,
			minLength: 7,
			maxLength: 500
		},
		refreshInformation: {
			type: 'string',
			required: false
		},
		addedBy: {
			model: 'ValidUsers'
		},
		docType: {
			model: 'DocTypes'
		},
		category: {
			model: 'Categories',
			via: 'items'
		},
		isActive: {
			type: 'boolean',
			defaultsTo: true
		},
		group: {
			model: 'AccessGroups'
		}
	}
};

