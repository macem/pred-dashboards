/**
* Mashups.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
	  userId: {
		  type: 'string',
		  required: true,
		  minLength: 2
	  },
	  name: {
		  type: 'string',
		  required: true,
		  minLength: 1,
		  maxLength: 255
	  },
	  description: {
		  type: 'string',
		  required: false
	  },
	  contents: {
		  collection: 'Items',
		  protected: false
	  },
	  ordering: {
			type: 'array',
		  	required: false
	  },
	  getContents: function() {
		  return this.contents;
	  }
  }
};

