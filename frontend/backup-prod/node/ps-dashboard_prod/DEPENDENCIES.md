Necessary dependencies that should be installed on server:

- ImageMagick
  $ sudo yum install ImageMagick-c++ ImageMagick-c++-devel
  
- NodeJS ImageMagick wrapper
  https://github.com/aheckmann/gm
  
- Python >= 2.7