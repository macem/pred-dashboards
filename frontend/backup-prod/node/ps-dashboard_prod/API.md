***** This is my 'meat' for the later swagger UI. *****

POST ######### /Signin

You can perform this operation without any special restrictions.

Body:
{
      "username":"USER_DOMAIN_NAME",
      "password": "USER_PASSWORD"
}

Example Response:
{
  "username": "borkowsm",
  "roles": [
    "GLOAEP_webport_Penzberg",
    "EMEA_UserAuthAuto-35",
    "Warsaw-IT_AF_Users-01",
    "GLOSAPSystems_P38_010-14",
    "GLOSAPPortalRoles_P38_010_Z1383_ZZZ_SBC_EYZ_dp_N_COMMONFUNC-69",
    "GLOMADE_All-Contractors-Roche-07",
    "GLOSDP-CM12_SKYPE 6.2 EN",
    "GLOSDP-CM12_ACCELRYS DRAW 4.1 EE SP1 EN",
    "EMEA_EMailEncryptAuto-32",
    "GLOSAPPortalRoles_P38_010_Z0383_ZZZ_HES_XSM_dp_N_LSO_ESSEXT-12",
    "GLOSAPPortalRoles_P38_010_Z0383_YYZ_HES_XSM_dp_N_EMPLOYEEXT-02",
    "GLOMADE_RWA-00",
    "GLOMADE_FP-R-cnt-00",
    "GLOMADE_FP-int-cnt-02",
    "GLOpAppAuth_Pharma_exclGenentechChugai_EMEA2",
    "MobileMailEMEA_GA",
    "RWAORG_ADMD-Warsaw-Contractors",
    "RBAJiraSIS_Business",
    "GLOIRCI_USER_TEST",
    "GLORange_App_MAC_FullAccess",
    "RBAMassMail_EM711",
    "EMEA_SMR_CORE_pri",
    "GLONGSC_Desktop_User",
    "GLORange_App_MDM_Junos_Pulse"
  ],
  "email": "GLOpREDPrtflDshbrd_TechnicalAdmins@msxdl.roche.com",
  "isSuperUser": true,
  "createdAt": "2015-06-30T08:16:46.679Z",
  "updatedAt": "2015-07-02T12:59:21.420Z",
  "firstName": "Marcin",
  "lastName": "Borkowski",
  "id": "5592506ec38f32000b13f661"
}

GET ######### /Signin/currentUser
Returns the current user's username.

Example:
{
  "username": "borkowsm"
}

GET ######### /Signout
You can perform this operation only if you are signed in.

Correct response:
{
  "code": 200,
  "message": "Logged out."
}

'No permission' response:
{
  "code": 403,
  "message": "You don't have permission to access this API method. Please contact with your pRed Portfolio Dashboard Administrator."
}

GET ######### /Categories
Returns categories with items assigned to them.

Example response:
[
  {
    "items": [
      {
        "title": "blue",
        "descriptionShort": "description123",
        "category": "555225013aa27a1c1e275ebd",
        "createdAt": "2015-05-14T11:39:13.591Z",
        "updatedAt": "2015-05-14T11:39:13.591Z",
        "id": "55548961e7e4c65406a79e15"
      },
      {
        "title": "orange",
        "descriptionShort": "description123",
        "category": "555225013aa27a1c1e275ebd",
        "createdAt": "2015-05-14T11:39:20.331Z",
        "updatedAt": "2015-05-14T11:39:20.331Z",
        "id": "55548968e7e4c65406a79e16"
      },
      {
        "title": "yellow",
        "descriptionShort": "description123",
        "category": "555225013aa27a1c1e275ebd",
        "createdAt": "2015-05-14T11:39:23.462Z",
        "updatedAt": "2015-05-14T11:39:23.462Z",
        "id": "5554896be7e4c65406a79e17"
      }
    ],
    "name": "Second Category",
    "position": 101,
    "createdAt": "2015-05-12T16:06:25.271Z",
    "updatedAt": "2015-05-12T16:06:25.271Z",
    "id": "555225013aa27a1c1e275ebd"
  },
  {
    "items": [
      {
        "title": "Warsaw",
        "descriptionShort": "xxx",
        "category": "55548983e7e4c65406a79e1a",
        "createdAt": "2015-05-14T11:40:27.234Z",
        "updatedAt": "2015-05-14T11:40:27.234Z",
        "id": "555489abe7e4c65406a79e1c"
      },
      {
        "title": "Cracow",
        "descriptionShort": "xxx",
        "category": "55548983e7e4c65406a79e1a",
        "createdAt": "2015-05-14T11:40:33.316Z",
        "updatedAt": "2015-05-14T11:40:33.316Z",
        "id": "555489b1e7e4c65406a79e1d"
      },
      {
        "title": "London",
        "descriptionShort": "xxx",
        "category": "55548983e7e4c65406a79e1a",
        "createdAt": "2015-05-14T11:40:37.949Z",
        "updatedAt": "2015-05-14T11:40:37.949Z",
        "id": "555489b5e7e4c65406a79e1e"
      }
    ],
    "name": "First Category",
    "position": 102,
    "createdAt": "2015-05-14T11:39:47.810Z",
    "updatedAt": "2015-05-14T11:39:47.810Z",
    "id": "55548983e7e4c65406a79e1a"
  }
]

GET ######### /Categories/list
Returns categories without assigned items.

Response example:
[
  {
    "name": "Second Category",
    "position": 101,
    "createdAt": "2015-05-12T16:06:25.271Z",
    "updatedAt": "2015-05-12T16:06:25.271Z",
    "id": "555225013aa27a1c1e275ebd"
  },
  {
    "name": "First Category",
    "position": 102,
    "createdAt": "2015-05-14T11:39:47.810Z",
    "updatedAt": "2015-05-14T11:39:47.810Z",
    "id": "55548983e7e4c65406a79e1a"
  },
  {
    "name": "Other Category",
    "position": 103,
    "createdAt": "2015-05-14T11:40:00.172Z",
    "updatedAt": "2015-05-14T11:40:00.172Z",
    "id": "55548990e7e4c65406a79e1b"
  }
]

GET ######### /Items

Will return collection of all items.

Response example:
[
  {
    "docType": {
      "title": "Business Objects",
      "imgFlagId": "2",
      "createdAt": "2015-07-07T11:24:09.883Z",
      "updatedAt": "2015-07-07T11:24:09.883Z",
      "id": "559bb6d9ceee96a04966938e"
    },
    "category": {
      "name": "Portfolio Composition",
      "id": "559bbd8cceee96a049669391"
    },
    "title": "Sed massa neque, egestas",
    "descriptionShort": "Duis non sapien quam. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis dolor tellus, imperdiet id consectetur a, bibendum vel dui.",
    "embedURL": "https://www.google.com/maps/embed?pb=!1m0!3m2!1sen!2spl!4v1436513119199!6m8!1m7!1sOf5WMt7kXXlal20Nc4HPWQ!2m2!1d52.164417!2d21.070174!3f33.97!4f11.159999999999997!5f0.7820865974627469",
    "coverFileDescriptor": "C:\\Users\\borkowsm\\WebstormProjects\\pRedPortfolioDashboard\\backend\\portfolioDashboardAPI\\.tmp\\uploads\\1d3a544a-52a7-4e8a-aae1-0746dc7225a0.png",
    "createdAt": "2015-07-10T06:07:50.927Z",
    "updatedAt": "2015-07-10T06:07:50.927Z",
    "id": "559f6136d036d9142b51613b"
  },
  {
    "docType": {
      "title": "Power Point",
      "imgFlagId": "3",
      "createdAt": "2015-07-07T11:24:28.083Z",
      "updatedAt": "2015-07-07T11:24:28.083Z",
      "id": "559bb6ecceee96a04966938f"
    },
    "category": {
      "name": "Portfolio Composition",
      "id": "559bbd8cceee96a049669391"
    },
    "title": "Curabitur quam ex",
    "descriptionShort": "Aenean nibh risus, ullamcorper non rutrum id, dictum a nisi. Praesent et sagittis nulla, vel tempor metus. Proin eget lacinia ipsum.",
    "embedURL": "https://www.google.com/maps/embed?pb=!1m0!3m2!1sen!2spl!4v1436513171800!6m8!1m7!1s-9YK2-Uwbpoi8CN5NKOOOQ!2m2!1d52.154727!2d21.067334!3f272.19!4f2.019999999999996!5f0.7820865974627469",
    "coverFileDescriptor": "C:\\Users\\borkowsm\\WebstormProjects\\pRedPortfolioDashboard\\backend\\portfolioDashboardAPI\\.tmp\\uploads\\5a30a644-27bc-443f-8537-e8e770e58b48.png",
    "createdAt": "2015-07-10T06:07:14.180Z",
    "updatedAt": "2015-07-10T06:07:14.180Z",
    "id": "559f6112d036d9142b51613a"
  }
]

GET ######### /Items/cover/:id

Will return the cover image for given item id.
You should put the item unique id as a 'id' parameter.
E.g.:

      GET /Items/cover/559ecc63049ee170255a03cb

Response:
[binary data]

POST ######### /Items

Will create a new entry in 'Items'.
Body:
{
      "title": "ITEM TITLE",
      "descriptionShort": "Short description",
      "embedURL": req.body.embedURL,
      docType: req.body.docType,
      category: req.body.category,
      coverFileDescriptor: uploadedFiles[0].fd
}

GET ######### /Favorites

Gets favorites collection for current user.

Example Response:
{
  "contents": [
    {
      "title": "Green Orange Blue",
      "descriptionShort": "Lorem ipsum red black white orange ...",
      "embedURL": "https://www.google.com/maps/embed?pb=!1m0!3m2!1sen!2spl!4v1436512948203!6m8!1m7!1s-9YK2-Uwbpoi8CN5NKOOOQ!2m2!1d52.154727!2d21.067334!3f272.1856870747981!4f2.017126760045514!5f0.7820865974627469",
      "docType": "559bb811ceee96a049669390",
      "coverFileDescriptor": "C:\\Users\\borkowsm\\WebstormProjects\\pRedPortfolioDashboard\\backend\\portfolioDashboardAPI\\.tmp\\uploads\\02d00b2f-0ae3-44f9-b034-e15b0302c17c.JPG",
      "createdAt": "2015-07-09T19:32:51.387Z",
      "updatedAt": "2015-07-09T19:33:27.309Z",
      "category": "559bbd8cceee96a049669391",
      "id": "559ecc63049ee170255a03cb"
    },
    {
      "title": "Lorem ipsum",
      "descriptionShort": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dolor purus, efficitur id pharetra vel, pulvinar eu sapien.",
      "embedURL": "https://www.google.com/maps/embed?pb=!1m0!3m2!1sen!2spl!4v1436513274618!6m8!1m7!1s2PvJjzBvOVPzb9S3jX-3kQ!2m2!1d52.166221!2d21.083083!3f255.1980244387558!4f12.098347001916721!5f0.7820865974627469",
      "docType": "559bb6b7ceee96a04966938d",
      "category": "559bbdb9ceee96a049669392",
      "coverFileDescriptor": "C:\\Users\\borkowsm\\WebstormProjects\\pRedPortfolioDashboard\\backend\\portfolioDashboardAPI\\.tmp\\uploads\\2b1251f9-c4c3-4105-a395-4536497ea5b4.gif",
      "createdAt": "2015-07-10T06:03:35.733Z",
      "updatedAt": "2015-07-10T06:03:35.733Z",
      "id": "559f6037d036d9142b516139"
    }
  ],
  "userId": "5592506ec38f32000b13f661",
  "createdAt": "2015-07-16T13:07:53.020Z",
  "updatedAt": "2015-07-16T13:47:26.996Z",
  "id": "55a7aca90e9fdb9815571af5"
}

PUT ######### /Favorites/:id

Will toggle the given item id on favorites.

Example request:
      PUT /Favorites/559f6037d036d9142b516139
      
Example response:
[
  {
    "userId": "5592506ec38f32000b13f661",
    "createdAt": "2015-07-16T13:07:53.020Z",
    "updatedAt": "2015-07-16T13:51:23.331Z",
    "id": "55a7aca90e9fdb9815571af5"
  }
]