# Installation guidelines

You should install the necessary modules BEFORE deployment process.
On Roche's machines with Red Hat Linux Enterprise Edition please follow these steps:

* Switch into root: sudo /home/adm/bin/suroot
* Go to /tmp
```sh
$ cd /tmp
```
* Download the latest NodeJS source code from https://nodejs.org/download/
* Install Libstdc++ library
```sh
yum install libstdc++-devel
```
* Next:
```sh
$ tar zxvf node-v0.12.5.tar.gz
$ cd zxvf node-v0.12.5
$ ./configure
$ make
$ make install
$ mv /tmp/node-v0.12.5/out/Release /opt/node-v0.12.5
$ ln -s /opt/node-v0.12.5/node /usr/bin/node
```

## How to set permissions to specify folder

* Switch into root (sudo /home/adm/bin/suroot)
```sh
$ chown -R username directory
$ chmod -R u+rX directory
```