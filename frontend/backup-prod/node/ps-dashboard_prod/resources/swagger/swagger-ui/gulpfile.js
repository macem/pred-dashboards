'use strict';

var gulp = require('gulp');
var es = require('event-stream');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var less = require('gulp-less');
var handlebars = require('gulp-handlebars');
var wrap = require('gulp-wrap');
var declare = require('gulp-declare');
var watch = require('gulp-watch');
var connect = require('gulp-connect');
var header = require('gulp-header');
var pkg = require('./package.json');
var order = require('gulp-order');
var jshint = require('gulp-jshint');
var exec = require('child_process').exec;
var banner = ['/**',
	' * <%= pkg.name %> - <%= pkg.description %>',
	' * @version v<%= pkg.version %>',
	' * @link <%= pkg.homepage %>',
	' * @license <%= pkg.license %>',
	' */',
	''].join('\n');

/**
 * Clean ups ./dist folder
 */
gulp.task('clean', function () {
	return gulp
			.src('../../assets/swagger', {read: false})
			.pipe(clean({force: true}))
			.on('error', log);
});

/**
 * Processes Handlebars templates
 */
function templates() {
	return gulp
			.src(['./src/main/template/**/*'])
			.pipe(handlebars())
			.pipe(wrap('Handlebars.template(<%= contents %>)'))
			.pipe(declare({
				namespace: 'Handlebars.templates',
				noRedeclare: true, // Avoid duplicate declarations
			}))
			.on('error', log);
}

/**
 * JShint all *.js files
 */
gulp.task('lint', function () {
	return gulp.src('./src/main/javascript/**/*.js')
			.pipe(jshint())
			.pipe(jshint.reporter('jshint-stylish'));
});

/**
 * Build a distribution
 */
gulp.task('dist', ['clean', 'lint'], function () {

	return es.merge(
			gulp.src([
				'./src/main/javascript/**/*.js',
				'./node_modules/swagger-client/browser/swagger-client.js'
			]),
			templates()
	)
			.pipe(order(['scripts.js', 'templates.js']))
			.pipe(concat('swagger-ui.js'))
			.pipe(wrap('(function(){<%= contents %>}).call(this);'))
			.pipe(header(banner, {pkg: pkg}))
			.pipe(gulp.dest('../../assets/swagger'))
			.pipe(uglify())
			.on('error', log)
			.pipe(rename({extname: '.min.js'}))
			.on('error', log)
			.pipe(gulp.dest('../../assets/swagger'))
			.pipe(connect.reload());
});

/**
 * Processes less files into CSS files
 */
gulp.task('less', ['clean'], function () {

	return gulp
			.src([
				'./src/main/less/screen.less',
				'./src/main/less/print.less',
				'./src/main/less/reset.less',
				'./src/main/less/style.less'
			])
			.pipe(less())
			.on('error', log)
			.pipe(gulp.dest('./src/main/html/css/'))
			.pipe(connect.reload());
});


/**
 * Copy lib and html folders
 */
gulp.task('copy', ['less'], function () {

	setTimeout(function () {
		// copy JavaScript files inside lib folder
		gulp
				.src(['./lib/**/*.{js,map}'])
				.pipe(gulp.dest('../../assets/swagger/lib'))
				.on('error', log);

		// copy `lang` for translations
		gulp
				.src(['./lang/**/*.js'])
				.pipe(gulp.dest('../../assets/swagger/lang'))
				.on('error', log);

		// copy all files inside html folder
		gulp
				.src(['./src/main/html/**/*'])
				.pipe(gulp.dest('../../assets/swagger'))
				.on('error', log);

		// generate the latest swagger.json as input
	}, 1000);

});

/**
 * Watch for changes and recompile
 */
gulp.task('watch', function () {
	return watch(['./src/**/*.{js,less,handlebars}'], function () {
		gulp.start('default');
	});
});

gulp.task('generateinpput', function (cb) {
	console.log('Generating the outpuf JSON, please wait...');
	exec('node generate-input.js', function (err, stdout, stderr) {
		console.log(stdout);
		console.log(stderr);
		cb(err);
	});
});

/**
 * Live reload web server of `dist`
 */
gulp.task('connect', function () {
	connect.server({
		root: 'dist',
		livereload: true
	});
});

function log(error) {
	console.error(error.toString && error.toString());
}


gulp.task('default', ['generateinpput', 'dist', 'copy']);
gulp.task('serve', ['connect', 'watch']);
