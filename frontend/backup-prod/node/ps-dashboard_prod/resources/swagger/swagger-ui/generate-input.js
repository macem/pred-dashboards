/**
 * Example schema: https://github.com/swagger-api/swagger-spec/blob/master/examples/v2.0/json/petstore-simple.json
 */

var fileSystem = require('fs');

var Annotations = function (dir, filters) {
	this.dir = dir || './';
	this.filters = filters || ['js'];
	this.output = {
		"swagger": "2.0",
		"info": {
			"version": "1.0.1",
			"title": "pRED Portfolio Dashboard: API",
			"description": "A sample RESTful API.",
			"termsOfService": "http://swagger.io/terms/",
			"contact": {
				"name": "Admin <GLOpREDPrtflDshbrd_TechnicalAdmins@msxdl.roche.com>"
			}
		},
		"host": "",
		"basePath": "/api/v1",
		"schemes": [
			"http"
		],
/*		"consumes": [
			"application/json"
		],
		"produces": [
			"application/json"
		],*/
		"paths": {}
	};
};

Annotations.prototype.getFiles = function (callback) {
	var _self = this;

	return fileSystem.readdir(_self.dir, function (err, files) {
		if (err) {
			throw new Error(err.message);
		}

		files = files.filter(function (file, index) {
			var parse = file.split('.');
			if (_self.filters.indexOf(parse[1].toLowerCase()) >= 0) {
				return true;
			}
		});

		callback(files);

	});
};

Annotations.prototype.addPath = function (path) {
	var _self = this;

	//console.log(path);

	if (!path instanceof Object || !path.hasOwnProperty('route')) {
		throw new Error('Wrong object.');
	}

	if (!_self.output.paths.hasOwnProperty(path.route)) {
		_self.output.paths[path.route] = {};
	}

	path.parameters = path.parameters || [];

	if (typeof path.parameters === 'string') {
		path.parameters = [path.parameters];
	}

	_self.output.paths[path.route][path.method] = {
		operationId: path.holder,
		description: path.description,
		parameters: path.parameters.map(function (value) {

			var all = value.split('|');
			var param = all[0].split(':')

			return {
				name: param[0] || '?',
				type: param[1] || 'string',
				required: (param[2] && param[2] === 'required') || false,
				in: param[3] || undefined,
				paramType: param[3] || 'formData',
				description: all[1] || undefined
			};
		})
	};
};

Annotations.prototype.getNode = function (source, key) {
	try {
		var pattern = new RegExp('\@' + key + ' .+', 'gm');
		var value = source.match(pattern);


		if (value.length === 1) {
			value = value.pop();
			value = value.substring(value.indexOf(' ') + 1, value.length);

		} else {
			value = value.map(function (v) {
				return v.substring(v.indexOf(' ') + 1, v.length);
			});
		}

		return value;
	} catch (e) {
		return undefined;
	}
};

Annotations.prototype.readAnnotations = function (content, fileName) {
	var _self = this;
	fileName = fileName || '';
	var routePrefix = fileName.replace('Controller.js', '');
	var holder = routePrefix;

	if (routePrefix.length > 0) {
		routePrefix = ['/', routePrefix].join('');
	}

	var annotations = content.match(/(\/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+\/)|(\/\/.*)/igm);
	annotations.forEach(function (annotation) {
		if (/@route/gm.test(annotation) && /@name/gm.test(annotation)) {
			var element = annotation.match(/@.+/gm);

			_self.addPath({
				holder: holder,
				name: _self.getNode(annotation, 'name'),
				route: routePrefix + '' + _self.getNode(annotation, 'route'),
				method: _self.getNode(annotation, 'method'),
				description: _self.getNode(annotation, 'description'),
				parameters: _self.getNode(annotation, 'parameter')
			});
		}
	});
};

Annotations.prototype.getAnnotations = function (callback) {
	var _self = this;
	callback = callback || function () {
	};

	_self.getFiles(function (files) {
		files.forEach(function (file, index) {
			fileSystem.readFile(_self.dir + file, 'utf8', function (err, content) {
				if (err) {
					throw new Error(err.message);
				}

				_self.readAnnotations(content, file);
				if (++index === files.length) {
					setTimeout(function () {
						callback(_self.output);
					}, 4000);
				}
			});
		});
	});


};

var routes = new Annotations('../../api/controllers/');
setTimeout(function () {
	routes.getAnnotations(function (json) {
		var strJSON = JSON.stringify(json, null, 4);
		fileSystem.writeFile('./src/main/html/swagger.json', strJSON, function (err) {
			if (err) {
				throw new Error('Could not write a JSON file!');
			}
			console.log('Done.');
		});
	});
}, 5000);
