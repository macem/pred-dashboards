/**
* ValidUsers.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
	attributes: {
		username: {
			type: 'string',
			required: true,
			minLength: 2
		},
		firstName: {
			type: 'string',
			required: true,
			minLength: 1
		},
		lastName: {
			type: 'string',
			required: true,
			minLength: 1
		},
		roles: {
			type: 'array',
			required: false
		},
		email: {
			type: 'email',
			required: true
		},
		isSuperUser: {
			type: 'boolean',
			required: false,
			defaultsTo: false
		}
	}
};

