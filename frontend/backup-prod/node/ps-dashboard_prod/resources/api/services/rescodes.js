module.exports = {
	success: {
		ok: 200,
		created: 201,
		accepted: 202
	},
	redirection: {
		multipleChoices: 300,
		movedPermanently: 301,
		found: 302
	},
	clientError: {
		badRequest: 400,
		unauthorized: 401,
		forbidden: 403,
		notFound: 404,
		methodNotAllowed: 405,
		requestTimeout: 408
	},
	serverError: {
		internalError: 500,
		notImplemented: 501,
		unavailable: 503,
		unknownError: 520
	}
};