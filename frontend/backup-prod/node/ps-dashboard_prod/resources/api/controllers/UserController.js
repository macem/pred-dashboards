var http = require('https');

module.exports = {
	/**
	 * @route /photo/{id}
	 * @name photo
	 * @method get
	 * @description Getting user photo
	 * @parameter id:string:required:path
	 */
	photo: function(req, res) {
		var options = {
			host: sails.config.mogate.host,
			connection: sails.config.mogate.close,
			port: sails.config.mogate.port,
			path: sails.config.mogate.operations.photo.path(req.session.userData ? req.session.userData.username : 'fournis5'),
			method: sails.config.mogate.operations.photo.method,
			rejectUnauthorized: false,
			headers: {
				'Api-Key': sails.config.mogate.apiKey
			}
		};

		var mogateReq = http.request(options, function(response) {

			response.setEncoding('binary');

			/**
			 * Create an empty string
			 * where the incoming data
			 * will be stored.
			 * @type {string}
			 */
			var tmpBinaryData = '';

			/**
			 * Define 'onData' callback
			 * to catch and store incoming data.
			 */
			response.on('data', function (chunk) {
				tmpBinaryData += chunk;
			});

			/**
			 * After the incoming binary data
			 * transfer is completed,
			 * we can handle this data and display
			 * as an image in browser
			 */
			response.on('end', function() {
				/**
				 * Serve user photo
				 * if it's available on Mogate.
				 */
				if(response.statusCode === 200) {
					res.writeHead(200, {
						'Content-Type': 'image/png'
					});
					res.end(tmpBinaryData, 'binary');
				} else {

					/**
					 * Otherwise, serve default user photo
					 * using Skipper Disk.
					 *
					 * https://github.com/balderdashy/skipper-disk
					 *
					 * @type {function(): adapter|exports}
					 */
					var SkipperDisk = require('skipper-disk');
					var fileAdapter = SkipperDisk();

					fileAdapter.read('./assets/images/default_user.png')
							.on('error', function (err){
								return res.serverError(err);
							})
							.pipe(res);
				}
				tmpBinaryData = undefined;
			});
		});


		/**
		 * Define 'onTimeout' callback
		 * to catch the timeout event
		 * and send a proper message to client.
		 */
		mogateReq.on('timeout', function(err) {
			console.log('>>>>>>>>>>>>>>>>> TIMEOUT: ' +  + JSON.stringify(err));
			mogateReq.abort();
			return res.json(
					sails.services.rescodes.clientError.requestTimeout,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.requestTimeout, req.__('connectionTimeout'))
			);
		});

		/**
		 * Define 'onError' callback
		 * to catch possible connection
		 * errors, e.g. with roche's internal network.
		 */
		mogateReq.on('error', function(err) {
			console.log('>>>>>>>>>>>>>>>>> ERROR: ' + JSON.stringify(err));
			mogateReq.abort();
			return res.json(
					sails.services.rescodes.clientError.requestTimeout,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.requestTimeout, req.__('connectionTimeout'))
			);
		});

		mogateReq.end();
	}
};