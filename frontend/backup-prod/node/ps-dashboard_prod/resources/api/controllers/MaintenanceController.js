/**
 * MaintenanceController
 *
 * @description :: Server-side logic for managing Maintenances
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	/**
	 * @route /
	 * @name
	 *
	 * @method get
	 * @description Returns informations about the possible maintenance.
	 */
	index: function(req, res) {
		Maintenance.find().limit(1)
				.then(function(found) {

					if(!found || found.length === 0) {
						return res.json({
							notfound: true
						});
					}

					found = found.pop();


					if(found.hasOwnProperty('isActive')) {
						return res.json(found);
					}

					return res.json({});

				});
	},

	/**
	 * @route /setStatus
	 * @name setStatus
	 *
	 * @method put
	 * @description Sets the maintenance message.
	 * @parameter message:string:required:formData
	 * @parameter isActive:boolean:notrequired:formData
	 */
	setStatus: function(req, res) {
		/**
		 * Check if client's method is allowed in this
		 * method.
		 */
		if(!sails.services.utils.allowedMethod(req, res, ['PUT'])) {
			return res.json(
					sails.services.rescodes.clientError.methodNotAllowed,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.methodNotAllowed, req.__('methodNotAllowed'))
			);
		}

		Maintenance.find().limit(1).exec(function createFindCB(err, record){

			if(err) {
				return res.negotiate(err);
			}

			if(record && record.length > 0) {
				record = record.pop();
				Maintenance.update({id: record.id}, {message: req.body.message, isActive: req.body.isActive})
						.then(function(record) {
							return res.json(record.pop());
						});
			} else {
				Maintenance.create({message: req.body.message, isActive: req.body.isActive})
						.then(function(record) {
							return res.json(record);
						});
			}

		});
	},
	create: function(req, res) {
		this.index(req, res);
	},
	update: function(req, res) {
		this.index(req, res);
	},
	destroy: function(req, res) {
		this.index(req,res);
	}
};

