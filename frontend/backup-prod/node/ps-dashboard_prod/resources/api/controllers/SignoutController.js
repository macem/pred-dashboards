/**
 * SignoutController
 *
 * @description :: Server-side logic for managing Signouts
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	index: function(req, res) {
		try {
			/**
			 * Destroy session cookie
			 * only if the user is still
			 * logged in.
			 */
			if (req.session.authenticated) {
				req.session.destroy();
			}
			return res.json(
					sails.services.rescodes.success.ok,
					sails.services.utils.printMessage(sails.services.rescodes.success.ok, req.__('loggedOut'))
			);
		} catch(e) {
			return res.json(
					sails.services.rescodes.serverError.internalError,
					sails.services.utils.printMessage(sails.services.rescodes.serverError.internalError, req.__('couldntLogOut'))
			);
		}
	}
};

