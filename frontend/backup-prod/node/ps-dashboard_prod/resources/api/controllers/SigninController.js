/**
 * SigninController
 *
 * @description :: Server-side logic for managing sign-in process
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var http = require('http');
var https = require('https');
var urlencode = require('urlencode');

module.exports = {

  index: function (req, res) {

    sails.log.warn('signin index start');
    sails.log.warn(req.session);
    sails.log.warn(req.cookies);

    if (!req.session.hasOwnProperty('userData')) {

      sails.log.warn('===== no property userData');

      if (req.cookies['bioinfo_bas']) {

        var options = {
          method: 'GET',
          host: 'predilogin.roche.com',
          path: '/pREDiLogin?info',
          headers: {
            Cookie: 'bioinfo_bas=' + urlencode(req.cookies['bioinfo_bas'])
          }
        };

        var loginReq = http.request(options, function(response) {

          var tmpStr = '';
          response.on('data', function (chunk) {
            tmpStr += chunk;
          });

          response.on('end', function() {

            // get userID
            var regexp = /user .+\<br\/\>cookie/i;
            var found = tmpStr.match(regexp);
            if (found && found[0]) {

              var userId = found[0].replace('user ', '').replace('<br/>cookie', '').trim();

              sails.log.warn('USERID: ', userId);

              var userDetailsOptions = {
                method: 'POST',
                rejectUnauthorized: false,
                host: 'predilogin.roche.com',
                path: '/rest-api/users/details',
                headers: {
                  'Content-Type': 'application/json'
                }
              };
              var body = JSON.stringify({ id: userId });

              sails.log.warn('USER DETAILS OPTIONS: ', userDetailsOptions);

              var detailsReq = https.request(userDetailsOptions, function (detailsResp) {

                var userData = '';
                detailsResp.on('data', function (chunk) {
                  userData += chunk;
                });

                detailsResp.on('end', function() {

                  var userDataJSON = JSON.parse(userData);

                  sails.log.warn('USER JSON: ', userDataJSON);

                  ValidUsers
                    .findOrCreate(
                      {username: userId},
                      {
                        username: userId,
                        firstName: userDataJSON.firstName,
                        lastName: userDataJSON.lastName,
                        roles: [],
                        email: userDataJSON.email
                      }
                    )
                    .exec(function afterwards(err, found) {

                      if (err) {
                        sails.log.warn('Error while create / get db user, err: ' + JSON.stringify(err));
                        res.negotiate(err);
                      }

                      // update data
                      found.username = userId;
                      found.firstName = userDataJSON.firstName;
                      found.lastName = userDataJSON.lastName;
                      found.roles = [];
                      found.email = userDataJSON.email;
                      found.save();

                      req.session.authenticated = true;
                      req.session.userData = found;

                      // check if user is superuser
                      checkIfSuperUser(sails.config.admin.allowGroup, userId)
                        .then(function (isSuperUser) {

                          req.session.userData.isSuperUser = isSuperUser;

                          if (!req.session.userData.isSuperUser) {
                            req.session.userData.mappedRoles = [];
                            req.session.userData.roles = [];

                            sails.services.utils.groupsToIds(function (mappedRoles) {
                              req.session.userData.mappedRoles = mappedRoles;
                              return res.json(req.session.userData);
                            }, req);
                          } else {
                            return res.json(req.session.userData);
                          }

                        });

                    });
                });


                detailsResp.on('error', function(err) {
                  sails.log.warn('Error while getting user details, err: ' + JSON.stringify(err));
                  req.session.authenticated = false;
                  return res.json(
                    sails.services.rescodes.serverError.internalError,
                    sails.services.utils.printErrorMessage(sails.services.rescodes.serverError.internalError, 'Cannot get user details!')
                  );
                });

                detailsResp.on('timeout', function (err) {
                  sails.log.warn('Timeout while getting user details, err: ' + JSON.stringify(err));
                });

              });

              detailsReq.end(body);

            } else {

              sails.log.warn('No user id found while parsing userInfo page');

              req.session.authenticated = false;
              return res.json(
                sails.services.rescodes.serverError.internalError,
                sails.services.utils.printErrorMessage(sails.services.rescodes.serverError.internalError, 'Active session not found!')
              );

            }
          });

          response.on('error', function (err) {
            sails.log.warn('Error while getting prediLogin info (user id), err: ' + JSON.stringify(err));
          });
          response.on('timeout', function (err) {
            sails.log.warn('Timeout while getting prediLogin info (user id), err: ' + JSON.stringify(err));
          });

        });

        loginReq.end();

      } else {

        sails.log.warn('No bioinfo cookie!');

        req.session.authenticated = false;
        return res.json(
          sails.services.rescodes.serverError.internalError,
          sails.services.utils.printErrorMessage(sails.services.rescodes.serverError.internalError, 'No bioinfo cookie!')
        );

      }

    } else {

      sails.log.warn('===== return userData');
      sails.log.warn(req.session.userData);

      return res.json(req.session.userData);

    }

  },


	/**
   * =========== DEPRECATED ===============
   *
	 * @route /
	 * @name
	 *
	 * @method post
	 * @description Sign-in with user credentials.
	 * @parameter username:string:required:formData
	 * @parameter password:string:required:formData
	 */
	index_old: function(req, res) {

		/**
		 * Check if client's method is allowed in this
		 * method.
		 */
		if(!sails.services.utils.allowedMethod(req, res, ['POST'])) {
			return res.json(
					sails.services.rescodes.clientError.methodNotAllowed,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.methodNotAllowed, req.__('methodNotAllowed'))
			);
		}

		/**
		 * Our credentials from POST
		 * @type {{username: (*|exports.attributes.username), password: (*|exports.connections.someMysqlServer.password|exports.connections.somePostgresqlServer.password|object.auth.password|adapter.defaults.password|Sizzle.password)}}
		 */
		var credentials = {
			username: req.body.username,
			password: req.body.password
		};

		/**
		 * Verify if credentials are not empty
		 */
		if(!credentials.username || !credentials.password) {
			return res.json(
					sails.services.rescodes.clientError.forbidden,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.forbidden, req.__('emptyCredentials'))
			);
		}

		/**
		 * Prepare a proper request
		 * to the Mogate API Service
		 * (hosted on Roche side).
		 *
		 * @type {{host: *, connection: *, port: *, path: *, method: (exports.mogate.operations.authenticate.method|*), rejectUnauthorized: boolean, headers: {Authorization: string, Api-Key: (exports.mogate.apiKey|*|string), Content-Type: string}}}
		 */
		var options = {
			host: sails.config.mogate.host,
			connection: sails.config.mogate.close,
			port: sails.config.mogate.port,
			path: sails.config.mogate.operations.authenticate.path(),
			method: sails.config.mogate.operations.authenticate.method,
			rejectUnauthorized: false,
			headers: {
				'Authorization': 'Basic ' + new Buffer(credentials.username + ':' + credentials.password).toString('base64'),
				'Api-Key': sails.config.mogate.apiKey,
				'Content-Type': 'application/json'
			}
		};

		/**
		 * Send a proper request to Mogate
		 * and store this request in mogateReq variable.
		 */
		var mogateReq = http.request(options, function(response) {

			/**
			 * Set needed encoding.
			 */
			response.setEncoding('utf8');

			/**
			 * Create an empty string
			 * where the incoming data
			 * will be stored.
			 * @type {string}
			 */
			var tmpStr = '';

			/**
			 * Define 'onData' callback
			 * to catch and store incoming data.
			 */
			response.on('data', function (chunk) {
				tmpStr += chunk;
			});

			/**
			 * After the incoming data
			 * transfer is completed,
			 * we can handle this data...
			 */
			response.on('end', function() {

				var responseJSON;

				/**
				 * In case when incoming data
				 * is not in JSON format...
				 */
				try {
					responseJSON = JSON.parse(tmpStr);
				} catch(e) {

					/**
					 * Just in case - set 'authenticated' session key
					 * to false;
					 * @type {boolean}
					 */
					req.session.authenticated = false;
					return res.json(
							sails.services.rescodes.serverError.internalError,
							sails.services.utils.printErrorMessage(sails.services.rescodes.serverError.internalError, req.__('StringToJSONError'))
					);
				}

				if(responseJSON.Status !== 'Success') {
					/**
					 * Just in case - set 'authenticated' session key
					 * to false;
					 * @type {boolean}
					 */
					req.session.authenticated = false;
					return res.json(
							sails.services.rescodes.clientError.forbidden,
							sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.forbidden, req.__('accessForbidden') + ' ' + responseJSON.Status)
					);
				}

				/**
				 * Next, we will try to find
				 * our user in database.
				 */
				try {


					ValidUsers.findOrCreate(
						{
							username: responseJSON.User.Username
						},
						{
							username: responseJSON.User.Username,
							firstName: responseJSON.User.FirstName,
							lastName: responseJSON.User.LastName,
							roles: responseJSON.User.Roles,
							email: responseJSON.User.Email
						}
					).exec(function afterwards(err, found){
						if(err) {
							res.negotiate(err);
						}

						// update data
						found.username = responseJSON.User.Username;
						found.firstName = responseJSON.User.FirstName;
						found.lastName = responseJSON.User.LastName;
						found.roles = responseJSON.User.Roles;
						found.email = responseJSON.User.Email;
						found.save();

						/**
						 * If everything is OK.,
						 * we can set 'authenticated' session key to be true.
						 * @type {boolean}
						 */
						req.session.authenticated = true;

						/**
						 * And store some user's data
						 * in 'userData' session key.
						 */
						req.session.userData = found;

            // TODO: try to save this value
						req.session.userData.isSuperUser = found.roles.indexOf(sails.config.admin.allowGroup) >= 0;

            delete req.session.userData.roles;
            delete found.roles;

            if (!req.session.userData.isSuperUser) {

              req.session.userData.mappedRoles = [];

              sails.services.utils.groupsToIds(function (mappedRoles) {

                req.session.userData.mappedRoles = mappedRoles;

                /**
                 * At the end, we can send
                 * our new record to client.
                 */
                return res.json(req.session.userData);

              }, req);
            } else {
              return res.json(req.session.userData);
            }

					});

 				} catch(e) {
					/**
					 * Just in case - set 'authenticated' session key
					 * to false;
					 * @type {boolean}
					 */
					req.session.authenticated = false;

					return res.json(sails.services.rescodes.serverError.internalError, sails.services.utils.printErrorMessage(sails.services.rescodes.serverError.internalError, e));
				}
			});
		});

		/**
		 * Define 'onTimeout' callback
		 * to catch the timeout event
		 * and send a proper message to client.
		 */
		mogateReq.on('timeout', function(err) {
			console.log('>>>>>>>>>>>>>>>>> TIMEOUT: ' +  + JSON.stringify(err));
			mogateReq.abort();
			return res.json(
					sails.services.rescodes.clientError.requestTimeout,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.requestTimeout, req.__('connectionTimeout'))
			);
		});

		/**
		 * Define 'onError' callback
		 * to catch possible connection
		 * errors, e.g. with roche's internal network.
		 */
		mogateReq.on('error', function(err) {
			console.log('>>>>>>>>>>>>>>>>> ERROR: ' + JSON.stringify(err));
			mogateReq.abort();
			return res.json(
					sails.services.rescodes.clientError.requestTimeout,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.requestTimeout, req.__('connectionTimeout'))
			);
		});

		mogateReq.end();

	},
	/**
	 * @route /currentUser
	 * @name currentUser
	 *
	 * @method get
	 * @description Returns the current user's data.
	 */
	currentUser: function(req, res) {

		/**
		 * Let's check if we have 'userData' object
		 * in our session store...
		 */
		if(req.session.hasOwnProperty('userData')) {

      if (!req.session.userData.isSuperUser) {

        req.session.userData.mappedRoles = [];

        sails.services.utils.groupsToIds(function (mappedRoles) {

          req.session.userData.mappedRoles = mappedRoles;

          /**
           * At the end, we can send
           * our new record to client.
           */
          return res.json(req.session.userData);

        }, req);
      } else {
        return res.json(req.session.userData);
      }

		} else {
			/**
			 * If not, we print an error
			 * to the user's client.
			 */
			return res.json(
				sails.services.rescodes.serverError.unknownError,
				sails.services.utils.printErrorMessage(sails.services.rescodes.serverError.unknownError, req.__('unknownError'))
			);
		}
	}
};

var q = require('q');

function checkIfSuperUser(adminGroup, userId) {

  var deferred = q.defer();
  var options = {
    method: 'GET',
    rejectUnauthorized: false,
    host: 'predilogin.roche.com',
    path: '/rest-api/groups/' + adminGroup + '/users/' + userId,
    headers: {
      Accept: 'application/json'
    }
  };

  // console.log(options);

  var adminReq = https.request(options, function (response) {

    var data = '';
    response.on('data', function (chunk) {
      data += chunk;
    });

    response.on('end', function () {
      var member = JSON.parse(data);
      deferred.resolve(member.member);
    });

    response.on('error', function () {
      deferred.resolve(false);
    });

  });

  adminReq.end();

  return deferred.promise;

}
