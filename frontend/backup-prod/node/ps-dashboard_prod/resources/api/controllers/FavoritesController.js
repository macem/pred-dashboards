/**
 * FavoritesController
 *
 * @description :: Server-side logic for managing favorites
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	/**
	 * @route /
	 * @name index
	 *
	 * @method get
	 * @description Return favorites for the current user.
	 */
	index: function(req, res) {

		var _ = require('underscore');
		var async = require('async');

		var popCriteria = {};

		if (!req.session.userData.isSuperUser) {
			popCriteria.group =  req.session.userData.mappedRoles.length > 0 ? req.session.userData.mappedRoles : ['xyzXYZJhndow'];
		}

		var criteria = {
			userId: req.session.userData.id
		};

		var createIfNotFound = {
			userId: req.session.userData.id,
			contents: [],
			ordering: []
		};




		async.auto({
					mashup: function(cb) {
						var favs = Favorites.findOne(criteria).populate('contents', popCriteria);
						favs.then(function(records) {
							if(!records) {
								Favorites.create(createIfNotFound).populateAll().exec(cb);
							} else {
								favs.exec(cb);
							}
						})
								.catch(function(err) {
									favs.exec(cb);
								});
					},

					docTypes: ['mashup', function(cb, async_data) {
						DocTypes.find().exec(cb);
					}],

					categories: ['docTypes', function(cb, async_data) {
						Categories.find().exec(cb);
					}]

				},
				function allDone(err, async_data) {

					if(!async_data.mashup) {
						return res.json([]);
					}

					_.map(async_data.mashup.contents, function (record) {
						var docType = _.where(async_data.docTypes, { id: record.docType });
						record.docType = docType.pop();
						if(async_data.mashup.hasOwnProperty('ordering')) {
							record.position = async_data.mashup.ordering.indexOf(record.id);
						}
					});

					_.map(async_data.mashup.contents, function (record) {
						var category = _.where(async_data.categories, { id: record.category });
						record.category = category.pop();
						try {
							delete record.category.items;
							delete record.category.position;
						} catch(e) {
							console.error(e);
						}
					});

					if(async_data.mashup.hasOwnProperty('ordering')) {
						try {
							delete async_data.mashup.ordering;
						} catch(e) {
							console.error(e);
						}
					}

					res.json(async_data.mashup);
				});
	},

	/**
	 * @route /order
	 * @name order
	 *
	 * @method put
	 * @description Provide a method to change order of current user favorites.
	 * @parameter order:array:required:formData
	 */
	order: function(req, res) {
		/**
		 * Check if client's method is allowed in this
		 * method.
		 */
		if(!sails.services.utils.allowedMethod(req, res, ['PUT'])) {
			return res.json(
					sails.services.rescodes.clientError.methodNotAllowed,
					sails.services.utils.printErrorMessage(sails.services.rescodes.clientError.methodNotAllowed, req.__('methodNotAllowed'))
			);
		}

		var order = req.body.order || [];

		Favorites.update({
			userId: req.session.userData.id
		},{
			contents: order,
			ordering: order
		})
		.exec(function afterwards(err, updated){

			if(err) {
				return res.negotiate(err);
			}

			/**
			 * At the end, we can send
			 * our new record to client.
			 */
			return res.json(updated);
		});
	},

	/**
	 * @route /update/{id}
	 * @name update
	 *
	 * @method get
	 * @description Toggle the given report id in user favorites.
	 * @parameter id:string:required:path
	 */
	update: function(req, res) {

		/**
		 * Check if 'id' parameter
		 * is in string format.
		 */
		req.validate({
			id: 'string'
		});


		/**
		 * Item unique id from URL path.
		 */
		var itemId = req.param('id');

		/**
		 * Next, we will try to find
		 * if the user have his 'favorites' record
		 * in database.
		 */
		try {
			Favorites.findOne({
				userId: req.session.userData.id
			})
			.populate('contents')
			.exec(function findOneCB(err, found){

				/**
				 * If not found,
				 * we should add a new record
				 * to our database.
				 */
				if(found === undefined) {

					/**
					 * Create new record
					 * and put itemId in the 'contents' collection.
					 */
					var collection = [];
					collection.push(itemId);

					Favorites.create({
						userId: req.session.userData.id,
						contents: collection
					})
					.exec(function createCB(err, created){

						if(err) {
							return res.negotiate(err);
						}

						/**
						 * At the end, we can send
						 * our new record to client.
						 */
						return res.json(created[0]);
					});
				} else {

					var doUpdate = function(updatedCollection) {
						updatedCollection = updatedCollection || [];

						/**
						 * Check if given itemId
						 * exists in our collection.
						 *
						 * If yes, then remove it.
						 */
						if(updatedCollection.indexOf(itemId) > -1) {
							updatedCollection = updatedCollection.filter(function(value) {
								return value !== itemId;
							});
						} else {
							/**
							 * Or push our itemId to
							 * the collection.
							 */
							updatedCollection.push(itemId);
						}
						/**
						 * This is the scenario, where
						 * we know the user have his
						 * 'favorites' record in the database.
						 *
						 * Then we should only push
						 * new item to the 'contents' collection.
						 */
						console.log('User\'s favorites exist. Update.');
						Favorites.update({
							userId: req.session.userData.id
						},{
							contents: updatedCollection
						})
								.exec(function afterwards(err, updated){

									if(err) {
										return res.negotiate(err);
									}

									/**
									 * At the end, we can send
									 * our new record to client.
									 */
									return res.json(updated);
								});
					};

					var contents = found.contents || [];
					var tmpArray = [];

					contents.forEach(function(value, index) {
						tmpArray.push(value.id);

						if(++index === contents.length) {
							doUpdate(tmpArray);
						}
					});

					if(contents.length === 0) {
						doUpdate([]);
					}


				}
			});

		} catch(e) {
			return res.negotiate(err);
		}

	}
};

